package com.rni.rni.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class FixedAssetDto implements Serializable {
    private String code;
    private String dimension1;
    private String dimension2;
    private String faCategory;
    private String faClass;
    private String faSubclass;
    private String merk;
    private String tanggalPerolehan;
    private String hargaPerolehan;
    private String tanggalPenyusutan;
    private String nilaiPenysutan;
    private String tanggalNilaiBuku;
    private String nilaiBuku;
    private String unit;
    private String pengguna;
    private String noKendaraan;
    private String keterangan;
    private String createdBy;
    private String updatedBy;
    private String createdAt;
    private String updatedAt;
}
