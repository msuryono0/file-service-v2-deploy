package com.rni.rni.domain.dto;

import com.rni.rni.constant.FieldTypeEnum;
import com.rni.rni.constant.OperatorEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FilterSearchDto implements Serializable {
    public static final long serialVersionUID = 9204490190156275576L;
    private String key;

    private OperatorEnum operator;

    private FieldTypeEnum fieldType;

    private transient Object value;

    private transient Object valueTo;

    private transient List<Object> values;
}
