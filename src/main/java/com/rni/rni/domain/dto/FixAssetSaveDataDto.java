package com.rni.rni.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FixAssetSaveDataDto implements Serializable {

    private static long serialVersionUID = 3059642603348899100L;

    private Long id;
    private String dimension_1;
    private String dimension_2;
    private String faCategory;
    private String faClass;
    private String faSubclass;

    private String merk;
    private Date tanggalPerolehan;
    private Integer hargaPerolehan;
    private Date tanggalPenyusutan;
    private Integer nilaiPenyusutan;
    private Date tanggalNilaiBuku;
    private Integer nilaiBuku;
    private Integer unit;
    private String pengguna;
    private String noKendaraan;
    private String keterangan;
}
