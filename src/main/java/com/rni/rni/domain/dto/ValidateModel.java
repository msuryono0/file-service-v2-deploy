package com.rni.rni.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ValidateModel {

    private String dimension;
    private String dimensionValue;
    private String faCategory;
    private String faClass;
    private String faSubsClass;
}
