package com.rni.rni.domain.dto;

import com.rni.rni.domain.dao.AP9;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;
import java.util.List;

@Data
@AllArgsConstructor
@SuperBuilder
@NoArgsConstructor
public class Ap9ResponseDto extends BaseResponsePaging implements Serializable{
    public static final long serialVersionUID = -3424440754523510479L;
    private List<AP9> ap9s;

}
