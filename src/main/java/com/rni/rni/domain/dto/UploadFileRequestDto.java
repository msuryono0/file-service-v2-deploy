package com.rni.rni.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class UploadFileRequestDto implements Serializable {
    public static final long serialVersionUID = -8133786295445689587L;

    private String tableName;
}
