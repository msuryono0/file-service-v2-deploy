package com.rni.rni.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ErrorMapingDto implements Serializable {
    public static final long serialVersionUID = 8298405405466444659L;
    private String code;
    private String message;
}
