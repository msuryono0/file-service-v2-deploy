package com.rni.rni.domain.dto;

import com.rni.rni.domain.dao.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;


@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class CommonDto implements Serializable {
    public static final long serialVersionUID = -6368841061516679048L;
    private List<Jabatan> jabatanList;
    private List<FaCategory> faCategories;
    private List<GroupItem> groupItems;
    private List<ClassProd> classProds;
    private List<TypeProd> typeProds;
    private List<ProdCategory> prodCategories;
    private List<FaClass> faClasses;
    private List<FaSubClass> faSubClasses;
    private List<Divisi> divisis;
    private List<Golongan> golongans;
    private List<StatusKaryawan> statusKaryawans;
    private List<GajiKaryawan> gajiKaryawans;
    private List<Dimension> dimensions;
    private List<DimensionValue> dimensionValues;
    private List<TbGlAccount> tbGlAccounts;
    private List<FixedAsset> fixedAssets;
    private List<Product> products;
    private List<Karyawan> karyawans;
    private List<KK9> kk9List;
    private List<Struktural> strukturals;
    private List<TbPjBeras> tbPjBeras;
    private List<AP9> ap9;
    private List<TbPjSeragam> tbPjSeragams;
    private List<TransactionEntryHcShare> transactionEntryHcShares;
    private List<TransactionEntryHcOmzet> transactionEntryHcOmzets;
}
