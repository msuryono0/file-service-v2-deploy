package com.rni.rni.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CoaRequestDto implements Serializable {

    private static final long serialVersionUID = 3550621322688361744L;

    private Integer page;
    private Integer limit;
    private String search;
}