package com.rni.rni.domain.dto;

import com.rni.rni.domain.dao.Menu;
import com.rni.rni.domain.dao.MenuLevel;
import com.rni.rni.domain.dao.MenuSub;
import com.rni.rni.domain.dao.UserLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class PermissionRequestDto implements Serializable {
    public static final long serialVersionUID = -3424440754523510479L;
    private Long id;
    private Menu menu;
    private MenuSub menuSub;
    private MenuLevel menuLevel;
    private UserLevel userLevel;
}
