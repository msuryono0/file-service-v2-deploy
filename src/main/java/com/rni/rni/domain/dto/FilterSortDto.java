package com.rni.rni.domain.dto;

import com.rni.rni.constant.DirectionEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FilterSortDto implements Serializable {
    public static final long serialVersionUID = 9204490190156275576L;
    private String key;
    private DirectionEnum direction;
}
