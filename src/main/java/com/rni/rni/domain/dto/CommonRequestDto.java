package com.rni.rni.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;

@Data
@AllArgsConstructor
@SuperBuilder
@NoArgsConstructor
public class CommonRequestDto extends BaseRequestPaging implements Serializable {
    public static final long serialVersionUID = -3424440754523510479L;
    private String search;
    private String codeSearch;
    private String tahun;
    private String periode;
    private String unit;
}
