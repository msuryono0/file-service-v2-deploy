package com.rni.rni.domain.dto;

import com.rni.rni.domain.dao.FaSubClass;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;
import java.util.List;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class FaSubClassFindResponseDto extends BaseResponsePaging implements Serializable {

    private static final long serialVersionUID = 3550621322688361744L;
    private List<FaSubClass> faSubClasses;
}