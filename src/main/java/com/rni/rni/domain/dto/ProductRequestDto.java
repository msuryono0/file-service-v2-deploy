package com.rni.rni.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;

@Data
@AllArgsConstructor
@SuperBuilder
@NoArgsConstructor
public class ProductRequestDto extends BaseRequestPaging implements Serializable{
    public static final long serialVersionUID = -3424440754523510479L;
    private Long id;
    private String productId;
    private String productName;
    private String groupProd;
    private String category;
    private String type;
    private String uom;
    private String supplier;
}
