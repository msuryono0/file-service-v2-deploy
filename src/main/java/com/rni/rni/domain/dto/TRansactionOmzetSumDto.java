package com.rni.rni.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class TRansactionOmzetSumDto implements Serializable {
    public static final long serialVersionUID = 4217917757476221024L;
    private Long jan;
    private Long feb;
    private Long mar;
    private Long twi;
    private Long apr;
    private Long mei;
    private Long jun;
    private Long twii;
    private Long jul;
    private Long agt;
    private Long sep;
    private Long twiii;
    private Long okt;
    private Long nov;
    private Long des;
    private Long twiv;

}
