package com.rni.rni.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Builder
@AllArgsConstructor
@Data
@NoArgsConstructor
public class CommonSearchDto implements Serializable {
    public static final long serialVersionUID = 8059779008061351074L;
    private List<FilterSearchDto> filters = new ArrayList<>();

    private List<FilterSortDto> sorts = new ArrayList<>();
    private int page =0;
    private int size =10;
}
