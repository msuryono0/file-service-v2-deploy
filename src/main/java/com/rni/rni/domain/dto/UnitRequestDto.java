package com.rni.rni.domain.dto;

import com.rni.rni.domain.dao.Dimension;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class UnitRequestDto implements Serializable {
    public static final long serialVersionUID = -3424440754523510479L;
    private Long id;
    private String code;
    private Dimension dimensionCode;
    private String noCabang;
    private String keterangan;
}
