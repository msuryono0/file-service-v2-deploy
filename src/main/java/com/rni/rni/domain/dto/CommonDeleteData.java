package com.rni.rni.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class CommonDeleteData {

    private static final long serialVersionUID = -7838140408419410059L;

    private Long id;

}
