package com.rni.rni.domain.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.rni.rni.domain.dao.CoaHeader;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;
import java.util.List;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class CoaResponseDto extends BaseResponsePaging implements Serializable {

    private static final long serialVersionUID = 3550621322688361744L;
    private List<CoaHeader> coaHeaders;
}