package com.rni.rni.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class CabangRequestDto implements Serializable {
    public static final long serialVersionUID = -3424440754523510479L;
    private Long id;
    private String code;
    private String keterangan;
    private int page;
    private int size;
}
