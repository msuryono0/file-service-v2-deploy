package com.rni.rni.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Metadata implements Serializable {

    private static final long serialVersionUID = 3550621302688361744L;

    private String cabangCategory;
    private String unit;
    private String branch;
    private String username;
}