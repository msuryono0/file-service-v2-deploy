package com.rni.rni.domain.dto;

import com.rni.rni.domain.dao.Uom;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class UomResponseDto implements Serializable {
    public static final long serialVersionUID = -3424440754523510479L;
    private List<Uom> uoms;
    private Integer totalPages;
    private Integer totalElements;
    private Integer numberOfElements;
    private Integer pageNumber;
}
