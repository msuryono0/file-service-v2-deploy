package com.rni.rni.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class UserLoginRequestDto implements Serializable {
    public static final long serialVersionUID = -3424440754523510479L;
    private Long id;
    private Long idKaryawan;
    private String username;
    private String password;
    private String name;
    private String cabang;
    private String divisi;
    private Long level;
    private String search;
    private int page;
    private int size;
}
