package com.rni.rni.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class TransactionHCOmzetDto implements Serializable {
    public static final long serialVersionUID = -5056695471890908497L;
    private Long id;
    private String cabangDivisi;
    private String product;
    private Long jan;
    private Long feb;
    private Long mar;
    private Long twi;
    private Long apr;
    private Long mei;
    private Long jun;
    private Long twii;
    private Long jul;
    private Long agt;
    private Long sep;
    private Long twiii;
    private Long okt;
    private Long nov;
    private Long des;
    private Long twiv;
    private Long ttl;
    private String periode;
    private String year;
    private String additionalData;
    private String rowNumber;
    private String keterangan;
    private String groupProd;

}
