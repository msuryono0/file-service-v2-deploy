package com.rni.rni.domain.dao;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "fixed_asset")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
@Where(clause = "is_deleted = false")
public class FixedAsset implements Serializable {
    public static final long serialVersionUID = 1624904102937614855L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String code;

    @OneToOne
    @JoinColumn(name = "dimension_1", referencedColumnName = "code")
    private Dimension dimension_1;

    @OneToOne
    @JoinColumn(name = "dimension_2", referencedColumnName = "code")
    private DimensionValue dimension_2;

    private String faCategory;
    private String faClass;
    private String faSubclass;

    private String merk;
    private Date tanggalPerolehan;
    private BigDecimal hargaPerolehan;
    private Date tanggalPenyusutan;
    private BigDecimal nilaiPenyusutan;
    private Date tanggalNilaiBuku;
    private BigDecimal nilaiBuku;
    private BigDecimal unit;
    private String pengguna;
    private String noKendaraan;
    private String keterangan;
    private String createdBy;
    private String updatedBy;

    @JsonIgnore
    private Date createdAt;
    @JsonIgnore
    private Date updatedAt;
    @JsonIgnore
    private boolean isDeleted;

}
