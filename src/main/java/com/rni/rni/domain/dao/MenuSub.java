package com.rni.rni.domain.dao;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "tb_m_sub_menu")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
@Where(clause = "is_deleted = false")
public class MenuSub implements Serializable {

    public static final Long serialVersionUID = 1968308713726727962L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String menuCode;
    private String subMenuCode;
    private String subMenuName;
    private String subMenuUrl;
    private String subMenuImage_1;
    private String subMenuImage_2;
    private Long permissionLevel;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-mm-dd' 'HH:mm:ss", timezone = "Asia/Jakarta")
    private Date updatedAt;
    private String createdBy;
    private String updatedBy;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-mm-dd' 'HH:mm:ss", timezone = "Asia/Jakarta")
    private Date createdAt;
    private Boolean isDeleted;


}
