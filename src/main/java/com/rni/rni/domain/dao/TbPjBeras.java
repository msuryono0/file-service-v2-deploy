package com.rni.rni.domain.dao;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "tb_pj_beras")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class TbPjBeras implements Serializable {

    public static final Long serialVersionUID = 1968308713726727962L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String year;
    private BigDecimal janJunQuantity;
    private BigDecimal jaJunTotal;
    private BigDecimal julDesQuantity;
    private BigDecimal julDesTotal;
    private BigDecimal eaQuantity;
    private BigDecimal eaTotal;
    private BigDecimal apQuantity;
    private BigDecimal apTotal;
    @OneToOne
    @JoinColumn(name = "idKaryawan", referencedColumnName = "id")
    private Karyawan idKaryawan;
}
