package com.rni.rni.domain.dao;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "users")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
@Where(clause = "is_deleted = false")
public class UserLogin {
    public static final Long serialVersionUID = 1968308713726727962L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @OneToOne
    @JoinColumn(name = "idKaryawan",referencedColumnName = "id")
    private Karyawan idKaryawan;
    private String username;
    @JsonIgnore
    private String password;
    private String name;
    @OneToOne
    @JoinColumn(name = "cabang",referencedColumnName = "code")
    private Dimension cabang;
    @OneToOne
    @JoinColumn(name = "divisi",referencedColumnName = "code")
    private DimensionValue divisi;
    @OneToOne
    @JoinColumn(name = "level",referencedColumnName = "id")
    private UserLevel level;
    @JsonIgnore
    private String createdBy;
    @JsonIgnore
    private String updatedBy;
    @JsonIgnore
    private Date createdAt;
    @JsonIgnore
    private Date updatedAt;
    @JsonIgnore
    private Boolean isDeleted;

}
