package com.rni.rni.domain.dao;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "coa_header")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
@Where(clause = "is_deleted = false")
public class CoaHeader implements Serializable {

    private static long serialVersionUID = 6864194077169129593L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "kodeRek", referencedColumnName = "accountCode")
    private TbGlAccount kodeRek;
    private String keterangan;
    private String groupRek;
    private String groupName;
    private Long saldo;
    @JsonIgnore
    private Date createdAt;
    @JsonIgnore
    private String createdBy;
    @JsonIgnore
    private Date updatedAt;
    @JsonIgnore
    private String updatedBy;
    private boolean isDeleted;


}
