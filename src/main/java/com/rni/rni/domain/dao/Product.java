package com.rni.rni.domain.dao;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "item")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Where(clause = "is_deleted = false")
public class Product implements Serializable {

    public static final Long serialVersionUID = 1968308713726727962L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String productId;
    private String productName;
    @OneToOne
    @JoinColumn(name = "groupProd",referencedColumnName = "groupProd")
    private ProdGroup groupProd;
    private String category;
    @OneToOne
    @JoinColumn(name = "type",referencedColumnName = "typeId")
    private TypeProd type;
    @OneToOne
    @JoinColumn(name = "uom",referencedColumnName = "keterangan")
    private Uom uom;
    @OneToOne
    @JoinColumn(name = "supplier",referencedColumnName = "supplierId")
    private Supplier supplier;
    private String createdBy;
    private String updatedBy;
    private Date createdAt;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-mm-dd' 'HH:mm:ss", timezone = "Asia/Jakarta")
    private Date updatedAt;
    private Boolean isDeleted;

}
