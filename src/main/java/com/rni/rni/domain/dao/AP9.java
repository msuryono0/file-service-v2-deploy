package com.rni.rni.domain.dao;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "tb_ap9")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
@Where(clause = "is_deleted = false")
public class AP9 implements Serializable {

    private static long serialVersionUID = -845319272592762186L;

    @Id
    @GeneratedValue(generator="tb_ap9_seq")
    @SequenceGenerator(name="tb_ap9_seq",sequenceName="tb_ap9_seq", allocationSize=1)
    private Long id;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "cabang_category", referencedColumnName = "code")
    private Dimension cabangCategory;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "cabang_divisi", referencedColumnName = "code")
    private DimensionValue cabangDivisi;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "struktural",referencedColumnName = "strukturalName")
    private Struktural struktural;
    private String years;
    private String staff;
    private String nonStaff;
    private String kkwt;
    private String formasi;

    private Date createdAt;
    private String createdBy;
    private Date updatedAt;
    private String updatedBy;
    private boolean isDeleted;


}
