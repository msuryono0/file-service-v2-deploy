package com.rni.rni.domain.dao;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "karyawan")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class Karyawan implements Serializable {

    private static long serialVersionUID = -6378904373008559182L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String nik;
    @OneToOne
    @JoinColumn(name = "cabang", referencedColumnName = "code")
    private Dimension cabang;
    private String divisi;
    private String nama;
    @OneToOne
    @JoinColumn(name = "jabatan", referencedColumnName = "code")
    private Jabatan jabatan;
    private Integer level;
    private String statusKaryawan;
    private String golongan;
    private String pendidikan;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-mm-dd", timezone = "Asia/Jakarta")
    private Date tanggalBekerja;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-mm-dd", timezone = "Asia/Jakarta")
    private Date tanggalLahir;

    private String statusKeluarga;
    private String typeKaryawan;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-mm-dd' 'HH:mm:ss", timezone = "Asia/Jakarta")
    private Date createdAt;

    private String createdBy;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-mm-dd' 'HH:mm:ss", timezone = "Asia/Jakarta")
    private Date updatedAt;
    private String updatedBy;
    private String jurusan;
    private String tahunLulus;
    private String tempatLahir;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-mm-dd' 'HH:mm:ss", timezone = "Asia/Jakarta")
    private Date tanggalTetap;

    private String tempatMasuk;
    private String struktural;
    private String sekolah;
}
