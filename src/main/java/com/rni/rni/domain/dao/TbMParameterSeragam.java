package com.rni.rni.domain.dao;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "tb_m_parameter_seragam")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class TbMParameterSeragam implements Serializable {

    public static final Long serialVersionUID = 1968308713726727962L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String parameterCode;
    private String dimension;
    private Integer seragam;
    private Integer seragam_2;
    private String tahun;
    private String tahun_2;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-mm-dd' 'HH:mm:ss", timezone = "Asia/Jakarta")
    private Date createdAt;
    private String createdBy;

}
