package com.rni.rni.domain.dao;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "transaction_entry_trading_share")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
@Where(clause = "is_deleted = false")
public class TransactionEntryTradingShare implements Serializable {

    private static long serialVersionUID = 6864194077169129593L;

    @Id
    @GeneratedValue(generator="transaction_entry_trading_share_seq")
    @SequenceGenerator(name="transaction_entry_trading_share_seq",sequenceName="transaction_entry_trading_share_seq", allocationSize=1)
    private Long id;

    private String cabangDivisi;
    private String product;
    private String omzetBruto;
    private String sharePenjualanTender;
    private String prDiscPedOps;
    private String prDiscRakor;
    private String ptDiscPedOps;
    private String ptDiscRakor;
    private String category;
    private String periode;
    private String tahun;
    private String omzetNetto;
    private String keterangan;
    private String groupProd;
    private String additionalData;

    @JsonIgnore
    private Date createdAt;
    @JsonIgnore
    private Date updatedAt;
    private String createdBy;

    private String updatedBy;
    private boolean isDeleted;


}
