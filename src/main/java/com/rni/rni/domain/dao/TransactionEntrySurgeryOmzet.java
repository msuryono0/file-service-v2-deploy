package com.rni.rni.domain.dao;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "transaction_entry_surgery_omzet")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
@Where(clause = "is_deleted = false")
public class TransactionEntrySurgeryOmzet implements Serializable {

    private static long serialVersionUID = 6864194077169129593L;

    @Id
    @GeneratedValue(generator="transaction_entry_surgery_omzet_seq")
    @SequenceGenerator(name="transaction_entry_surgery_omzet_seq",sequenceName="transaction_entry_surgery_omzet_seq", allocationSize=1)
    private Long id;

    private String cabangDivisi;

    private String product;
    private String jan;
    private String feb;
    private String mar;
    private String apr;
    private String mei;
    private String jun;
    private String jul;
    private String agt;
    private String sep;
    private String okt;
    private String nov;
    private String des;
    private String periode;
    private String year;
    private String additionalData;
    private String rowNumber;
    private String keterangan;
    private String groupProd;
    private String formula;

    @JsonIgnore
    private Date createdAt;
    @JsonIgnore
    private Date updatedAt;
    private String createdBy;

    private String updatedBy;
    private boolean isDeleted;


}
