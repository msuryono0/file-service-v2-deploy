package com.rni.rni.domain.dao;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "tb_kk_9")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
@Where(clause = "is_deleted = false")
public class KK9 implements Serializable {

    private static long serialVersionUID = -845319272592762186L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "cabang", referencedColumnName = "code")
    private Dimension cabang;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "divis", referencedColumnName = "code")
    private DimensionValue divisi;

    private String periode;
    private String year;
    private String name;
    private String estimateStartWorking;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "jabatanCode", referencedColumnName = "code")
    private Jabatan JabatanCode;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "statusEmployee", referencedColumnName = "code")
    private StatusKaryawan statusEmployee;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "pendidikanCode", referencedColumnName = "code")
    private Pendidikan pendidikanCode;

    @JsonIgnore
    private Date createdAt;
    @JsonIgnore
    private String createdBy;
    @JsonIgnore
    private Date updatedAt;
    @JsonIgnore
    private String updatedBy;
    private boolean isDeleted;


}
