package com.rni.rni.domain.dao;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

@Entity
@Table(name = "master_entry_hc_omzet")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
@Where(clause = "is_deleted = false")
public class MasterEntryHcOmzet implements Serializable {

    private static long serialVersionUID = 6864194077169129593L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String cabangDivisi;
    private String product;
    private String jan;
    private String feb;
    private String mar;
    private String apr;
    private String mei;
    private String jun;
    private String jul;
    private String agt;
    private String sep;
    private String okt;
    private String nov;
    private String des;
    private String periode;
    private String year;
    private String additionalData;
    private String rowNumber;
    private String formula;
    private String groupProd;


    @JsonIgnore
    private Timestamp createdAt;
    @JsonIgnore
    private Timestamp updatedAt;
    private String createdBy;

    private String updatedBy;
    private boolean isDeleted;


}
