package com.rni.rni.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.listener.ChannelTopic;
import org.springframework.stereotype.Component;

@Component
public class RedisPublishConfig implements RedisMessagePublish{

    @Autowired
    private RedisTemplate<String,Object> redisTemplate;

    @Autowired
    private ChannelTopic channelTopic;

    public RedisPublishConfig(RedisTemplate<String,Object> redisTemplate,ChannelTopic channelTopic){
        this.redisTemplate = redisTemplate;
        this.channelTopic = channelTopic;
    }

    @Override
    public void publish(String message, String chanel) {
        redisTemplate.convertAndSend(new ChannelTopic(chanel).getTopic(),message);

    }

    @Override
    public void publish(String message){
        redisTemplate.convertAndSend(channelTopic.getTopic(),message);
    }

}
