package com.rni.rni.config;


import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.stereotype.Service;

import java.nio.charset.StandardCharsets;

@Slf4j
@Service
public class RedisSubscribeConfig implements MessageListener {
    public static String messageList;
    @Override
    public void onMessage(Message message, byte[] bytes){
        String data =  new String(message.getBody(), StandardCharsets.UTF_8);
        data = data.replaceAll("\\s+", "");
        messageList = message.toString().trim();
        log.info("Success Get Data : {}",message);
    }
}
