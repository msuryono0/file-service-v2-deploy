package com.rni.rni.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.RedisPassword;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.jedis.JedisClientConfiguration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.listener.ChannelTopic;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.data.redis.listener.adapter.MessageListenerAdapter;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import redis.clients.jedis.JedisPoolConfig;

@Configuration
@Component
public class RedisConfig {

    @Value("${spring.redis.host}")
    private String host;

    @Value("${spring.redis.port}")
    private Integer port;

    @Value("${spring.redis.password}")
    private String password;

    @Value("${spring.redis.database}")
    private Integer database;

    /**
     *
     * @return jedispool
     */
    @Bean
    public JedisPoolConfig jedisPoolConfig() {
        JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
        jedisPoolConfig.setMaxTotal(100);
        jedisPoolConfig.setMinIdle(20);
        jedisPoolConfig.setTestOnBorrow(true);
        return jedisPoolConfig;
    }
    /**
     * jedis connection factory
     *
     * @param jedisPoolConfig
     * @return
     */
    @Bean
    public RedisConnectionFactory redisConnectionFactory(JedisPoolConfig jedisPoolConfig) {
        RedisStandaloneConfiguration redisStandaloneConfiguration = new RedisStandaloneConfiguration();
        redisStandaloneConfiguration.setHostName(host);
        redisStandaloneConfiguration.setDatabase(database);
        redisStandaloneConfiguration.setPassword(RedisPassword.of(password));
        redisStandaloneConfiguration.setPort(port);
        //Get the default connection pool constructor (how to design, why not abstract a separate class for users to use)
        JedisClientConfiguration.JedisPoolingClientConfigurationBuilder jpccb =
                (JedisClientConfiguration.JedisPoolingClientConfigurationBuilder) JedisClientConfiguration.builder();
        jpccb.poolConfig(jedisPoolConfig);
        JedisClientConfiguration jedisClientConfiguration = jpccb.build();
        return new JedisConnectionFactory(redisStandaloneConfiguration, jedisClientConfiguration);
    }
    public String chanel;

    @Bean
    public String getChanel() {
        return chanel;
    }

    public void setChanel(String chanel) {
        this.chanel = chanel;
    }

    @Bean
    public RedisTemplate<String, Object> redisTemplate(RedisConnectionFactory connectionFactory) {
        RedisTemplate<String, Object> redisTemplate = new RedisTemplate<>();
        redisTemplate.setConnectionFactory(this.redisConnectionFactory(jedisPoolConfig()));
        redisTemplate.setKeySerializer(new StringRedisSerializer());
        redisTemplate.setHashKeySerializer(new StringRedisSerializer());
        redisTemplate.setHashValueSerializer(new GenericJackson2JsonRedisSerializer());
        redisTemplate.setExposeConnection(true);
        redisTemplate.afterPropertiesSet();
        return redisTemplate;
    }

    @Bean
    MessageListenerAdapter messageListenerAdapter(){
        return new MessageListenerAdapter(new RedisSubscribeConfig(),"onMessage");
    }
    @Bean
    public ChannelTopic channelTopic(){
        this.chanel = ObjectUtils.isEmpty(chanel) ? "servicerni" : this.chanel;
        return new ChannelTopic(this.getChanel());
    }
    @Bean
    RedisMessagePublish messagePublish(String chanel){
        return new RedisPublishConfig(redisTemplate(redisConnectionFactory(this.jedisPoolConfig())),new ChannelTopic(this.getChanel()));
    }
    public RedisMessageListenerContainer redisMessageListenerContainer;
    @Bean
    public RedisMessageListenerContainer container(RedisConnectionFactory redisConnectionFactory, MessageListenerAdapter messageListenerAdapter){
        this.redisMessageListenerContainer = new RedisMessageListenerContainer();
        this.redisMessageListenerContainer.setConnectionFactory(redisConnectionFactory);
        this.redisMessageListenerContainer.addMessageListener(messageListenerAdapter,new ChannelTopic(this.getChanel()));
        return this.redisMessageListenerContainer;
    }
    public void setMessageListener(String chanel){
        var data = this.redisMessageListenerContainer;
        this.redisMessageListenerContainer.addMessageListener(messageListenerAdapter(),new ChannelTopic(chanel));
    }
}
