package com.rni.rni.exception;

public class RequestNotNullException extends RuntimeException {
    private static final long serialVersionUID = 8468398833272788093L;
    public RequestNotNullException(String message) {
        super(message);
    }
}
