package com.rni.rni.exception;

public class SheetNotMatchException extends RuntimeException {
    private static final long serialVersionUID = 8468398833272788093L;
    public SheetNotMatchException(String message) {
        super(message);
    }
}
