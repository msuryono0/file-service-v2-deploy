package com.rni.rni.exception;

public class DataIsBeingUsedException extends RuntimeException {
    private static final long serialVersionUID = 8468398833272788093L;
    public DataIsBeingUsedException(String message) {
        super(message);
    }
}
