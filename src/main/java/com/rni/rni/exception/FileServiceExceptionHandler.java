package com.rni.rni.exception;

import com.rni.rni.constant.ErrorConstant;
import com.rni.rni.domain.dto.ErrorMapingDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
@Slf4j
public class FileServiceExceptionHandler  {

    private ResponseEntity<Object> buildResponseEntity(String code, String message) {
        try {
            ErrorMapingDto errorDetail = ErrorMapingDto.builder()
                    .code(code)
                    .message(message)
                    .build();
            return new ResponseEntity<>(errorDetail, HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            throw e;
        }
    }

    @ExceptionHandler(SheetNotMatchException.class)
    public ResponseEntity<Object> sheetNotMatch(SheetNotMatchException e) {
        log.info("Handling Sheet not match");
        return buildResponseEntity(ErrorConstant.GENERAL_ERROR, e.getMessage());
    }

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<Object> sheetNotMatch(NotFoundException e) {
        log.info("Data not found");
        return buildResponseEntity(ErrorConstant.GENERAL_ERROR, e.getMessage());
    }
    @ExceptionHandler(DataIsBeingUsedException.class)
    public ResponseEntity<Object> dataIsBeingUsed(DataIsBeingUsedException e) {
        log.info("Data is being used");
        return buildResponseEntity(ErrorConstant.GENERAL_ERROR, e.getMessage());
    }
    @ExceptionHandler(RequestNotNullException.class)
    public ResponseEntity<Object> requestNotNull(RequestNotNullException e) {
        log.info("Request Not Null");
        return buildResponseEntity(ErrorConstant.GENERAL_ERROR, e.getMessage());
    }

}
