package com.rni.rni.exception;

public class NotFoundException extends RuntimeException {
    private static final long serialVersionUID = 8468398833272788093L;
    public NotFoundException(String message) {
        super(message);
    }
}
