package com.rni.rni.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rni.rni.constant.Constant;
import com.rni.rni.domain.dao.*;
import com.rni.rni.domain.dto.*;
import com.rni.rni.exception.DataIsBeingUsedException;
import com.rni.rni.repository.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import util.ResponseUtil;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static com.rni.rni.constant.Constant.redisKey.*;
import static com.rni.rni.constant.Constant.responseKey.*;

@Slf4j
@Service
public class DataSetupService {
    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    private UserLoginRepository userLoginRepository;

    @Autowired
    private DimensionRepository dimensionRepository;

    @Autowired
    private DimensionValueRepository dimensionValueRepository;

    @Autowired
    private UomRepository uomRepository;
    @Autowired
    private KaryawanRepository karyawanRepository;

    @Autowired
    private UserLevelRepository userLevelRepository;

    @Autowired
    private CommonService commonService;
    @Autowired
    private PermissionRepository permissionRepository;

    /**
     * @param request
     * @param metadata
     * @return
     * @throws JsonProcessingException
     * Get User Login
     */
    //#region get userlogin
    public ResponseEntity<Object> getUserLogin(CommonRequestDto request, Metadata metadata) throws JsonProcessingException {
        String redisKey = KEY_USERLOGIN;
        String respKey = INQUIRY_SUCCESS;
        List<UserLogin> userLogins = new ArrayList<>();
        UserLoginResponseDto response = new UserLoginResponseDto();
        try {
            if (!ObjectUtils.isEmpty(request.getSearch()))
                userLogins = userLoginRepository.findByUsernameContainingIgnoreCaseOrNameContainingIgnoreCase(request.getSearch(),request.getSearch());
            else {
                String data = (String) redisTemplate.boundValueOps(redisKey).get();
                if (ObjectUtils.isEmpty(data)) {
                    userLogins = commonService.findAllUserLogin();
                } else {
                    userLogins = mapper.readValue(data, new TypeReference<List<UserLogin>>() {
                    });
                }
            }
            PagedListHolder paging = new PagedListHolder(userLogins);
            paging = commonService.setData(paging,request.getSize(),request.getPage());
            response.setUserLogins(paging.getPageList());
            response.setNumberOfElements(paging.getPageList().size());
            response.setPageNumber(paging.getPage());
            response.setTotalPages(paging.getPageCount());
            response.setTotalElements(paging.getNrOfElements());
            return ResponseUtil.buildResponse(
                    SUCCESS,
                    respKey,
                    response,
                    HttpStatus.OK);

        }catch (Exception e){
            log.info("error inquiry userlogin : {}",e.getMessage());
            return ResponseUtil.buildResponse(
                    FAILED,
                    INQUIRY_FAILED,
                    null,
                    HttpStatus.BAD_REQUEST);
        }
    }
    //#endregion
    /**
     *
     * @param requestDto
     * @param metadata
     * @return
     */
    //#region save userlogin
    public ResponseEntity<Object> saveUserLogin(UserLoginRequestDto requestDto,Metadata metadata){
        String redisKey = Constant.redisKey.KEY_USERLOGIN;
        String respKey = SAVE_SUCCESS;
        try {
            UserLogin userLogin = new UserLogin();
            if (!ObjectUtils.isEmpty(requestDto.getId())){
                userLogin = userLoginRepository.findById(requestDto.getId()).get();
                userLogin.setId(userLogin.getId());
                userLogin.setUpdatedAt(new Date());
                userLogin.setUpdatedBy(metadata.getUsername());
            }else{
                userLogin.setCreatedAt(new Date());
                userLogin.setCreatedBy(metadata.getUsername());
            }
            userLogin.setUsername(requestDto.getUsername());
            userLogin.setCabang(ObjectUtils.isEmpty(requestDto.getCabang()) ?userLogin.getCabang() : dimensionRepository.findByCode(requestDto.getCabang()));
            userLogin.setDivisi(ObjectUtils.isEmpty(requestDto.getDivisi()) ? userLogin.getDivisi() :dimensionValueRepository.findByCode(requestDto.getDivisi()));
            userLogin.setIdKaryawan(ObjectUtils.isEmpty(requestDto.getIdKaryawan()) ? userLogin.getIdKaryawan() : karyawanRepository.findById(requestDto.getIdKaryawan()).get());
            userLogin.setLevel(ObjectUtils.isEmpty(requestDto.getLevel()) ? userLogin.getLevel() : userLevelRepository.findById(requestDto.getLevel()).get());
            userLogin.setName(requestDto.getName());

            userLoginRepository.save(userLogin);
            redisTemplate.delete(redisKey);
            return  ResponseUtil.buildResponse(
                    SUCCESS,
                    respKey,
                    null,
                    HttpStatus.OK);
        }catch (Exception e){
            return  ResponseUtil.buildResponse(
                    FAILED,
                    SAVE_FAILED,
                    null,
                    HttpStatus.BAD_GATEWAY);
        }
    }
    //#endregion
    /**
     *
     * @param requestDto
     * @param metadata
     * @return
     */
    //#region delete userlogin
    public ResponseEntity<Object> deleteUserLogin(UserLoginRequestDto requestDto,Metadata metadata){
        String redisKey = Constant.redisKey.KEY_USERLOGIN;
        String respKey = DELETE_SUCCESS;
        try {
            UserLogin userLogin = userLoginRepository.findById(requestDto.getId()).get();
            userLogin.setUpdatedBy(metadata.getUsername());
            userLogin.setUpdatedAt(new Date());
            userLogin.setIsDeleted(true);
            userLoginRepository.save(userLogin);
            redisTemplate.delete(redisKey);
            return  ResponseUtil.buildResponse(
                    SUCCESS,
                    respKey,
                    null,
                    HttpStatus.OK);
        }catch (Exception e){
            return  ResponseUtil.buildResponse(
                    FAILED,
                    DELETE_FAILED,
                    null,
                    HttpStatus.BAD_GATEWAY);
        }
    }
    //#endregion
    /**
     *
     * @param request
     * @param metadata
     * @return
     * @throws JsonProcessingException
     */
    //#region get Cabang
    public ResponseEntity<Object> getCabang(CommonRequestDto request, Metadata metadata) throws JsonProcessingException {
        String redisKey = KEY_CABANG;
        String respKey = INQUIRY_SUCCESS;
        List<Dimension> cabangs = new ArrayList<>();
        CabangResponseDto response = new CabangResponseDto();
        try {
            if (!ObjectUtils.isEmpty(request.getSearch()))
                cabangs = dimensionRepository.findByKeteranganContainingIgnoreCase(request.getSearch());
            else {
                String data = (String) redisTemplate.boundValueOps(redisKey).get();
                if (ObjectUtils.isEmpty(data)) {
                    cabangs = commonService.findAllCabang();
                } else {
                    cabangs = mapper.readValue(data, new TypeReference<List<Dimension>>() {
                    });
                }
            }
            PagedListHolder pagedListHolder = new PagedListHolder<>(cabangs);
            pagedListHolder = commonService.setData(pagedListHolder,request.getSize(), request.getPage());
            response.setDimensions(pagedListHolder.getPageList());
            response.setNumberOfElements(pagedListHolder.getPageList().size());
            response.setPageNumber(pagedListHolder.getPage());
            response.setTotalPages(pagedListHolder.getPageCount());
            response.setTotalElements(pagedListHolder.getNrOfElements());
            return ResponseUtil.buildResponse(
                    SUCCESS,
                    respKey,
                    response,
                    HttpStatus.OK);

        }catch (Exception e){
            log.info("error inquiry cabang : {}",e.getMessage());
            return ResponseUtil.buildResponse(
                    FAILED,
                    INQUIRY_FAILED,
                    null,
                    HttpStatus.BAD_REQUEST);
        }
    }
    //#endregion
    /**
     *
     * @param requestDto
     * @param metadata
     * @return
     */
    //#region save cabang
    public ResponseEntity<Object> saveCabang(CabangRequestDto requestDto,Metadata metadata){
        String redisKey = KEY_CABANG;
        String respKey = SAVE_SUCCESS;
        try {
            Dimension dimension = new Dimension();
            if (!ObjectUtils.isEmpty(requestDto.getId())){
                dimension = dimensionRepository.findById(requestDto.getId()).get();

                if (!ObjectUtils.isEmpty(requestDto.getCode()) && !ObjectUtils.isEmpty(dimensionValueRepository.findByDimensionCode(Dimension.builder().code(requestDto.getCode()).build())))
                    throw new DataIsBeingUsedException("Data masih digunakan !");
                dimension.setId(dimension.getId());
                dimension.setUpdatedAt(new Date());
                dimension.setUpdatedBy(metadata.getUsername());
            }else{
                dimension.setCreatedAt(new Date());
                dimension.setCreatedBy(metadata.getUsername());
            }
            dimension.setKeterangan(requestDto.getKeterangan());
            dimensionRepository.save(dimension);
            redisTemplate.delete(redisKey);
            return  ResponseUtil.buildResponse(
                    SUCCESS,
                    respKey,
                    null,
                    HttpStatus.OK);
        }catch (Exception e){
            return  ResponseUtil.buildResponse(
                    FAILED,
                    SAVE_FAILED,
                    null,
                    HttpStatus.BAD_GATEWAY);
        }
    }
    //#endregion
    /**
     *
     * @param requestDto
     * @param metadata
     * @return
     */
    //#region delete cabang
    public ResponseEntity<Object> deleteCabang(CabangRequestDto requestDto,Metadata metadata){
        String redisKey = "CABANG";
        String respKey = DELETE_SUCCESS;
        try {
            Dimension dimension = dimensionRepository.findById(requestDto.getId()).get();
            if (!ObjectUtils.isEmpty(dimensionValueRepository.findByDimensionCode(Dimension.builder().code(dimension.getCode()).build())))
                throw new DataIsBeingUsedException("Data masih digunakan");
            dimension.setUpdatedBy(metadata.getUsername());
            dimension.setUpdatedAt(new Date());
            dimension.setIsDeleted(true);
            dimensionRepository.save(dimension);
            redisTemplate.delete(redisKey);
            return  ResponseUtil.buildResponse(
                    SUCCESS,
                    respKey,
                    null,
                    HttpStatus.OK);
        }catch (Exception e){
            return  ResponseUtil.buildResponse(
                    FAILED,
                    DELETE_FAILED,
                    null,
                    HttpStatus.BAD_GATEWAY);
        }
    }
    //#endregion
    /**
     *
     * @param request
     * @param metadata
     * @return
     * @throws JsonProcessingException
     */
    //#region getunit
    public ResponseEntity<Object> getUnit(CommonRequestDto request, Metadata metadata) throws JsonProcessingException {
        String redisKey = KEY_UNIT;
        String respKey = INQUIRY_SUCCESS;
        List<DimensionValue> units = new ArrayList<>();
        UnitResponseDto response = new UnitResponseDto();
        try {
            if (!ObjectUtils.isEmpty(request.getSearch()) || !ObjectUtils.isEmpty(request.getCodeSearch())) {
                Dimension dimension = dimensionRepository.findByCode(request.getCodeSearch());
                if (!ObjectUtils.isEmpty(dimension)){
                   units = dimensionValueRepository.findByDimensionCode(dimension);
                   if (!ObjectUtils.isEmpty(request.getSearch()))
                        units = units.stream()
                                .filter(x-> x.getCode().contains(request.getSearch()) || x.getKeterangan().contains(request.getSearch()))
                                .collect(Collectors.toList());
                }else{
                    units = dimensionValueRepository.findByKeteranganContainingIgnoreCaseOrCode(
                            request.getSearch(),
                            request.getSearch());
                }
            }else {
                String data = (String) redisTemplate.boundValueOps(redisKey).get();
                if (ObjectUtils.isEmpty(data)) {
                    units = commonService.findAllUnit();
                } else {
                    units = mapper.readValue(data, new TypeReference<List<DimensionValue>>() {
                    });
                }
            }
            PagedListHolder pagedListHolder = new PagedListHolder<>(units);
            pagedListHolder = commonService.setData(pagedListHolder,request.getSize(), request.getPage());
            response.setDimensionValues(pagedListHolder.getPageList());
            response.setNumberOfElements(pagedListHolder.getPageList().size());
            response.setPageNumber(pagedListHolder.getPage());
            response.setTotalPages(pagedListHolder.getPageCount());
            response.setTotalElements(pagedListHolder.getNrOfElements());
            return ResponseUtil.buildResponse(
                    SUCCESS,
                    respKey,
                    response,
                    HttpStatus.OK);

        }catch (Exception e){
            log.info("error inquiry cabang : {}",e.getMessage());
            return ResponseUtil.buildResponse(
                    FAILED,
                    INQUIRY_FAILED,
                    null,
                    HttpStatus.BAD_REQUEST);
        }
    }
    //#endregion
    /**
     *
     * @param requestDto
     * @param metadata
     * @return
     */
    //#region saveUnit
    public ResponseEntity<Object> saveUnit(UnitRequestDto requestDto,Metadata metadata){
        String redisKey = KEY_UNIT;
        String respKey = SAVE_SUCCESS;
        try {
            DimensionValue dimensionValue = new DimensionValue();
            if (!ObjectUtils.isEmpty(requestDto.getId())){
                dimensionValue = dimensionValueRepository.findById(requestDto.getId()).get();

                if (!ObjectUtils.isEmpty(requestDto.getCode()) && !ObjectUtils.isEmpty(userLoginRepository.findByDivisi(dimensionValue)))
                    throw new DataIsBeingUsedException("Data masih digunakan !");
                dimensionValue.setId(dimensionValue.getId());
                dimensionValue.setUpdatedAt(new Date());
                dimensionValue.setUpdatedBy(metadata.getUsername());
            }else{
                dimensionValue.setCreatedAt(new Date());
                dimensionValue.setCreatedBy(metadata.getUsername());
            }
            dimensionValue.setKeterangan(requestDto.getKeterangan());
            dimensionValueRepository.save(dimensionValue);
            redisTemplate.delete(redisKey);
            return  ResponseUtil.buildResponse(
                    SUCCESS,
                    respKey,
                    null,
                    HttpStatus.OK);
        }catch (Exception e){
            return  ResponseUtil.buildResponse(
                    FAILED,
                    SAVE_FAILED,
                    null,
                    HttpStatus.BAD_GATEWAY);
        }
    }
    //#endregion

    /**
     * @apiNote delete Unit
     * @param requestDto
     * @param metadata
     * @return
     */
   //#region deleteunit
    public ResponseEntity<Object> deleteUnit(UnitRequestDto requestDto,Metadata metadata){
        String redisKey = KEY_UNIT;
        String respKey = DELETE_SUCCESS;
        try {
            DimensionValue dimensionValue = dimensionValueRepository.findById(requestDto.getId()).get();
            if (!ObjectUtils.isEmpty(userLoginRepository.findByDivisi(dimensionValue)))
                throw new DataIsBeingUsedException("Data masih digunakan");
            dimensionValue.setUpdatedBy(metadata.getUsername());
            dimensionValue.setUpdatedAt(new Date());
            dimensionValue.setDeleted(true);
            dimensionValueRepository.save(dimensionValue);
            redisTemplate.delete(redisKey);
            return  ResponseUtil.buildResponse(
                    SUCCESS,
                    respKey,
                    null,
                    HttpStatus.OK);
        }catch (Exception e){
            return  ResponseUtil.buildResponse(
                    FAILED,
                    DELETE_FAILED,
                    null,
                    HttpStatus.BAD_GATEWAY);
        }
    }
    //#endregion
    /**
     *
     * @param request
     * @param metadata
     * @return
     * @throws JsonProcessingException
     */
    //#region get UOM
    public ResponseEntity<Object> getUom(CommonRequestDto request, Metadata metadata) throws JsonProcessingException {
        String redisKey = KEY_UOM;
        String respKey = INQUIRY_SUCCESS;
        List<Uom> uoms = new ArrayList<>();
        UomResponseDto response = new UomResponseDto();
        try {
            if (!ObjectUtils.isEmpty(request.getSearch()))
                uoms = uomRepository.findByKeteranganContainingIgnoreCase(request.getSearch());
            else {
                String data = (String) redisTemplate.boundValueOps(redisKey).get();
                if (ObjectUtils.isEmpty(data)) {
                    uoms = commonService.findAllUom();
                } else {
                    uoms = mapper.readValue(data, new TypeReference<List<Uom>>() {
                    });
                }
            }
            PagedListHolder pagedListHolder = new PagedListHolder<>(uoms);
            pagedListHolder = commonService.setData(pagedListHolder,request.getSize(), request.getPage());
            response.setUoms(pagedListHolder.getPageList());
            response.setNumberOfElements(pagedListHolder.getPageList().size());
            response.setPageNumber(pagedListHolder.getPage());
            response.setTotalPages(pagedListHolder.getPageCount());
            response.setTotalElements(pagedListHolder.getNrOfElements());
            return ResponseUtil.buildResponse(
                    SUCCESS,
                    respKey,
                    response,
                    HttpStatus.OK);

        }catch (Exception e){
            log.info("error inquiry cabang : {}",e.getMessage());
            return ResponseUtil.buildResponse(
                    FAILED,
                    INQUIRY_FAILED,
                    null,
                    HttpStatus.BAD_REQUEST);
        }
    }
    //#endregion
    /**
     *
     * @param requestDto
     * @param metadata
     * @return
     */
    //#region save UOM
    public ResponseEntity<Object> saveUOM(UomRequestDto requestDto,Metadata metadata){
        String redisKey = KEY_UOM;
        String respKey = SAVE_SUCCESS;
        try {
            Uom uom = new Uom();
            if (!ObjectUtils.isEmpty(requestDto.getId())){
                uom = uomRepository.findById(requestDto.getId()).get();
                uom.setId(uom.getId());
                uom.setUpdatedAt(new Date());
                uom.setUpdatedBy(metadata.getUsername());
            }else{
                uom.setCreatedAt(new Date());
                uom.setCreatedBy(metadata.getUsername());
            }
            uom.setKeterangan(requestDto.getKeterangan());
            uomRepository.save(uom);
            redisTemplate.delete(redisKey);
            return  ResponseUtil.buildResponse(
                    SUCCESS,
                    respKey,
                    null,
                    HttpStatus.OK);
        }catch (Exception e){
            return  ResponseUtil.buildResponse(
                    FAILED,
                    SAVE_FAILED,
                    null,
                    HttpStatus.BAD_GATEWAY);
        }
    }
    //#endregion

    /**
     * @apiNote delete UOM
     * @param requestDto
     * @param metadata
     * @return
     */
    //#region deleteunit
    public ResponseEntity<Object> deleteUOM(UomRequestDto requestDto,Metadata metadata){
        String redisKey = KEY_UOM;
        String respKey = DELETE_SUCCESS;
        try {
            Uom uom = uomRepository.findById(requestDto.getId()).get();
            uom.setUpdatedBy(metadata.getUsername());
            uom.setUpdatedAt(new Date());
            uom.setIsDeleted(true);
            uomRepository.save(uom);
            redisTemplate.delete(redisKey);
            return  ResponseUtil.buildResponse(
                    SUCCESS,
                    respKey,
                    null,
                    HttpStatus.OK);
        }catch (Exception e){
            return  ResponseUtil.buildResponse(
                    FAILED,
                    DELETE_FAILED,
                    null,
                    HttpStatus.BAD_GATEWAY);
        }
    }
    //#endregion
    /**
     *
     * @param request
     * @param metadata
     * @return
     * @throws JsonProcessingException
     */
    //#region get Permssion
    public ResponseEntity<Object> getPermission(CommonRequestDto request, Metadata metadata) throws JsonProcessingException {
        String redisKey = KEY_PERMISSION;
        String respKey = INQUIRY_SUCCESS;
        List<Permission> permissions = new ArrayList<>();
        PermissionResponseDto response = new PermissionResponseDto();
        try {
            if (!ObjectUtils.isEmpty(request.getSearch()))
                permissions = permissionRepository.findPermission(request.getSearch());
            else {
                String data = (String) redisTemplate.boundValueOps(redisKey).get();
                if (ObjectUtils.isEmpty(data) || data.equalsIgnoreCase("[]")) {
                    permissions = commonService.findAllPermission();
                } else {
                    permissions = mapper.readValue(data, new TypeReference<List<Permission>>() {
                    });
                }
            }
            PagedListHolder pagedListHolder = new PagedListHolder<>(permissions);
            pagedListHolder = commonService.setData(pagedListHolder,request.getSize(), request.getPage());
            response.setPermissions(pagedListHolder.getPageList());
            response.setNumberOfElements(pagedListHolder.getPageList().size());
            response.setPageNumber(pagedListHolder.getPage());
            response.setTotalPages(pagedListHolder.getPageCount());
            response.setTotalElements(pagedListHolder.getNrOfElements());
            return ResponseUtil.buildResponse(
                    SUCCESS,
                    respKey,
                    response,
                    HttpStatus.OK);

        }catch (Exception e){
            log.info("error inquiry cabang : {}",e.getMessage());
            return ResponseUtil.buildResponse(
                    FAILED,
                    INQUIRY_FAILED,
                    null,
                    HttpStatus.BAD_REQUEST);
        }
    }
    //#endregion
    /**
     *
     * @param requestDto
     * @param metadata
     * @return
     */
    //#region save Permission
    public ResponseEntity<Object> savePermission(PermissionRequestDto requestDto,Metadata metadata){
        String redisKey = KEY_PERMISSION;
        String respKey = SAVE_SUCCESS;
        try {
            Permission permission = new Permission();
            if (!ObjectUtils.isEmpty(requestDto.getId())){
                permission = permissionRepository.findById(requestDto.getId()).get();
                permission.setId(permission.getId());
                permission.setUpdatedAt(new Date());
                permission.setUpdatedBy(metadata.getUsername());
            }else{
                permission.setCreatedAt(new Date());
                permission.setCreatedBy(metadata.getUsername());
            }
            permission.setMenuCode(requestDto.getMenu());
            permission.setSubMenuCode(requestDto.getMenuSub());
            permission.setLevelMenuCode(requestDto.getMenuLevel());
            permission.setPermission(requestDto.getUserLevel());
            permissionRepository.save(permission);
            redisTemplate.delete(redisKey);
            return  ResponseUtil.buildResponse(
                    SUCCESS,
                    respKey,
                    null,
                    HttpStatus.OK);
        }catch (Exception e){
            return  ResponseUtil.buildResponse(
                    FAILED,
                    SAVE_FAILED,
                    null,
                    HttpStatus.BAD_GATEWAY);
        }
    }
    //#endregion

    /**
     * @apiNote delete UOM
     * @param requestDto
     * @param metadata
     * @return
     */
    //#region delete Permission
    public ResponseEntity<Object> deletePermission(PermissionRequestDto requestDto,Metadata metadata){
        String redisKey = KEY_PERMISSION;
        String respKey = DELETE_SUCCESS;
        try {
            Permission permission = permissionRepository.findById(requestDto.getId()).get();
            permission.setUpdatedBy(metadata.getUsername());
            permission.setUpdatedAt(new Date());
            permission.setIsDeleted(true);
            permissionRepository.save(permission);
            redisTemplate.delete(redisKey);
            return  ResponseUtil.buildResponse(
                    SUCCESS,
                    respKey,
                    null,
                    HttpStatus.OK);
        }catch (Exception e){
            return  ResponseUtil.buildResponse(
                    FAILED,
                    DELETE_FAILED,
                    null,
                    HttpStatus.BAD_GATEWAY);
        }
    }
    //#endregion
}
