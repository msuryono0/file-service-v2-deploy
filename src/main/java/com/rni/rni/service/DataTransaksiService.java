package com.rni.rni.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rni.rni.domain.dao.AP9;
import com.rni.rni.domain.dao.TbPjBeras;
import com.rni.rni.domain.dao.TransactionEntryHcOmzet;
import com.rni.rni.domain.dto.*;
import com.rni.rni.exception.RequestNotNullException;
import com.rni.rni.repository.Ap9Repository;
import com.rni.rni.repository.TbPjBerasRepository;
import com.rni.rni.repository.TransactionHcOmzetRepository;
import com.rni.rni.utils.ConvertionUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import util.ResponseUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

import static com.rni.rni.constant.Constant.chanelKey.CN_TRAN_HC_OMZET;
import static com.rni.rni.constant.Constant.chanelKey.CN_TRAN_HC_OMZET_MASTER;
import static com.rni.rni.constant.Constant.redisKey.KEY_AP9;
import static com.rni.rni.constant.Constant.redisKey.KEY_BERAS;
import static com.rni.rni.constant.Constant.responseKey.*;

@Slf4j
@Service
public class DataTransaksiService {

    @Autowired
    private TbPjBerasRepository pjBerasRepository;

    @Autowired
    private TransactionHcOmzetRepository transactionHcOmzetRepository;

    @Autowired
    private Ap9Repository ap9Repository;

    @Autowired
    private CommonService commonService;

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private ObjectMapper mapper;

    public ResponseEntity<Object> getBeras(CoaRequestDto requestDto, Metadata metadata) throws JsonProcessingException {
        List<TbPjBeras> tbPjBeras = new ArrayList<>();
        PjBerasResonseDto pjBerasResonseDto = new PjBerasResonseDto();
        try {
            if (!ObjectUtils.isEmpty(requestDto.getSearch()))
                tbPjBeras = pjBerasRepository.findByIdKaryawan_NamaContainingIgnoreCase(requestDto.getSearch());
            else {
                String data = (String) redisTemplate.boundValueOps(KEY_BERAS).get();
                if (ObjectUtils.isEmpty(data)) {
                    tbPjBeras = commonService.findBeras();
                } else {
                    tbPjBeras = mapper.readValue(data, new TypeReference<List<TbPjBeras>>() {
                    });
                }
            }
            PagedListHolder paging = new PagedListHolder(tbPjBeras);
            paging = commonService.setData(paging, requestDto.getLimit(), requestDto.getPage());
            pjBerasResonseDto.setTotalPage(paging.getPageCount());
            pjBerasResonseDto.setNumberOfElement(paging.getPageList().size());
            pjBerasResonseDto.setPageNumber(tbPjBeras.size());
            pjBerasResonseDto.setTotalElement(paging.getNrOfElements());
            pjBerasResonseDto.setTbPjBeras(paging.getPageList());
            return ResponseUtil.buildResponse(
                    SUCCESS,
                    "SUCCESS GET DATA",
                    pjBerasResonseDto,
                    HttpStatus.OK);
        } catch (Exception e) {
            throw e;
        }
    }
    public ResponseEntity<Object> getAp9(CommonRequestDto requestDto, Metadata metadata) throws JsonProcessingException {
        List<AP9> listTable = new ArrayList<>();
        Ap9ResponseDto response = new Ap9ResponseDto();
        try {
            if (!ObjectUtils.isEmpty(requestDto.getSearch()) || !ObjectUtils.isEmpty(requestDto.getCodeSearch()))
                listTable = ap9Repository.findAllAp9(requestDto.getCodeSearch(), requestDto.getSearch());
            else {
                String data = (String) redisTemplate.boundValueOps(KEY_AP9).get();
                if (ObjectUtils.isEmpty(data)) {
                    listTable = commonService.findAp9();
                } else {
                    listTable = mapper.readValue(data, new TypeReference<List<AP9>>() {
                    });
                }
            }
            PagedListHolder paging = new PagedListHolder(listTable);
            paging = commonService.setData(paging, requestDto.getSize(), requestDto.getPage());
            response.setTotalPage(paging.getPageCount());
            response.setNumberOfElement(paging.getPageList().size());
            response.setPageNumber(listTable.size());
            response.setTotalElement(paging.getNrOfElements());
            response.setAp9s(paging.getPageList());
            return ResponseUtil.buildResponse(
                    SUCCESS,
                    INQUIRY_SUCCESS,
                    response,
                    HttpStatus.OK);
        } catch (Exception e) {
            return ResponseUtil.buildResponse(
                    FAILED,
                    INQUIRY_FAILED,
                    response,
                    HttpStatus.BAD_REQUEST);
        }
    }

    public ResponseEntity<Object> getOmzetHc(CommonRequestDto requestDto, Metadata metadata) {
        List<TransactionEntryHcOmzet> listTable = new ArrayList<>();
        TransactionOmzetHcResponseDto responseFinal = new TransactionOmzetHcResponseDto();
        try {
            if (ObjectUtils.isEmpty(requestDto.getTahun()) || ObjectUtils.isEmpty(requestDto.getPeriode())){
                throw new RequestNotNullException("Request Tahun & Periode must be not null !");
            }
            if (!ObjectUtils.isEmpty(requestDto.getSearch()) || !ObjectUtils.isEmpty(requestDto.getCodeSearch())
                    || !ObjectUtils.isEmpty(requestDto.getPeriode()) || !ObjectUtils.isEmpty(requestDto.getTahun())) {
                listTable = transactionHcOmzetRepository.findByYearAndCabangDivisi_CodeAndPeriode(requestDto.getTahun(), metadata.getUnit(), requestDto.getPeriode());
            }else {
                String data = (String) redisTemplate.boundValueOps(CN_TRAN_HC_OMZET).get();
                if (ObjectUtils.isEmpty(data)) {
                    listTable = commonService.findOmzet();
                } else {
                    listTable = mapper.readValue(data, new TypeReference<List<TransactionEntryHcOmzet>>() {
                    });
                }
            }
            AtomicReference<TRansactionOmzetSumDto> tRansactionOmzetSumDto = new AtomicReference<>(new TRansactionOmzetSumDto());
            List<TransactionHCOmzetDto> resstream = new ArrayList<>();
            listTable.stream().forEach(x->{

                if (!Objects.equals(x.getFormula(), "")) {
                    resstream.add(sumTotal(tRansactionOmzetSumDto.get(), x));
                    tRansactionOmzetSumDto.set(new TRansactionOmzetSumDto());
                    return;
                }
                TransactionHCOmzetDto res = new TransactionHCOmzetDto();
                res.setId(x.getId());
                res.setProduct(x.getProduct());
                res.setCabangDivisi(x.getCabangDivisi());
                res.setPeriode(x.getPeriode());
                res.setYear(x.getYear());
                res.setKeterangan(x.getKeterangan());
                res.setGroupProd(x.getGroupProd());
                res.setRowNumber(x.getRowNumber());
                res.setJan(ObjectUtils.isEmpty(x.getJan()) ? 0L : Long.valueOf(x.getJan().replaceAll(",", "")));
                res.setFeb(ObjectUtils.isEmpty(x.getFeb()) ? 0L : Long.valueOf(x.getFeb().replaceAll(",", "")));
                res.setMar(ObjectUtils.isEmpty(x.getMar()) ? 0L : Long.valueOf(x.getMar().replaceAll(",", "")));
                res.setTwi(res.getMar()+res.getJan()+res.getFeb());
                res.setApr(ObjectUtils.isEmpty(x.getApr()) ? 0L : Long.valueOf(x.getApr().replaceAll(",", "")));
                res.setMei(ObjectUtils.isEmpty(x.getMei()) ? 0L : Long.valueOf(x.getMei().replaceAll(",", "")));
                res.setJun(ObjectUtils.isEmpty(x.getJun()) ? 0L : Long.valueOf(x.getJun().replaceAll(",", "")));
                res.setTwii(res.getApr()+res.getMei()+res.getJun());
                res.setJul(ObjectUtils.isEmpty(x.getJul()) ? 0L : Long.valueOf(x.getJul().replaceAll(",", "")));
                res.setAgt(ObjectUtils.isEmpty(x.getAgt()) ? 0L : Long.valueOf(x.getAgt().replaceAll(",", "")));
                res.setSep(ObjectUtils.isEmpty(x.getSep()) ? 0L : Long.valueOf(x.getSep().replaceAll(",", "")));
                res.setTwiii(res.getJul()+res.getAgt()+res.getSep());
                res.setOkt(ObjectUtils.isEmpty(x.getOkt()) ? 0L : Long.valueOf(x.getOkt().replaceAll(",", "")));
                res.setNov(ObjectUtils.isEmpty(x.getNov()) ? 0L : Long.valueOf(x.getNov().replaceAll(",", "")));
                res.setDes(ObjectUtils.isEmpty(x.getDes()) ? 0L : Long.valueOf(x.getDes().replaceAll(",", "")));
                res.setTwiv(res.getOkt()+res.getNov()+res.getDes());
                res.setTtl(res.getTwi()+res.getTwii()+res.getTwiii()+res.getTwiv());

                if (!Objects.equals(x.getRowNumber(), "")) {
                    tRansactionOmzetSumDto.set(sumTotalTransactionByRowNumber(tRansactionOmzetSumDto.get(), x));
                }

                resstream.add(res);
            });
            responseFinal.setTransactionEntryHcOmzets(resstream);
            PagedListHolder paging = new PagedListHolder(resstream);
            paging = commonService.setData(paging, requestDto.getSize(), requestDto.getPage());
            responseFinal.setTotalPage(paging.getPageCount());
            responseFinal.setNumberOfElement(paging.getPageList().size());
            responseFinal.setPageNumber(resstream.size());
            responseFinal.setTotalElement(paging.getNrOfElements());
            responseFinal.setTransactionEntryHcOmzets(paging.getPageList());
            return ResponseUtil.buildResponse(
                    SUCCESS,
                    INQUIRY_SUCCESS,
                    responseFinal,
                    HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return ResponseUtil.buildResponse(
                    FAILED,
                    INQUIRY_FAILED,
                    responseFinal,
                    HttpStatus.BAD_REQUEST);
        }
    }
    public ResponseEntity<Object> getOmzetMasterHc() {
        List<TransactionEntryHcOmzet> listTable = new ArrayList<>();
        TransactionOmzetHcMasterResponseDto responseFinal = new TransactionOmzetHcMasterResponseDto();
        try {
                String data = (String) redisTemplate.boundValueOps(CN_TRAN_HC_OMZET_MASTER).get();
                if (ObjectUtils.isEmpty(data)) {
                    listTable = commonService.findOmzet();
                } else {
                    listTable = mapper.readValue(data, new TypeReference<List<TransactionEntryHcOmzet>>() {
                    });
                }
            responseFinal.setTransactionEntryHcOmzets(listTable);
            return ResponseUtil.buildResponse(
                    SUCCESS,
                    INQUIRY_SUCCESS,
                    responseFinal,
                    HttpStatus.OK);
        } catch (Exception e) {
            return ResponseUtil.buildResponse(
                    FAILED,
                    INQUIRY_FAILED,
                    responseFinal,
                    HttpStatus.BAD_REQUEST);
        }
    }

    private TransactionHCOmzetDto sumTotal(TRansactionOmzetSumDto tRansactionOmzetSumDto, TransactionEntryHcOmzet x) {
        TransactionHCOmzetDto res = new TransactionHCOmzetDto();
        res.setId(x.getId());
        res.setProduct(x.getProduct());
        res.setCabangDivisi(x.getCabangDivisi());
        res.setPeriode(x.getPeriode());
        res.setYear(x.getYear());
        res.setKeterangan(x.getKeterangan());
        res.setGroupProd(x.getGroupProd());
        res.setRowNumber(x.getRowNumber());
        res.setJan(tRansactionOmzetSumDto.getJan());
        res.setFeb(tRansactionOmzetSumDto.getFeb());
        res.setMar(tRansactionOmzetSumDto.getMar());
        res.setTwi(res.getMar()+res.getJan()+res.getFeb());
        res.setApr(tRansactionOmzetSumDto.getApr());
        res.setMei(tRansactionOmzetSumDto.getMei());
        res.setJun(tRansactionOmzetSumDto.getJun());
        res.setTwii(res.getApr()+res.getMei()+res.getJun());
        res.setJul(tRansactionOmzetSumDto.getJul());
        res.setAgt(tRansactionOmzetSumDto.getAgt());
        res.setSep(tRansactionOmzetSumDto.getSep());
        res.setTwiii(res.getJul()+res.getAgt()+res.getSep());
        res.setOkt(tRansactionOmzetSumDto.getOkt());
        res.setNov(tRansactionOmzetSumDto.getNov());
        res.setDes(tRansactionOmzetSumDto.getDes());
        res.setTwiv(res.getOkt()+res.getNov()+res.getDes());
        res.setTtl(res.getTwi()+res.getTwii()+res.getTwiii()+res.getTwiv());
        return res;
    }

    private TRansactionOmzetSumDto sumTotalTransactionByRowNumber(TRansactionOmzetSumDto tRansactionOmzetSumDto, TransactionEntryHcOmzet x) {
        TRansactionOmzetSumDto res = new TRansactionOmzetSumDto();
        res.setJan(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getJan()) + Long.parseLong(Objects.equals(x.getJan(), "") ? "0" : x.getJan()));
        res.setFeb(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getFeb()) + Long.parseLong(Objects.equals(x.getFeb(), "") ? "0" : x.getFeb()));
        res.setMar(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getMar()) + Long.parseLong(Objects.equals(x.getMar(), "") ? "0" : x.getMar()));
        res.setApr(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getApr()) + Long.parseLong(Objects.equals(x.getApr(), "") ? "0" : x.getApr()));
        res.setMei(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getMei()) + Long.parseLong(Objects.equals(x.getMei(), "") ? "0" : x.getMei()));
        res.setJun(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getJun()) + Long.parseLong(Objects.equals(x.getJun(), "") ? "0" : x.getJun()));
        res.setJul(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getJul()) + Long.parseLong(Objects.equals(x.getJul(), "") ? "0" : x.getJul()));
        res.setAgt(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getAgt()) + Long.parseLong(Objects.equals(x.getAgt(), "") ? "0" : x.getAgt()));
        res.setSep(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getSep()) + Long.parseLong(Objects.equals(x.getSep(), "") ? "0" : x.getSep()));
        res.setOkt(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getOkt()) + Long.parseLong(Objects.equals(x.getOkt(), "") ? "0" : x.getOkt()));
        res.setNov(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getNov()) + Long.parseLong(Objects.equals(x.getNov(), "") ? "0" : x.getNov()));
        res.setDes(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getDes()) + Long.parseLong(Objects.equals(x.getDes(), "") ? "0" : x.getDes()));
        return res;
    }

    public ResponseEntity<Object> updateRowHcOmzet(TransactionHCOmzetDto transactionHCOmzetDto) {

        Optional<TransactionEntryHcOmzet> transactionEntryHcOmzet = transactionHcOmzetRepository.findById(transactionHCOmzetDto.getId());
        if (transactionEntryHcOmzet.isPresent()) {
            TransactionEntryHcOmzet transactionEntryHcOmzet1 = transactionEntryHcOmzet.get();
            transactionEntryHcOmzet1.setJan(transactionHCOmzetDto.getJan().toString());
            transactionEntryHcOmzet1.setFeb(transactionHCOmzetDto.getFeb().toString());
            transactionEntryHcOmzet1.setMar(transactionHCOmzetDto.getMar().toString());
            transactionEntryHcOmzet1.setApr(transactionHCOmzetDto.getApr().toString());
            transactionEntryHcOmzet1.setMei(transactionHCOmzetDto.getMei().toString());
            transactionEntryHcOmzet1.setJun(transactionHCOmzetDto.getJun().toString());
            transactionEntryHcOmzet1.setJul(transactionHCOmzetDto.getJul().toString());
            transactionEntryHcOmzet1.setAgt(transactionHCOmzetDto.getAgt().toString());
            transactionEntryHcOmzet1.setSep(transactionHCOmzetDto.getSep().toString());
            transactionEntryHcOmzet1.setOkt(transactionHCOmzetDto.getOkt().toString());
            transactionEntryHcOmzet1.setNov(transactionHCOmzetDto.getNov().toString());
            transactionEntryHcOmzet1.setDes(transactionHCOmzetDto.getDes().toString());
            transactionHcOmzetRepository.save(transactionEntryHcOmzet1);
            return ResponseUtil.buildResponse(
                    SUCCESS,
                    INQUIRY_SUCCESS,
                    transactionHCOmzetDto,
                    HttpStatus.OK);
        }


        return ResponseUtil.buildResponse(
                FAILED,
                INQUIRY_FAILED,
                null,
                HttpStatus.BAD_REQUEST);
    }
}
