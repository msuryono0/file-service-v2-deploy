package com.rni.rni.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rni.rni.domain.dao.*;
import com.rni.rni.domain.dto.CommonDto;
import com.rni.rni.domain.dto.Metadata;
import com.rni.rni.repository.*;
import com.rni.rni.utils.GeneralUtils;
import com.rni.rni.utils.InsertLogMonitoring;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import util.ResponseUtil;

import javax.transaction.Transactional;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import static com.rni.rni.constant.Constant.responseKey.SUCCESS;
import static com.rni.rni.constant.Constant.responseKey.UPLOADING;

@Slf4j
@Service
public class ProductService {

    @Autowired
    private InsertLogMonitoring insertLogMonitoring;

    @Autowired
    private TableMasterSchedulerRepository tableMasterSchedulerRepository;

    @Autowired
    private UomRepository uomRepository;

    @Autowired
    private GroupProdRepository groupProdRepository;
    @Autowired
    private GroupItemRepository groupItemRepository;
    @Autowired
    private SupplierRepository supplierRepository;

    @Autowired
    private ProdCategoryRepository prodCategoryRepository;

    @Autowired
    private TypeProdRepository typeProdRepository;

    @Transactional
    public ResponseEntity<Object> prodGroup(Workbook workbook, Sheet sheet, Long size, Metadata metadata) throws IOException {
        log.info("star upload product group");
        List<GroupItem> groupItemSave = new ArrayList<>();
        TableMasterScheduler tableMasterScheduler = new TableMasterScheduler();
        CommonDto commonDto = new CommonDto();
        try {
            Iterator<Row> rows = sheet.iterator();
            int rowNumber = 0;
            while (rows.hasNext()) {
                GroupItem groupItem = new GroupItem();
                Row currentRow = rows.next();
                if (rowNumber == 0) {
                    rowNumber++;
                    continue;
                }
                Iterator<Cell> cellInRow = currentRow.iterator();
                int cellIdx = 0;
                while (cellInRow.hasNext()) {
                    Cell currentCell = cellInRow.next();
                    switch (cellIdx) {
                        case 0:
                            groupItem.setGroupId(currentCell.getStringCellValue());
                            break;
                        case 1:
                            groupItem.setGroupName(currentCell.getStringCellValue());
                            break;
                        default:
                            break;
                    }
                    cellIdx++;
                }
                groupItemSave.add(groupItem);
            }
            workbook.close();
            groupItemSave.stream().forEach(x -> {
                x.setCreatedBy(metadata.getUsername());
                x.setCreatedAt(new Date());
            });
            var logId = insertLogMonitoring.logMonitoring(null, sheet.getSheetName(), "Uploading", size, groupItemSave.stream().count());
            commonDto.setGroupItems(groupItemSave);
            tableMasterScheduler.setTemplateName(sheet.getSheetName());
            tableMasterScheduler.setClassName(GroupItem.class.getCanonicalName());
            tableMasterScheduler.setData(new ObjectMapper().writeValueAsString(commonDto));
            tableMasterScheduler.setIdLog(logId);
            tableMasterSchedulerRepository.save(tableMasterScheduler);
            return ResponseUtil.buildResponse(
                    SUCCESS,
                    UPLOADING,
                    null,
                    HttpStatus.OK);
        } catch (Exception e) {
            log.info("error upload group product : {}",e.getMessage());
            throw e;
        }
    }
    @Transactional
    public ResponseEntity<Object> prodClass(Workbook workbook, Sheet sheet, Long size,Metadata metadata) throws IOException {
        log.info("star upload product class");
        List<ClassProd> classProdSave = new ArrayList<>();
        TableMasterScheduler tableMasterScheduler = new TableMasterScheduler();
        CommonDto commonDto = new CommonDto();
        try {
            Iterator<Row> rows = sheet.iterator();
            int rowNumber = 0;
            while (rows.hasNext()) {
                ClassProd classProd = new ClassProd();
                Row currentRow = rows.next();
                if (rowNumber == 0) {
                    rowNumber++;
                    continue;
                }
                Iterator<Cell> cellInRow = currentRow.iterator();
                int cellIdx = 0;
                while (cellInRow.hasNext()) {
                    Cell currentCell = cellInRow.next();
                    switch (cellIdx) {
                        case 0:
                            classProd.setClassId(currentCell.getStringCellValue());
                            break;
                        case 1:
                            classProd.setClassName(currentCell.getStringCellValue());
                            break;
                        default:
                            break;
                    }
                    cellIdx++;
                }
                classProdSave.add(classProd);
            }
            workbook.close();
            classProdSave.stream().forEach(x -> {
                x.setCreatedBy(metadata.getUsername());
                x.setCreatedAt(new Date());
            });
            var logId = insertLogMonitoring.logMonitoring(null, sheet.getSheetName(), "Uploading", size, classProdSave.stream().count());
            commonDto.setClassProds(classProdSave);
            tableMasterScheduler.setTemplateName(sheet.getSheetName());
            tableMasterScheduler.setClassName(ClassProd.class.getCanonicalName());
            tableMasterScheduler.setData(new ObjectMapper().writeValueAsString(commonDto));
            tableMasterScheduler.setIdLog(logId);
            tableMasterSchedulerRepository.save(tableMasterScheduler);
            return ResponseUtil.buildResponse(
                    SUCCESS,
                    UPLOADING,
                    null,
                    HttpStatus.OK);
        } catch (Exception e) {
            log.info("error upload group product : {}",e.getMessage());
            throw e;
        }
    }
    @Transactional
    public ResponseEntity<Object> prodType(Workbook workbook, Sheet sheet, Long size,Metadata metadata) throws IOException {
        log.info("star upload product type");
        List<TypeProd> typeProdSave = new ArrayList<>();
        TableMasterScheduler tableMasterScheduler = new TableMasterScheduler();
        CommonDto commonDto = new CommonDto();
        try {
            Iterator<Row> rows = sheet.iterator();
            int rowNumber = 0;
            while (rows.hasNext()) {
                TypeProd typeProd = new TypeProd();
                Row currentRow = rows.next();
                if (rowNumber == 0) {
                    rowNumber++;
                    continue;
                }
                Iterator<Cell> cellInRow = currentRow.iterator();
                int cellIdx = 0;
                while (cellInRow.hasNext()) {
                    Cell currentCell = cellInRow.next();
                    switch (cellIdx) {
                        case 0:
                            typeProd.setTypeId(currentCell.getStringCellValue());
                            break;
                        case 1:
                            typeProd.setTypeName(currentCell.getStringCellValue());
                            break;
                        default:
                            break;
                    }
                    cellIdx++;
                }
                typeProdSave.add(typeProd);
            }
            workbook.close();
            typeProdSave.stream().forEach(x -> {
                x.setCreatedBy(metadata.getUsername());
                x.setCreatedAt(new Date());
            });
            var logId = insertLogMonitoring.logMonitoring(null, sheet.getSheetName(), "Uploading", size, typeProdSave.stream().count());
            commonDto.setTypeProds(typeProdSave);
            tableMasterScheduler.setTemplateName(sheet.getSheetName());
            tableMasterScheduler.setClassName(TypeProd.class.getCanonicalName());
            tableMasterScheduler.setData(new ObjectMapper().writeValueAsString(commonDto));
            tableMasterScheduler.setIdLog(logId);
            tableMasterSchedulerRepository.save(tableMasterScheduler);
            return ResponseUtil.buildResponse(
                    SUCCESS,
                    UPLOADING,
                    null,
                    HttpStatus.OK);
        } catch (Exception e) {
            log.info("error upload group product : {}",e.getMessage());
            throw e;
        }
    }

    @Transactional
    public ResponseEntity<Object> prodCategory(Workbook workbook, Sheet sheet, Long size,Metadata metadata) throws IOException {
        log.info("star upload product category");
        List<ProdCategory> prodCategories = new ArrayList<>();
        TableMasterScheduler tableMasterScheduler = new TableMasterScheduler();
        CommonDto commonDto = new CommonDto();
        try {
            Iterator<Row> rows = sheet.iterator();
            int rowNumber = 0;
            while (rows.hasNext()) {
                ProdCategory category = new ProdCategory();
                Row currentRow = rows.next();
                if (rowNumber == 0) {
                    rowNumber++;
                    continue;
                }
                Iterator<Cell> cellInRow = currentRow.iterator();
                int cellIdx = 0;
                while (cellInRow.hasNext()) {
                    Cell currentCell = cellInRow.next();
                    switch (cellIdx) {
                        case 0:
                            category.setKeterangan(currentCell.getStringCellValue());
                            break;
                        default:
                            break;
                    }
                    cellIdx++;
                }
                prodCategories.add(category);
            }
            workbook.close();
            prodCategories.stream().forEach(x -> {
                x.setCreatedBy(metadata.getUsername());
                x.setCreatedAt(new Date());
                x.setCode(GeneralUtils.code("PC"));
            });
            var logId = insertLogMonitoring.logMonitoring(null, sheet.getSheetName(), "Uploading", size, prodCategories.stream().count());
            commonDto.setProdCategories(prodCategories);
            tableMasterScheduler.setTemplateName(sheet.getSheetName());
            tableMasterScheduler.setClassName(ProdCategory.class.getCanonicalName());
            tableMasterScheduler.setData(new ObjectMapper().writeValueAsString(commonDto));
            tableMasterScheduler.setIdLog(logId);
            tableMasterSchedulerRepository.save(tableMasterScheduler);
            return ResponseUtil.buildResponse(
                    SUCCESS,
                    UPLOADING,
                    null,
                    HttpStatus.OK);
        } catch (Exception e) {
            throw e;
        }
    }
    @Transactional
    public ResponseEntity<Object> productUpload(Workbook workbook, Sheet sheet, Long size, Metadata metadata) throws IOException, ParseException {
        log.info("Processing Update Fixed Asset");
        List<Product> products = new ArrayList<>();
        TableMasterScheduler tableMasterScheduler = new TableMasterScheduler();
        CommonDto commonDto = new CommonDto();

        try {
            Iterator<Row> rows = sheet.iterator();
            DataFormatter formatter = new DataFormatter();
            int rowNumber = 0;
            while (rows.hasNext()) {
                Product product = new Product();
                Row currentRow = rows.next();
                if (rowNumber == 0) {
                    rowNumber++;
                    continue;
                }
                Iterator<Cell> cellInRow = currentRow.iterator();
                int cellIdx = 0;
                while (cellInRow.hasNext()) {
                    Cell currentCell = cellInRow.next();
                    String dataCol = formatter.formatCellValue(sheet.getRow(currentRow.getRowNum()).getCell(currentCell.getColumnIndex()));

                    switch (cellIdx) {
                        case 0:
                            product.setProductId(dataCol);
                            break;
                        case 1:
                            product.setProductName(dataCol);
                            break;
                        case 2:
//                            dataCol = ObjectUtils.isEmpty(uomRepository.findByKeterangan(dataCol)) ? "" : dataCol ;
                            product.setUom(uomRepository.findByKeterangan(dataCol));
                            break;
                        case 3:
//                            dataCol = ObjectUtils.isEmpty(groupProdRepository.findByGroupId(dataCol)) ? "" : dataCol;
                            product.setGroupProd(groupProdRepository.findByGroupProd(dataCol));
                            break;
                        case 4:
//                            dataCol = ObjectUtils.isEmpty(supplierRepository.findBySupplierId(dataCol)) ? "" : dataCol;
                            product.setSupplier(supplierRepository.findBySupplierId(dataCol));
                            break;
                        case 5:
//                            dataCol = ObjectUtils.isEmpty(prodCategoryRepository.findByCode(dataCol)) ? "" : dataCol;
                            product.setCategory(dataCol);
                            break;
                        case 6:
//                            dataCol = ObjectUtils.isEmpty(typeProdRepository.findByTypeId(dataCol)) ? "" : dataCol;
                            product.setType(typeProdRepository.findByTypeId(dataCol));
                            break;
                        default:
                            break;
                    }
                    cellIdx++;
                }
                products.add(product);
            }
            workbook.close();
            products.stream().forEach(x -> {
                x.setCreatedBy(metadata.getUsername());
                x.setCreatedAt(new Date());
            });
            var logId = insertLogMonitoring.logMonitoring(null, sheet.getSheetName(), "Uploading", size, products.stream().count());
            commonDto.setProducts(products);
            tableMasterScheduler.setTemplateName(sheet.getSheetName());
            tableMasterScheduler.setClassName(FixedAsset.class.getCanonicalName());
            tableMasterScheduler.setData(new ObjectMapper().writeValueAsString(commonDto));
            tableMasterScheduler.setIdLog(logId);
            tableMasterSchedulerRepository.save(tableMasterScheduler);
            return ResponseUtil.buildResponse(
                    SUCCESS,
                    UPLOADING,
                    null,
                    HttpStatus.OK);
        } catch (Exception e) {
            throw e;
        }
    }

}
