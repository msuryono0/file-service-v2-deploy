package com.rni.rni.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rni.rni.config.RedisConfig;
import com.rni.rni.config.RedisPublishConfig;
import com.rni.rni.config.RedisSubscribeConfig;
import com.rni.rni.domain.dao.*;
import com.rni.rni.domain.dto.*;
import com.rni.rni.repository.*;
import com.rni.rni.utils.InsertLogMonitoring;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import util.ResponseUtil;

import javax.transaction.Transactional;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

import static com.rni.rni.constant.Constant.chanelKey.CN_AP9;
import static com.rni.rni.constant.Constant.chanelKey.CN_TRAN_HC_OMZET_MASTER;
import static com.rni.rni.constant.Constant.redisKey.*;
import static com.rni.rni.constant.Constant.responseKey.*;

@Slf4j
@Service
public class TransaksiPusatService {

    @Autowired
    private RedisPublishConfig redisPublishConfig;

    @Autowired
    private RedisSubscribeConfig redisSubscribeConfig;

    @Autowired
    private RedisConfig redisConfig;


    @Autowired
    private ObjectMapper mapper;
    @Autowired
    private TbParameterBerasRepository tbParameterBerasRepository;

    @Autowired
    private KaryawanRepository karyawanRepository;

    @Autowired
    private DimensionRepository dimensionRepository;

    @Autowired
    private DimensionValueRepository dimensionValueRepository;

    @Autowired
    private InsertLogMonitoring insertLogMonitoring;

    @Autowired
    private TableMasterSchedulerRepository tableMasterSchedulerRepository;
    @Autowired
    private StrukturalRepository strukturalRepository;

    @Autowired
    private TbMParameterSeragamRepository tbMParameterSeragamRepository;

    @Autowired
    private TransactionHcShareRepository transactionHcShareRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private GroupProdRepository groupProdRepository;

    @Autowired
    private TransactionEntryDistOmzetRepository transactionEntryDistOmzetRepository;

    @Autowired
    private TransactionEntryEquipmentOmzetRepository transactionEntryEquipmentOmzetRepository;

    @Autowired
    private TransactionEntryHcOmzetRepository transactionEntryHcOmzetRepository;

    @Autowired
    private TransactionEntryLdOmzetRepository transactionEntryLdOmzetRepository;

    @Autowired
    private TransactionEntryLtsOmzetRepository transactionEntryLtsOmzetRepository;

    @Autowired
    private TransactionEntryMedicalOmzetRepository transactionEntryMedicalOmzetRepository;

    @Autowired
    private TransactionEntryRegOmzetRepository transactionEntryRegOmzetRepository;

    @Autowired
    private TransactionEntryServiceOmzetRepository transactionEntryServiceOmzetRepository;

    @Autowired
    private TransactionEntrySurgeryOmzetRepository transactionEntrySurgeryOmzetRepository;

    @Autowired
    private TransactionEntryTenderOmzetRepository transactionEntryTenderOmzetRepository;

    @Autowired
    private TransactionEntryTradingOmzetRepository transactionEntryTradingOmzetRepository;

    @Autowired
    private TransactionEntryDistSahreRepository transactionEntryDistSahreRepository;

    @Autowired
    private TransactionEntryEquipmentShareRepository transactionEntryEquipmentShareRepository;

    @Autowired
    private TransactionEntryHcShareRepository transactionEntryHcShareRepository;

    @Autowired
    private TransactionEntryLdShareRepository transactionEntryLdShareRepository;

    @Autowired
    private TransactionEntryLtsShareRepository transactionEntryLtsShareRepository;

    @Autowired
    private TransactionEntryMedicalShareRepository transactionEntryMedicalShareRepository;

    @Autowired
    private TransactionEntryRegShareRepository transactionEntryRegShareRepository;

    @Autowired
    private TransactionEntryServiceShareRepository transactionEntryServiceShareRepository;

    @Autowired
    private TransactionEntrySurgeryShareRepository transactionEntrySurgeryShareRepository;

    @Autowired
    private TransactionEntryTenderShareRepository transactionEntryTenderShareRepository;


    @Autowired
    private TransactionEntryTradingShareRepository TransactionEntryTradingShareRepository;
    @Autowired
    private SummaryServiceOmzet summaryServiceOmzet;
    @Autowired
    private RestTemplate restTemplate;

    @Value("${url.trigger}")
    private String url;

    @Transactional
    public ResponseEntity<Object> kk9(MultipartFile file,
                                      String cabang, String divisi,
                                      String periode, String year,
                                      Metadata metadata) throws IOException {
        log.info("Processing upload data kk9 value");
        List<KK9> kk9s = new ArrayList<>();
        TableMasterScheduler tableMasterScheduler = new TableMasterScheduler();
        CommonDto commonDto = new CommonDto();
        Workbook workbook = new XSSFWorkbook(file.getInputStream());
        Sheet sheet = workbook.getSheetAt(0);
        String sheetName = sheet.getSheetName();
        try {
            Iterator<Row> rows = sheet.iterator();
            int rowNumber = 0;
            while (rows.hasNext()) {
                DataFormatter formatter = new DataFormatter();
                KK9 kk9 = new KK9();
                Row currentRow = rows.next();
                if (rowNumber == 0) {
                    rowNumber++;
                    continue;
                }
                Iterator<Cell> cellInRow = currentRow.iterator();
                int cellIdx = 0;
                while (cellInRow.hasNext()) {
                    Cell currentCell = cellInRow.next();
                    String dataCol = formatter.formatCellValue(sheet.getRow(currentRow.getRowNum()).getCell(currentCell.getColumnIndex()));
                    switch (cellIdx) {
                        case 0:
                            kk9.setName(dataCol);
                            break;
                        case 1:
                            kk9.setEstimateStartWorking(dataCol);
                            break;
                        case 2:
                            kk9.setJabatanCode(Jabatan.builder()
                                    .code(dataCol).build());
                            break;
                        case 3:
                            kk9.setStatusEmployee(StatusKaryawan.builder()
                                    .code(dataCol).build());
                            break;
                        case 4:
                            kk9.setPendidikanCode(Pendidikan.builder()
                                    .code(dataCol).build());
                            break;
                        default:
                            break;
                    }
                    cellIdx++;
                }
                kk9s.add(kk9);
            }
            workbook.close();
            kk9s.stream().forEach(x -> {
                x.setCreatedBy(metadata.getUsername());
                x.setCreatedAt(new Date());
                x.setYear(year);
                x.setPeriode(periode);
                x.setCabang(dimensionRepository.findByCode(cabang));
                x.setDivisi(dimensionValueRepository.findByCode(divisi));
                x.setDeleted(false);
            });
            redisConfig.setChanel(KEY_KK9.concat(cabang.concat(divisi)));
            redisPublishConfig.publish(new ObjectMapper().writeValueAsString(kk9s), KEY_KK9.concat(cabang.concat(divisi)));
            return ResponseUtil.buildResponse(SUCCESS, UPLOADING, null, HttpStatus.OK);
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional
    public ResponseEntity<Object> beras(MultipartFile file,
                                        String year,
                                        Metadata metadata) throws ParseException, IOException {
        log.info("Processing upload data pj beras");
        List<TbPjBeras> tbPjBeras = new ArrayList<>();
        TableMasterScheduler tableMasterScheduler = new TableMasterScheduler();
        CommonDto commonDto = new CommonDto();
        Workbook workbook = new XSSFWorkbook(file.getInputStream());
        Sheet sheet = workbook.getSheetAt(0);
        try {
            DataFormatter formatter = new DataFormatter();
            Iterator<Row> rows = sheet.iterator();
            int rowNumber = 0;
            while (rows.hasNext()) {
                TbPjBeras pjBeras = new TbPjBeras();
                Row currentRow = rows.next();
                if (rowNumber == 0) {
                    rowNumber++;
                    continue;
                }
                Iterator<Cell> cellInRow = currentRow.iterator();
                int cellIdx = 0;
                while (cellInRow.hasNext()) {
                    Cell currentCell = cellInRow.next();
                    String dataCol = formatter.formatCellValue(sheet.getRow(currentRow.getRowNum()).getCell(currentCell.getColumnIndex()));
                    switch (cellIdx) {
                        case 0:
                            var data = karyawanRepository.findByNamaContainingIgnoreCase(dataCol);
                            if (Objects.isNull(data)) {
                                log.info("nama " + data + "tidak di temuakan");
                            }
                            pjBeras.setIdKaryawan(karyawanRepository.findByNamaContainingIgnoreCase(dataCol));
                            break;
                        case 1:
                            pjBeras.setJanJunQuantity(new BigDecimal(dataCol));
                            break;
                        case 2:
                            pjBeras.setJulDesQuantity(new BigDecimal(dataCol));
                            break;
                        case 3:
                            pjBeras.setApQuantity(new BigDecimal(dataCol));
                            break;
                    }
                    cellIdx++;
                }
                tbPjBeras.add(pjBeras);
            }
            workbook.close();
            ;
            Integer years = Integer.parseInt(year) - 1;
            tbPjBeras.stream().forEach(x -> {
                List<TbParameterBeras> tbParameterBeras = new ArrayList<>();
                tbParameterBeras = tbParameterBerasRepository.findByJabatan(x.getIdKaryawan().getJabatan());
                String nominalTh1 = tbParameterBeras.stream().filter(y -> y.getTahun().equals(String.valueOf(years)) && y.getParameter_code().equals("SM1")).findFirst().get().getNominal();
                String nominalTh2 = tbParameterBeras.stream().filter(y -> y.getTahun().equals(String.valueOf(years)) && y.getParameter_code().equals("SM2")).findFirst().get().getNominal();
                BigDecimal timesSM1 = x.getJanJunQuantity().multiply(new BigDecimal(nominalTh1));
                BigDecimal timesSM2 = x.getJanJunQuantity().multiply(new BigDecimal(nominalTh2));
                x.setEaQuantity(x.getJulDesQuantity());
                x.setJaJunTotal((timesSM1.divide(new BigDecimal(1000))).multiply(new BigDecimal(6)));
                x.setJulDesTotal((timesSM2.divide(new BigDecimal(1000))).multiply(new BigDecimal(6)));
                x.setEaQuantity(x.getJulDesQuantity());
                x.setEaTotal(x.getJaJunTotal().add(x.getJulDesTotal()));
                BigDecimal sumAp = x.getApQuantity().multiply(new BigDecimal(14000)).divide(new BigDecimal(1000));
                x.setApTotal(sumAp.multiply(new BigDecimal(12)));
                x.setYear(String.valueOf(years));
            });
            var logId = insertLogMonitoring.logMonitoring(null, sheet.getSheetName(), "Uploading", file.getSize(), tbPjBeras.stream().count());
            commonDto.setTbPjBeras(tbPjBeras);
            tableMasterScheduler.setTemplateName(sheet.getSheetName());
            tableMasterScheduler.setClassName(TbPjBeras.class.getCanonicalName());
            tableMasterScheduler.setData(new ObjectMapper().writeValueAsString(commonDto));
            tableMasterScheduler.setIdLog(logId);
            tableMasterSchedulerRepository.save(tableMasterScheduler);
            redisConfig.setChanel(KEY_BERAS.concat(metadata.getBranch()).concat(metadata.getUnit().concat(year)));
            redisPublishConfig.publish(new ObjectMapper().writeValueAsString(tbPjBeras), (KEY_BERAS.concat(metadata.getBranch()).concat(metadata.getUnit().concat(year))));
            return ResponseUtil.buildResponse(SUCCESS, UPLOADING, null, HttpStatus.OK);
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional
    public ResponseEntity<Object> seragam(MultipartFile file,
                                          String year,
                                          Metadata metadata) throws ParseException, IOException {
        log.info("Processing upload data pj seragam");
        List<TbPjSeragam> tbPjSeragams = new ArrayList<>();
        TableMasterScheduler tableMasterScheduler = new TableMasterScheduler();
        CommonDto commonDto = new CommonDto();
        Workbook workbook = new XSSFWorkbook(file.getInputStream());
        Sheet sheet = workbook.getSheetAt(0);
        try {
            DataFormatter formatter = new DataFormatter();
            Iterator<Row> rows = sheet.iterator();
            int rowNumber = 0;
            while (rows.hasNext()) {
                TbPjSeragam tbPjSeragam = new TbPjSeragam();
                Row currentRow = rows.next();
                if (rowNumber == 0) {
                    rowNumber++;
                    continue;
                }
                Iterator<Cell> cellInRow = currentRow.iterator();
                int cellIdx = 0;
                while (cellInRow.hasNext()) {
                    Cell currentCell = cellInRow.next();
                    String dataCol = formatter.formatCellValue(sheet.getRow(currentRow.getRowNum()).getCell(currentCell.getColumnIndex()));
                    switch (cellIdx) {
                        case 0:
                            var data = karyawanRepository.findByNamaContainingIgnoreCase(dataCol);
                            if (Objects.isNull(data)) {
                                log.info("nama " + data + "tidak di temuakan");
                            }
                            tbPjSeragam.setIdKaryawan(karyawanRepository.findByNamaContainingIgnoreCase(dataCol));
                            break;
                    }
                    cellIdx++;
                }
                tbPjSeragams.add(tbPjSeragam);
            }
            workbook.close();
            ;
            Integer years = Integer.parseInt(year) - 1;
            tbPjSeragams.stream().forEach(x -> {
                List<TbMParameterSeragam> tbMParameterSeragams = new ArrayList<>();
                tbMParameterSeragams = tbMParameterSeragamRepository.findAll();
                Integer nominalSM = tbMParameterSeragams.stream().filter(y -> y.getTahun().equals(String.valueOf(years)) && y.getParameterCode().equals("SM2")).findFirst().get().getSeragam();
                Integer nominalAp = tbMParameterSeragams.stream().filter(y -> y.getTahun().equals(year) && y.getParameterCode().equals("AP")).findFirst().get().getSeragam();

                x.setJanJun(Integer.parseInt(String.valueOf(BigDecimal.valueOf((nominalSM / 1000 * 2) + 5.7))));
                x.setJumlah(x.getJanJun() + x.getJulDes());
                x.setAp(Integer.parseInt(String.valueOf(BigDecimal.valueOf((nominalAp / 1000 * 2) + 5.7))));
                x.setYear(String.valueOf(years));
            });
            var logId = insertLogMonitoring.logMonitoring(null, sheet.getSheetName(), "Uploading", file.getSize(), tbPjSeragams.stream().count());
            commonDto.setTbPjSeragams(tbPjSeragams);
            tableMasterScheduler.setTemplateName(sheet.getSheetName());
            tableMasterScheduler.setClassName(TbMParameterSeragam.class.getCanonicalName());
            tableMasterScheduler.setData(new ObjectMapper().writeValueAsString(commonDto));
            tableMasterScheduler.setIdLog(logId);
            tableMasterSchedulerRepository.save(tableMasterScheduler);
            redisConfig.setChanel(KEY_BERAS.concat(metadata.getBranch()).concat(metadata.getUnit().concat(year)));
            redisPublishConfig.publish(new ObjectMapper().writeValueAsString(tbPjSeragams), (KEY_BERAS.concat(metadata.getBranch()).concat(metadata.getUnit().concat(year))));
            restTemplate.postForEntity(url, true, void.class);
            return ResponseUtil.buildResponse(SUCCESS, UPLOADING, null, HttpStatus.OK);
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional
    public ResponseEntity<Object> uploadAp9(MultipartFile file,
                                            String year,
                                            Metadata metadata) throws ParseException, IOException {
        log.info("Processing upload data AP 9");
        List<AP9> tableList = new ArrayList<>();
        TableMasterScheduler tableMasterScheduler = new TableMasterScheduler();
        CommonDto commonDto = new CommonDto();
        Workbook workbook = new XSSFWorkbook(file.getInputStream());
        Sheet sheet = workbook.getSheetAt(0);
        try {
            DataFormatter formatter = new DataFormatter();
            Iterator<Row> rows = sheet.iterator();
            int rowNumber = 0;
            while (rows.hasNext()) {
                AP9 table = new AP9();
                Row currentRow = rows.next();
                if (rowNumber < 4) {
                    rowNumber++;
                    continue;
                }
                Iterator<Cell> cellInRow = currentRow.iterator();
                int cellIdx = 0;
                while (cellInRow.hasNext()) {
                    Cell currentCell = cellInRow.next();
                    String dataCol = formatter.formatCellValue(sheet.getRow(currentRow.getRowNum()).getCell(currentCell.getColumnIndex()));
                    switch (cellIdx) {
                        case 0:
                            var data = strukturalRepository.findByStrukturalName(dataCol);
                            if (Objects.isNull(data)) {
                                table.setStruktural(null);
                            } else {
                                table.setStruktural(data.stream().findFirst().get());
                            }
                            break;
                        case 1:
                            table.setFormasi(dataCol);
                            break;
                        case 2:
                            table.setStaff(dataCol);
                            break;
                        case 3:
                            table.setNonStaff(dataCol);
                            break;
                        case 4:
                            table.setKkwt(dataCol);
                            break;
                    }
                    cellIdx++;
                }
                tableList.add(table);
            }
            workbook.close();
            ;
            tableList.stream().forEach(x -> {
                x.setCreatedBy(metadata.getUsername());
                x.setYears(year);
                x.setCreatedAt(new Date());
                x.setCabangCategory(dimensionRepository.findByCode(metadata.getBranch()));
                x.setCabangDivisi(dimensionValueRepository.findByCode(metadata.getUnit()));
            });
            var logId = insertLogMonitoring.logMonitoring(null, sheet.getSheetName(), "Uploading", file.getSize(), tableList.stream().count());
            commonDto.setAp9(tableList);
            tableMasterScheduler.setTemplateName(sheet.getSheetName());
            tableMasterScheduler.setClassName(Karyawan.class.getCanonicalName());
            tableMasterScheduler.setData(new ObjectMapper().writeValueAsString(commonDto));
            tableMasterScheduler.setIdLog(logId);
            tableMasterSchedulerRepository.save(tableMasterScheduler);
            restTemplate.postForEntity(url, true, void.class);
            redisConfig.setChanel(CN_AP9.concat(metadata.getBranch()).concat(metadata.getUnit()).concat(year));
            redisPublishConfig.publish(new ObjectMapper().writeValueAsString(tableList), CN_AP9.concat(metadata.getBranch()).concat(metadata.getUnit()).concat(year));
            return ResponseUtil.buildResponse(SUCCESS, UPLOADING, null, HttpStatus.OK);
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional
    public ResponseEntity<Object> transactionHcShare(MultipartFile file,
                                                     String year,
                                                     String periode,
                                                     Metadata metadata) throws ParseException, IOException {
        log.info("Processing upload data transaction hc share");
        List<TransactionEntryHcShare> entryHcShares = new ArrayList<>();
        TableMasterScheduler tableMasterScheduler = new TableMasterScheduler();
        CommonDto commonDto = new CommonDto();
        Workbook workbook = new XSSFWorkbook(file.getInputStream());
        Sheet sheet = workbook.getSheetAt(0);
        try {
            DataFormatter formatter = new DataFormatter();
            Iterator<Row> rows = sheet.iterator();
            int rowNumber = 0;
            while (rows.hasNext()) {
                TransactionEntryHcShare table = new TransactionEntryHcShare();
                Row currentRow = rows.next();
                if (rowNumber < 5) {
                    rowNumber++;
                    continue;
                }
                Iterator<Cell> cellInRow = currentRow.iterator();
                int cellIdx = 0;
                while (cellInRow.hasNext()) {
                    Cell currentCell = cellInRow.next();
                    String dataCol = formatter.formatCellValue(sheet.getRow(currentRow.getRowNum()).getCell(currentCell.getColumnIndex()));
                    switch (cellIdx) {
                        case 0:
                            table.setProduct(dataCol);
                            break;
                        case 5:
                            table.setSharePenjualanTender(dataCol);
                            break;
                        case 6:
                            table.setPrDiscPedOps(dataCol);
                            break;
                        case 7:
                            table.setPrDiscRakor(dataCol);
                            break;
                        case 8:
                            table.setPtDiscPedOps(dataCol);
                            break;
                        case 9:
                            table.setPtDiscRakor(dataCol);
                            break;
                    }
                    cellIdx++;
                }
                entryHcShares.add(table);
            }
            workbook.close();
            ;
            entryHcShares.stream().forEach(x -> {
                x.setCreatedBy(metadata.getUsername());
                x.setTahun(year);
                x.setCreatedAt(new Date());
                x.setPeriode(periode);
                x.setCabangDivisi(metadata.getUnit());
                x.setDeleted(false);
            });
            var logId = insertLogMonitoring.logMonitoring(null, sheet.getSheetName(), "Uploading", file.getSize(), entryHcShares.stream().count());
            commonDto.setTransactionEntryHcShares(entryHcShares);
            tableMasterScheduler.setTemplateName(sheet.getSheetName());
            tableMasterScheduler.setClassName(Karyawan.class.getCanonicalName());
            tableMasterScheduler.setData(new ObjectMapper().writeValueAsString(commonDto));
            tableMasterScheduler.setIdLog(logId);
            tableMasterSchedulerRepository.save(tableMasterScheduler);
            log.info("sending data to master scheduler");
            redisConfig.setChanel(CN_TRAN_HC_OMZET_MASTER.concat(year).concat(periode));
            redisPublishConfig.publish(new ObjectMapper().writeValueAsString(entryHcShares), CN_TRAN_HC_OMZET_MASTER.concat(year).concat(periode));
            return ResponseUtil.buildResponse(SUCCESS, UPLOADING, null, HttpStatus.OK);
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional
    public ResponseEntity<Object> transactionOmzet(MultipartFile file,
                                                   String year,
                                                   String periode,
                                                   Metadata metadata) throws ParseException, IOException {
        log.info("Processing upload data master hc share");
        List<TransactionEntryHcOmzet> transactionEntryHcOmzets = new ArrayList<>();
        TableMasterScheduler tableMasterScheduler = new TableMasterScheduler();
        CommonDto commonDto = new CommonDto();
        Workbook workbook = new XSSFWorkbook(file.getInputStream());
        Sheet sheet = workbook.getSheetAt(0);
        try {
            DataFormatter formatter = new DataFormatter();
            Iterator<Row> rows = sheet.iterator();
            int rowNumber = 0;
            while (rows.hasNext()) {
                TransactionEntryHcOmzet entryHcShare = new TransactionEntryHcOmzet();
                Row currentRow = rows.next();
                if (rowNumber < 4) {
                    rowNumber++;
                    continue;
                }
                Iterator<Cell> cellInRow = currentRow.iterator();
                int cellIdx = 0;
                while (cellInRow.hasNext()) {
                    Cell currentCell = cellInRow.next();
                    String dataCol = formatter.formatCellValue(sheet.getRow(currentRow.getRowNum()).getCell(currentCell.getColumnIndex()));
                    switch (cellIdx) {
                        case 0:
                            entryHcShare.setProduct(dataCol);
                            break;
                        case 3:
                            entryHcShare.setJan(dataCol);
                            break;
                        case 4:
                            entryHcShare.setFeb(dataCol);
                            break;
                        case 5:
                            entryHcShare.setMar(dataCol);
                            break;
                        case 6:
                            entryHcShare.setApr(dataCol);
                            break;
                        case 7:
                            entryHcShare.setMei(dataCol);
                            break;
                        case 8:
                            entryHcShare.setJun(dataCol);
                            break;
                        case 9:
                            entryHcShare.setJul(dataCol);
                            break;
                        case 10:
                            entryHcShare.setAgt(dataCol);
                            break;
                        case 11:
                            entryHcShare.setSep(dataCol);
                            break;
                        case 12:
                            entryHcShare.setOkt(dataCol);
                            break;
                        case 13:
                            entryHcShare.setNov(dataCol);
                            break;
                        case 14:
                            entryHcShare.setDes(dataCol);
                            break;
                    }
                    cellIdx++;
                }
                transactionEntryHcOmzets.add(entryHcShare);
            }
            workbook.close();
            ;
            transactionEntryHcOmzets.stream().forEach(x -> {
                x.setCreatedBy(metadata.getUsername());
                x.setYear(year);
                x.setCreatedAt(new Date());
                x.setPeriode(periode);
                x.setCabangDivisi(metadata.getUnit());
                x.setDeleted(false);
            });
            var logId = insertLogMonitoring.logMonitoring(null, sheet.getSheetName(), "Uploading", file.getSize(), transactionEntryHcOmzets.stream().count());
            commonDto.setTransactionEntryHcOmzets(transactionEntryHcOmzets);
            tableMasterScheduler.setTemplateName(sheet.getSheetName());
            tableMasterScheduler.setClassName(TransactionEntryHcOmzet.class.getCanonicalName());
            tableMasterScheduler.setData(new ObjectMapper().writeValueAsString(commonDto));
            tableMasterScheduler.setIdLog(logId);
            tableMasterSchedulerRepository.save(tableMasterScheduler);
            restTemplate.postForEntity(url, true, void.class);
            redisConfig.setChanel(CN_TRAN_HC_OMZET_MASTER.concat(year).concat(periode));
            redisPublishConfig.publish(new ObjectMapper().writeValueAsString(transactionEntryHcOmzets), CN_TRAN_HC_OMZET_MASTER.concat(year).concat(periode));
            return ResponseUtil.buildResponse(SUCCESS, UPLOADING, null, HttpStatus.OK);
        } catch (Exception e) {
            throw e;
        }
    }

    /**
     *
     * @param request
     * @param metadata
     * @return
     * @throws JsonProcessingException
     */
    //#region get getDataEntryDistOmzet
    public ResponseEntity<Object> getDataEntryDistOmzet(CommonRequestDto request, Metadata metadata) {
        List<TransactionEntryDistOmzet> listTable = new ArrayList<>();
        CommonResponseDto response = new CommonResponseDto();
        try {
            listTable =  transactionEntryDistOmzetRepository.findByTahunAndPeriodeAndCabangDivisi(request.getTahun(),request.getPeriode(),request.getUnit());
            AtomicReference<TRansactionOmzetSumDto> tRansactionOmzetSumDto = new AtomicReference<>(new TRansactionOmzetSumDto());
            List<TransactionHCOmzetDto> resstream = new ArrayList<>();
            listTable.stream().forEach(x->{

                if (!Objects.equals(x.getFormula(), "")) {
                    resstream.add(summaryServiceOmzet.sumTotal(tRansactionOmzetSumDto.get(), x));
                    tRansactionOmzetSumDto.set(new TRansactionOmzetSumDto());
                    return;
                }
                TransactionHCOmzetDto res = new TransactionHCOmzetDto();
                res.setId(x.getId());
                res.setProduct(x.getProduct());
                res.setCabangDivisi(x.getCabangDivisi());
                res.setPeriode(x.getPeriode());
                res.setYear(x.getTahun());
                res.setKeterangan(x.getKeterangan());
                res.setGroupProd(x.getGroupProd());
                res.setRowNumber(x.getRowNumber());
                res.setJan(ObjectUtils.isEmpty(x.getJan()) ? 0L : Long.valueOf(x.getJan().replaceAll(",", "")));
                res.setFeb(ObjectUtils.isEmpty(x.getFeb()) ? 0L : Long.valueOf(x.getFeb().replaceAll(",", "")));
                res.setMar(ObjectUtils.isEmpty(x.getMar()) ? 0L : Long.valueOf(x.getMar().replaceAll(",", "")));
                res.setTwi(res.getMar()+res.getJan()+res.getFeb());
                res.setApr(ObjectUtils.isEmpty(x.getApr()) ? 0L : Long.valueOf(x.getApr().replaceAll(",", "")));
                res.setMei(ObjectUtils.isEmpty(x.getMei()) ? 0L : Long.valueOf(x.getMei().replaceAll(",", "")));
                res.setJun(ObjectUtils.isEmpty(x.getJun()) ? 0L : Long.valueOf(x.getJun().replaceAll(",", "")));
                res.setTwii(res.getApr()+res.getMei()+res.getJun());
                res.setJul(ObjectUtils.isEmpty(x.getJul()) ? 0L : Long.valueOf(x.getJul().replaceAll(",", "")));
                res.setAgt(ObjectUtils.isEmpty(x.getAgt()) ? 0L : Long.valueOf(x.getAgt().replaceAll(",", "")));
                res.setSep(ObjectUtils.isEmpty(x.getSep()) ? 0L : Long.valueOf(x.getSep().replaceAll(",", "")));
                res.setTwiii(res.getJul()+res.getAgt()+res.getSep());
                res.setOkt(ObjectUtils.isEmpty(x.getOkt()) ? 0L : Long.valueOf(x.getOkt().replaceAll(",", "")));
                res.setNov(ObjectUtils.isEmpty(x.getNov()) ? 0L : Long.valueOf(x.getNov().replaceAll(",", "")));
                res.setDes(ObjectUtils.isEmpty(x.getDes()) ? 0L : Long.valueOf(x.getDes().replaceAll(",", "")));
                res.setTwiv(res.getOkt()+res.getNov()+res.getDes());
                res.setTtl(res.getTwi()+res.getTwii()+res.getTwiii()+res.getTwiv());

                if (!Objects.equals(x.getRowNumber(), "")) {
                    tRansactionOmzetSumDto.set(summaryServiceOmzet.sumTotalTransactionByRowNumber(tRansactionOmzetSumDto.get(), x));
                }
                resstream.add(res);
            });
            String chanel = TransactionEntryDistOmzet.class.getCanonicalName().concat(request.getTahun()).concat(request.getPeriode()).concat(request.getUnit());
            redisConfig.setMessageListener(chanel);
            redisPublishConfig.publish(mapper.writeValueAsString(resstream),chanel);
            return ResponseUtil.buildResponse(
                    SUCCESS,
                    INQUIRY_SUCCESS,
                    response,
                    HttpStatus.OK);

        }catch (Exception e){
            log.info("error inquiry dist Omzet : {}",e.getMessage());
            return ResponseUtil.buildResponse(
                    FAILED,
                    INQUIRY_FAILED,
                    null,
                    HttpStatus.BAD_REQUEST);
        }
    }

    @Transactional
    public ResponseEntity<Object> saveDataEntryDistOmzet(TransactionEntryDistOmzet request) {
        log.info("proccess saving data entry dist omzet");
        try {
            transactionEntryDistOmzetRepository.save(request);
            return ResponseUtil.buildResponse(SUCCESS, "DATA SAVED", null, HttpStatus.OK);
        } catch (Exception e) {
            throw e;
        }
    }

    // TODO: 8/29/2022 rendika get dataDataEntryEquipmentOmzet

    @Transactional
    public ResponseEntity<Object> saveDataEntryEquipmentOmzet(TransactionEntryEquipmentOmzet request) {
        log.info("proccess saving data entry equipment omzet");
        try {
            transactionEntryEquipmentOmzetRepository.save(request);
            return ResponseUtil.buildResponse(SUCCESS, "DATA SAVED", null, HttpStatus.OK);
        } catch (Exception e) {
            throw e;
        }
    }


    @Transactional
    public ResponseEntity<Object> saveDataEntryHcOmzet(TransactionEntryHcOmzet request) {
        log.info("proccess saving data entry HC omzet");
        try {
            transactionEntryHcOmzetRepository.save(request);
            return ResponseUtil.buildResponse(SUCCESS, "DATA SAVED", null, HttpStatus.OK);
        } catch (Exception e) {
            throw e;
        }
    }


   //TODO

    @Transactional
    public ResponseEntity<Object> saveDataEntryLdOmzet(TransactionEntryLdOmzet request) {
        log.info("proccess saving data entry Ld omzet");
        try {
            transactionEntryLdOmzetRepository.save(request);
            return ResponseUtil.buildResponse(SUCCESS, "DATA SAVED", null, HttpStatus.OK);
        } catch (Exception e) {
            throw e;
        }
    }


    /**
     *
     * @param request
     * @param metadata
     * @return
     * @throws JsonProcessingException
     */
    //#region get getDataEntryDistOmzet
    public ResponseEntity<Object> getDataEntryLtsOmzet(CommonRequestDto request, Metadata metadata) {
        List<TransactionEntryLdOmzet> listTable = new ArrayList<>();
        CommonResponseDto response = new CommonResponseDto();
        try {
            listTable =  transactionEntryLtsOmzetRepository.findByYearAndPeriodeAndCabangDivisi(request.getTahun(),request.getPeriode(),request.getUnit());
            AtomicReference<TRansactionOmzetSumDto> tRansactionOmzetSumDto = new AtomicReference<>(new TRansactionOmzetSumDto());
            List<TransactionOmzetDto> resstream = new ArrayList<>();
            listTable.stream().forEach(x->{

                if (!Objects.equals(x.getFormula(), "")) {
                    resstream.add(summaryServiceOmzet.sumTotal(tRansactionOmzetSumDto.get(), x));
                    tRansactionOmzetSumDto.set(new TRansactionOmzetSumDto());
                    return;
                }
                TransactionOmzetDto res = new TransactionOmzetDto();
                res.setId(x.getId());
                res.setProduct(x.getProduct());
                res.setCabangDivisi(x.getCabangDivisi());
                res.setPeriode(x.getPeriode());
                res.setYear(x.getYear());
                res.setKeterangan(x.getKeterangan());
                res.setGroupProd(x.getGroupProd());
                res.setRowNumber(x.getRowNumber());
                res.setJan(ObjectUtils.isEmpty(x.getJan()) ? 0L : Long.valueOf(x.getJan().replaceAll(",", "")));
                res.setFeb(ObjectUtils.isEmpty(x.getFeb()) ? 0L : Long.valueOf(x.getFeb().replaceAll(",", "")));
                res.setMar(ObjectUtils.isEmpty(x.getMar()) ? 0L : Long.valueOf(x.getMar().replaceAll(",", "")));
                res.setTwi(res.getMar()+res.getJan()+res.getFeb());
                res.setApr(ObjectUtils.isEmpty(x.getApr()) ? 0L : Long.valueOf(x.getApr().replaceAll(",", "")));
                res.setMei(ObjectUtils.isEmpty(x.getMei()) ? 0L : Long.valueOf(x.getMei().replaceAll(",", "")));
                res.setJun(ObjectUtils.isEmpty(x.getJun()) ? 0L : Long.valueOf(x.getJun().replaceAll(",", "")));
                res.setTwii(res.getApr()+res.getMei()+res.getJun());
                res.setJul(ObjectUtils.isEmpty(x.getJul()) ? 0L : Long.valueOf(x.getJul().replaceAll(",", "")));
                res.setAgt(ObjectUtils.isEmpty(x.getAgt()) ? 0L : Long.valueOf(x.getAgt().replaceAll(",", "")));
                res.setSep(ObjectUtils.isEmpty(x.getSep()) ? 0L : Long.valueOf(x.getSep().replaceAll(",", "")));
                res.setTwiii(res.getJul()+res.getAgt()+res.getSep());
                res.setOkt(ObjectUtils.isEmpty(x.getOkt()) ? 0L : Long.valueOf(x.getOkt().replaceAll(",", "")));
                res.setNov(ObjectUtils.isEmpty(x.getNov()) ? 0L : Long.valueOf(x.getNov().replaceAll(",", "")));
                res.setDes(ObjectUtils.isEmpty(x.getDes()) ? 0L : Long.valueOf(x.getDes().replaceAll(",", "")));
                res.setTwiv(res.getOkt()+res.getNov()+res.getDes());
                res.setTtl(res.getTwi()+res.getTwii()+res.getTwiii()+res.getTwiv());

                if (!Objects.equals(x.getRowNumber(), "")) {
                    tRansactionOmzetSumDto.set(summaryServiceOmzet.sumTotalTransactionByRowNumber(tRansactionOmzetSumDto.get(), x));
                }
                resstream.add(res);
            });
            String chanel = TransactionEntryLtsOmzet.class.getCanonicalName().concat(request.getTahun()).concat(request.getPeriode()).concat(request.getUnit());
            redisConfig.setMessageListener(chanel);
            redisPublishConfig.publish(mapper.writeValueAsString(resstream),chanel);
            return ResponseUtil.buildResponse(
                    SUCCESS,
                    INQUIRY_SUCCESS,
                    response,
                    HttpStatus.OK);

        }catch (Exception e){
            log.info("error inquiry Omzet : {}",e.getMessage());
            return ResponseUtil.buildResponse(
                    FAILED,
                    INQUIRY_FAILED,
                    null,
                    HttpStatus.BAD_REQUEST);
        }
    }
    @Transactional
    public ResponseEntity<Object> saveDataEntryLtsOmzet(TransactionEntryLtsOmzet request) {
        log.info("proccess saving data entry Lts omzet");
        try {
            transactionEntryLtsOmzetRepository.save(request);
            return ResponseUtil.buildResponse(SUCCESS, "DATA SAVED", null, HttpStatus.OK);
        } catch (Exception e) {
            throw e;
        }
    }


    // TODO: 8/29/2022 rendika get dataEntryMedicalOmzet

    @Transactional
    public ResponseEntity<Object> saveDataEntryMedicalOmzet(TransactionEntryMedicalOmzet request) {
        log.info("proccess saving data entry Medical omzet");
        try {
            transactionEntryMedicalOmzetRepository.save(request);
            return ResponseUtil.buildResponse(SUCCESS, "DATA SAVED", null, HttpStatus.OK);
        } catch (Exception e) {
            throw e;
        }
    }


    /**
     *
     * @param request
     * @param metadata
     * @return
     * @throws JsonProcessingException
     */
    //#region get getDataEntryDistOmzet
    public ResponseEntity<Object> getDataEntryRegOmzet(CommonRequestDto request, Metadata metadata) {
        List<TransactionEntryRegOmzet> listTable = new ArrayList<>();
        CommonResponseDto response = new CommonResponseDto();
        try {
            listTable =  transactionEntryRegOmzetRepository.findByYearAndPeriodeAndCabangDivisi(request.getTahun(),request.getPeriode(),request.getUnit());
            AtomicReference<TRansactionOmzetSumDto> tRansactionOmzetSumDto = new AtomicReference<>(new TRansactionOmzetSumDto());
            List<TransactionOmzetDto> resstream = new ArrayList<>();
            listTable.stream().forEach(x->{

                if (!Objects.equals(x.getFormula(), "")) {
                    resstream.add(summaryServiceOmzet.sumTotal(tRansactionOmzetSumDto.get(), x));
                    tRansactionOmzetSumDto.set(new TRansactionOmzetSumDto());
                    return;
                }
                TransactionOmzetDto res = new TransactionOmzetDto();
                res.setId(x.getId());
                res.setProduct(x.getProduct());
                res.setCabangDivisi(x.getCabangDivisi());
                res.setPeriode(x.getPeriode());
                res.setYear(x.getYear());
                res.setKeterangan(x.getKeterangan());
                res.setGroupProd(x.getGroupProd());
                res.setRowNumber(x.getRowNumber());
                res.setJan(ObjectUtils.isEmpty(x.getJan()) ? 0L : Long.valueOf(x.getJan().replaceAll(",", "")));
                res.setFeb(ObjectUtils.isEmpty(x.getFeb()) ? 0L : Long.valueOf(x.getFeb().replaceAll(",", "")));
                res.setMar(ObjectUtils.isEmpty(x.getMar()) ? 0L : Long.valueOf(x.getMar().replaceAll(",", "")));
                res.setTwi(res.getMar()+res.getJan()+res.getFeb());
                res.setApr(ObjectUtils.isEmpty(x.getApr()) ? 0L : Long.valueOf(x.getApr().replaceAll(",", "")));
                res.setMei(ObjectUtils.isEmpty(x.getMei()) ? 0L : Long.valueOf(x.getMei().replaceAll(",", "")));
                res.setJun(ObjectUtils.isEmpty(x.getJun()) ? 0L : Long.valueOf(x.getJun().replaceAll(",", "")));
                res.setTwii(res.getApr()+res.getMei()+res.getJun());
                res.setJul(ObjectUtils.isEmpty(x.getJul()) ? 0L : Long.valueOf(x.getJul().replaceAll(",", "")));
                res.setAgt(ObjectUtils.isEmpty(x.getAgt()) ? 0L : Long.valueOf(x.getAgt().replaceAll(",", "")));
                res.setSep(ObjectUtils.isEmpty(x.getSep()) ? 0L : Long.valueOf(x.getSep().replaceAll(",", "")));
                res.setTwiii(res.getJul()+res.getAgt()+res.getSep());
                res.setOkt(ObjectUtils.isEmpty(x.getOkt()) ? 0L : Long.valueOf(x.getOkt().replaceAll(",", "")));
                res.setNov(ObjectUtils.isEmpty(x.getNov()) ? 0L : Long.valueOf(x.getNov().replaceAll(",", "")));
                res.setDes(ObjectUtils.isEmpty(x.getDes()) ? 0L : Long.valueOf(x.getDes().replaceAll(",", "")));
                res.setTwiv(res.getOkt()+res.getNov()+res.getDes());
                res.setTtl(res.getTwi()+res.getTwii()+res.getTwiii()+res.getTwiv());

                if (!Objects.equals(x.getRowNumber(), "")) {
                    tRansactionOmzetSumDto.set(summaryServiceOmzet.sumTotalTransactionByRowNumber(tRansactionOmzetSumDto.get(), x));
                }
                resstream.add(res);
            });
            String chanel = TransactionEntryRegOmzet.class.getCanonicalName().concat(request.getTahun()).concat(request.getPeriode()).concat(request.getUnit());
            redisConfig.setMessageListener(chanel);
            redisPublishConfig.publish(mapper.writeValueAsString(resstream),chanel);
            return ResponseUtil.buildResponse(
                    SUCCESS,
                    INQUIRY_SUCCESS,
                    response,
                    HttpStatus.OK);

        }catch (Exception e){
            log.info("error inquiry Omzet : {}",e.getMessage());
            return ResponseUtil.buildResponse(
                    FAILED,
                    INQUIRY_FAILED,
                    null,
                    HttpStatus.BAD_REQUEST);
        }
    }

    @Transactional
    public ResponseEntity<Object> saveDataEntryRegOmzet(TransactionEntryRegOmzet request) {
        log.info("proccess saving data entry Reg omzet");
        try {
            transactionEntryRegOmzetRepository.save(request);
            return ResponseUtil.buildResponse(SUCCESS, "DATA SAVED", null, HttpStatus.OK);
        } catch (Exception e) {
            throw e;
        }
    }


    /**
     *
     * @param request
     * @param metadata
     * @return
     * @throws JsonProcessingException
     */
    //#region get getDataEntryDistOmzet
    public ResponseEntity<Object> getDataEntryServiceOmzet(CommonRequestDto request, Metadata metadata) {
        List<TransactionEntryServiceOmzet> listTable = new ArrayList<>();
        CommonResponseDto response = new CommonResponseDto();
        try {
            listTable =  transactionEntryServiceOmzetRepository.findByYearAndPeriodeAndCabangDivisi(request.getTahun(),request.getPeriode(),request.getUnit());
            AtomicReference<TRansactionOmzetSumDto> tRansactionOmzetSumDto = new AtomicReference<>(new TRansactionOmzetSumDto());
            List<TransactionOmzetDto> resstream = new ArrayList<>();
            listTable.stream().forEach(x->{

                if (!Objects.equals(x.getFormula(), "")) {
                    resstream.add(summaryServiceOmzet.sumTotal(tRansactionOmzetSumDto.get(), x));
                    tRansactionOmzetSumDto.set(new TRansactionOmzetSumDto());
                    return;
                }
                TransactionOmzetDto res = new TransactionOmzetDto();
                res.setId(x.getId());
                res.setProduct(x.getProduct());
                res.setCabangDivisi(x.getCabangDivisi());
                res.setPeriode(x.getPeriode());
                res.setYear(x.getYear());
                res.setKeterangan(x.getKeterangan());
                res.setGroupProd(x.getGroupProd());
                res.setRowNumber(x.getRowNumber());
                res.setJan(ObjectUtils.isEmpty(x.getJan()) ? 0L : Long.valueOf(x.getJan().replaceAll(",", "")));
                res.setFeb(ObjectUtils.isEmpty(x.getFeb()) ? 0L : Long.valueOf(x.getFeb().replaceAll(",", "")));
                res.setMar(ObjectUtils.isEmpty(x.getMar()) ? 0L : Long.valueOf(x.getMar().replaceAll(",", "")));
                res.setTwi(res.getMar()+res.getJan()+res.getFeb());
                res.setApr(ObjectUtils.isEmpty(x.getApr()) ? 0L : Long.valueOf(x.getApr().replaceAll(",", "")));
                res.setMei(ObjectUtils.isEmpty(x.getMei()) ? 0L : Long.valueOf(x.getMei().replaceAll(",", "")));
                res.setJun(ObjectUtils.isEmpty(x.getJun()) ? 0L : Long.valueOf(x.getJun().replaceAll(",", "")));
                res.setTwii(res.getApr()+res.getMei()+res.getJun());
                res.setJul(ObjectUtils.isEmpty(x.getJul()) ? 0L : Long.valueOf(x.getJul().replaceAll(",", "")));
                res.setAgt(ObjectUtils.isEmpty(x.getAgt()) ? 0L : Long.valueOf(x.getAgt().replaceAll(",", "")));
                res.setSep(ObjectUtils.isEmpty(x.getSep()) ? 0L : Long.valueOf(x.getSep().replaceAll(",", "")));
                res.setTwiii(res.getJul()+res.getAgt()+res.getSep());
                res.setOkt(ObjectUtils.isEmpty(x.getOkt()) ? 0L : Long.valueOf(x.getOkt().replaceAll(",", "")));
                res.setNov(ObjectUtils.isEmpty(x.getNov()) ? 0L : Long.valueOf(x.getNov().replaceAll(",", "")));
                res.setDes(ObjectUtils.isEmpty(x.getDes()) ? 0L : Long.valueOf(x.getDes().replaceAll(",", "")));
                res.setTwiv(res.getOkt()+res.getNov()+res.getDes());
                res.setTtl(res.getTwi()+res.getTwii()+res.getTwiii()+res.getTwiv());

                if (!Objects.equals(x.getRowNumber(), "")) {
                    tRansactionOmzetSumDto.set(summaryServiceOmzet.sumTotalTransactionByRowNumber(tRansactionOmzetSumDto.get(), x));
                }
                resstream.add(res);
            });
            String chanel = TransactionEntryServiceOmzet.class.getCanonicalName().concat(request.getTahun()).concat(request.getPeriode()).concat(request.getUnit());
            redisConfig.setMessageListener(chanel);
            redisPublishConfig.publish(mapper.writeValueAsString(resstream),chanel);
            return ResponseUtil.buildResponse(
                    SUCCESS,
                    INQUIRY_SUCCESS,
                    response,
                    HttpStatus.OK);

        }catch (Exception e){
            log.info("error inquiry Omzet : {}",e.getMessage());
            return ResponseUtil.buildResponse(
                    FAILED,
                    INQUIRY_FAILED,
                    null,
                    HttpStatus.BAD_REQUEST);
        }
    }

    @Transactional
    public ResponseEntity<Object> saveDataEntryServiceOmzet(TransactionEntryServiceOmzet request) {
        log.info("proccess saving data entry Service omzet");
        try {
            transactionEntryServiceOmzetRepository.save(request);
            return ResponseUtil.buildResponse(SUCCESS, "DATA SAVED", null, HttpStatus.OK);
        } catch (Exception e) {
            throw e;
        }
    }


  //TODO

    @Transactional
    public ResponseEntity<Object> saveDataEntrySurgeryOmzet(TransactionEntrySurgeryOmzet request) {
        log.info("proccess saving data entry Service omzet");
        try {
            transactionEntrySurgeryOmzetRepository.save(request);
            return ResponseUtil.buildResponse(SUCCESS, "DATA SAVED", null, HttpStatus.OK);
        } catch (Exception e) {
            throw e;
        }
    }


    /**
     *
     * @param request
     * @param metadata
     * @return
     * @throws JsonProcessingException
     */
    //#region get getDataEntryDistOmzet
    public ResponseEntity<Object> getDatanEntryTenderOmzet(CommonRequestDto request, Metadata metadata) {
        List<TransactionEntryTenderOmzet> listTable = new ArrayList<>();
        CommonResponseDto response = new CommonResponseDto();
        try {
            listTable =  transactionEntryTenderOmzetRepository.findByYearAndPeriodeAndCabangDivisi(request.getTahun(),request.getPeriode(),request.getUnit());
            AtomicReference<TRansactionOmzetSumDto> tRansactionOmzetSumDto = new AtomicReference<>(new TRansactionOmzetSumDto());
            List<TransactionOmzetDto> resstream = new ArrayList<>();
            listTable.stream().forEach(x->{

                if (!Objects.equals(x.getFormula(), "")) {
                    resstream.add(summaryServiceOmzet.sumTotal(tRansactionOmzetSumDto.get(), x));
                    tRansactionOmzetSumDto.set(new TRansactionOmzetSumDto());
                    return;
                }
                TransactionOmzetDto res = new TransactionOmzetDto();
                res.setId(x.getId());
                res.setProduct(x.getProduct());
                res.setCabangDivisi(x.getCabangDivisi());
                res.setPeriode(x.getPeriode());
                res.setYear(x.getYear());
                res.setKeterangan(x.getKeterangan());
                res.setGroupProd(x.getGroupProd());
                res.setRowNumber(x.getRowNumber());
                res.setJan(ObjectUtils.isEmpty(x.getJan()) ? 0L : Long.valueOf(x.getJan().replaceAll(",", "")));
                res.setFeb(ObjectUtils.isEmpty(x.getFeb()) ? 0L : Long.valueOf(x.getFeb().replaceAll(",", "")));
                res.setMar(ObjectUtils.isEmpty(x.getMar()) ? 0L : Long.valueOf(x.getMar().replaceAll(",", "")));
                res.setTwi(res.getMar()+res.getJan()+res.getFeb());
                res.setApr(ObjectUtils.isEmpty(x.getApr()) ? 0L : Long.valueOf(x.getApr().replaceAll(",", "")));
                res.setMei(ObjectUtils.isEmpty(x.getMei()) ? 0L : Long.valueOf(x.getMei().replaceAll(",", "")));
                res.setJun(ObjectUtils.isEmpty(x.getJun()) ? 0L : Long.valueOf(x.getJun().replaceAll(",", "")));
                res.setTwii(res.getApr()+res.getMei()+res.getJun());
                res.setJul(ObjectUtils.isEmpty(x.getJul()) ? 0L : Long.valueOf(x.getJul().replaceAll(",", "")));
                res.setAgt(ObjectUtils.isEmpty(x.getAgt()) ? 0L : Long.valueOf(x.getAgt().replaceAll(",", "")));
                res.setSep(ObjectUtils.isEmpty(x.getSep()) ? 0L : Long.valueOf(x.getSep().replaceAll(",", "")));
                res.setTwiii(res.getJul()+res.getAgt()+res.getSep());
                res.setOkt(ObjectUtils.isEmpty(x.getOkt()) ? 0L : Long.valueOf(x.getOkt().replaceAll(",", "")));
                res.setNov(ObjectUtils.isEmpty(x.getNov()) ? 0L : Long.valueOf(x.getNov().replaceAll(",", "")));
                res.setDes(ObjectUtils.isEmpty(x.getDes()) ? 0L : Long.valueOf(x.getDes().replaceAll(",", "")));
                res.setTwiv(res.getOkt()+res.getNov()+res.getDes());
                res.setTtl(res.getTwi()+res.getTwii()+res.getTwiii()+res.getTwiv());

                if (!Objects.equals(x.getRowNumber(), "")) {
                    tRansactionOmzetSumDto.set(summaryServiceOmzet.sumTotalTransactionByRowNumber(tRansactionOmzetSumDto.get(), x));
                }
                resstream.add(res);
            });
            String chanel = TransactionEntryTenderOmzet.class.getCanonicalName().concat(request.getTahun()).concat(request.getPeriode()).concat(request.getUnit());
            redisConfig.setMessageListener(chanel);
            redisPublishConfig.publish(mapper.writeValueAsString(resstream),chanel);
            return ResponseUtil.buildResponse(
                    SUCCESS,
                    INQUIRY_SUCCESS,
                    response,
                    HttpStatus.OK);

        }catch (Exception e){
            log.info("error inquiry Omzet : {}",e.getMessage());
            return ResponseUtil.buildResponse(
                    FAILED,
                    INQUIRY_FAILED,
                    null,
                    HttpStatus.BAD_REQUEST);
        }
    }

    @Transactional
    public ResponseEntity<Object> saveDataEntryTenderOmzet(TransactionEntryTenderOmzet request) {
        log.info("proccess saving data entry Tender omzet");
        try {
            transactionEntryTenderOmzetRepository.save(request);
            return ResponseUtil.buildResponse(SUCCESS, "DATA SAVED", null, HttpStatus.OK);
        } catch (Exception e) {
            throw e;
        }
    }


    /**
     *
     * @param request
     * @param metadata
     * @return
     * @throws JsonProcessingException
     */
    //#region get getDataEntryDistOmzet
    public ResponseEntity<Object> getDatanEntryTradingOmzet(CommonRequestDto request, Metadata metadata) {
        List<TransactionEntryTradingOmzet> listTable = new ArrayList<>();
        CommonResponseDto response = new CommonResponseDto();
        try {
            listTable =  transactionEntryTradingOmzetRepository.findByYearAndPeriodeAndCabangDivisi(request.getTahun(),request.getPeriode(),request.getUnit());
            AtomicReference<TRansactionOmzetSumDto> tRansactionOmzetSumDto = new AtomicReference<>(new TRansactionOmzetSumDto());
            List<TransactionOmzetDto> resstream = new ArrayList<>();
            listTable.stream().forEach(x->{

                if (!Objects.equals(x.getFormula(), "")) {
                    resstream.add(summaryServiceOmzet.sumTotal(tRansactionOmzetSumDto.get(), x));
                    tRansactionOmzetSumDto.set(new TRansactionOmzetSumDto());
                    return;
                }
                TransactionOmzetDto res = new TransactionOmzetDto();
                res.setId(x.getId());
                res.setProduct(x.getProduct());
                res.setCabangDivisi(x.getCabangDivisi());
                res.setPeriode(x.getPeriode());
                res.setYear(x.getYear());
                res.setKeterangan(x.getKeterangan());
                res.setGroupProd(x.getGroupProd());
                res.setRowNumber(x.getRowNumber());
                res.setJan(ObjectUtils.isEmpty(x.getJan()) ? 0L : Long.valueOf(x.getJan().replaceAll(",", "")));
                res.setFeb(ObjectUtils.isEmpty(x.getFeb()) ? 0L : Long.valueOf(x.getFeb().replaceAll(",", "")));
                res.setMar(ObjectUtils.isEmpty(x.getMar()) ? 0L : Long.valueOf(x.getMar().replaceAll(",", "")));
                res.setTwi(res.getMar()+res.getJan()+res.getFeb());
                res.setApr(ObjectUtils.isEmpty(x.getApr()) ? 0L : Long.valueOf(x.getApr().replaceAll(",", "")));
                res.setMei(ObjectUtils.isEmpty(x.getMei()) ? 0L : Long.valueOf(x.getMei().replaceAll(",", "")));
                res.setJun(ObjectUtils.isEmpty(x.getJun()) ? 0L : Long.valueOf(x.getJun().replaceAll(",", "")));
                res.setTwii(res.getApr()+res.getMei()+res.getJun());
                res.setJul(ObjectUtils.isEmpty(x.getJul()) ? 0L : Long.valueOf(x.getJul().replaceAll(",", "")));
                res.setAgt(ObjectUtils.isEmpty(x.getAgt()) ? 0L : Long.valueOf(x.getAgt().replaceAll(",", "")));
                res.setSep(ObjectUtils.isEmpty(x.getSep()) ? 0L : Long.valueOf(x.getSep().replaceAll(",", "")));
                res.setTwiii(res.getJul()+res.getAgt()+res.getSep());
                res.setOkt(ObjectUtils.isEmpty(x.getOkt()) ? 0L : Long.valueOf(x.getOkt().replaceAll(",", "")));
                res.setNov(ObjectUtils.isEmpty(x.getNov()) ? 0L : Long.valueOf(x.getNov().replaceAll(",", "")));
                res.setDes(ObjectUtils.isEmpty(x.getDes()) ? 0L : Long.valueOf(x.getDes().replaceAll(",", "")));
                res.setTwiv(res.getOkt()+res.getNov()+res.getDes());
                res.setTtl(res.getTwi()+res.getTwii()+res.getTwiii()+res.getTwiv());

                if (!Objects.equals(x.getRowNumber(), "")) {
                    tRansactionOmzetSumDto.set(summaryServiceOmzet.sumTotalTransactionByRowNumber(tRansactionOmzetSumDto.get(), x));
                }
                resstream.add(res);
            });
            String chanel = TransactionEntryTradingOmzet.class.getCanonicalName().concat(request.getTahun()).concat(request.getPeriode()).concat(request.getUnit());
            redisConfig.setMessageListener(chanel);
            redisPublishConfig.publish(mapper.writeValueAsString(resstream),chanel);
            return ResponseUtil.buildResponse(
                    SUCCESS,
                    INQUIRY_SUCCESS,
                    response,
                    HttpStatus.OK);

        }catch (Exception e){
            log.info("error inquiry Omzet : {}",e.getMessage());
            return ResponseUtil.buildResponse(
                    FAILED,
                    INQUIRY_FAILED,
                    null,
                    HttpStatus.BAD_REQUEST);
        }
    }

    @Transactional
    public ResponseEntity<Object> saveDataEntryTradingOmzet(TransactionEntryTradingOmzet request) {
        log.info("proccess saving data entry trading omzet");
        try {
            transactionEntryTradingOmzetRepository.save(request);
            return ResponseUtil.buildResponse(SUCCESS, "DATA SAVED", null, HttpStatus.OK);
        } catch (Exception e) {
            throw e;
        }
    }


    // TODO: 8/29/2022 rendika get saveDataEntryDistShare

    @Transactional
    public ResponseEntity<Object> saveDataEntryDistShare(TransactionEntryDistShare request) {
        log.info("proccess saving data entry dist Share");
        try {
            transactionEntryDistSahreRepository.save(request);
            return ResponseUtil.buildResponse(SUCCESS, "DATA SAVED", null, HttpStatus.OK);
        } catch (Exception e) {
            throw e;
        }
    }


    // TODO: 8/29/2022 rendika get saveDataEntryEquipmentShare

    public ResponseEntity<Object> saveDataEntryEquipmentShare(TransactionEntryEquipmentShare request) {
        log.info("proccess saving data entry equipment Share");
        try {
            transactionEntryEquipmentShareRepository.save(request);
            return ResponseUtil.buildResponse(SUCCESS, "DATA SAVED", null, HttpStatus.OK);
        } catch (Exception e) {
            throw e;
        }
    }


    // TODO: 8/29/2022 rendika get saveDataEntryHcShare

    public ResponseEntity<Object> saveDataEntryHcShare(TransactionEntryHcShare request) {
        log.info("proccess saving data entry hc Share");
        try {
            transactionEntryHcShareRepository.save(request);
            return ResponseUtil.buildResponse(SUCCESS, "DATA SAVED", null, HttpStatus.OK);
        } catch (Exception e) {
            throw e;
        }
    }


    // TODO: 8/29/2022 rendika get saveDataEntryLdShare

    public ResponseEntity<Object> saveDataEntryLdShare(TransactionEntryLdShare request) {
        log.info("proccess saving data entry LD Share");
        try {
            transactionEntryLdShareRepository.save(request);
            return ResponseUtil.buildResponse(SUCCESS, "DATA SAVED", null, HttpStatus.OK);
        } catch (Exception e) {
            throw e;
        }
    }


    // TODO: 8/29/2022 rendika get saveDataEntryLtsShare

    public ResponseEntity<Object> saveDataEntryLtsShare(TransactionEntryLtsShare request) {
        log.info("proccess saving data entry Lts Share");
        try {
            transactionEntryLtsShareRepository.save(request);
            return ResponseUtil.buildResponse(SUCCESS, "DATA SAVED", null, HttpStatus.OK);
        } catch (Exception e) {
            throw e;
        }
    }


    // TODO: 8/29/2022 rendika get saveDataEntryMedicalShare

    public ResponseEntity<Object> saveDataEntryMedicalShare(TransactionEntryMedicalShare request) {
        log.info("proccess saving data entry Medical Share");
        try {
            transactionEntryMedicalShareRepository.save(request);
            return ResponseUtil.buildResponse(SUCCESS, "DATA SAVED", null, HttpStatus.OK);
        } catch (Exception e) {
            throw e;
        }
    }


    // TODO: 8/29/2022 rendika get saveDataEntryRegShare

    public ResponseEntity<Object> saveDataEntryRegShare(TransactionEntryRegShare request) {
        log.info("proccess saving data entry reg Share");
        try {
            transactionEntryRegShareRepository.save(request);
            return ResponseUtil.buildResponse(SUCCESS, "DATA SAVED", null, HttpStatus.OK);
        } catch (Exception e) {
            throw e;
        }
    }


    // TODO: 8/29/2022 rendika get saveDataEntryServiceShare

    public ResponseEntity<Object> saveDataEntryServiceShare(TransactionEntryServiceShare request) {
        log.info("proccess saving data entry service Share");
        try {
            transactionEntryServiceShareRepository.save(request);
            return ResponseUtil.buildResponse(SUCCESS, "DATA SAVED", null, HttpStatus.OK);
        } catch (Exception e) {
            throw e;
        }
    }


    // TODO: 8/29/2022 rendika get saveDataEntrySurgeryShare

    public ResponseEntity<Object> saveDataEntrySurgeryShare(TransactionEntrySurgeryShare request) {
        log.info("proccess saving data entry surgery Share");
        try {
            transactionEntrySurgeryShareRepository.save(request);
            return ResponseUtil.buildResponse(SUCCESS, "DATA SAVED", null, HttpStatus.OK);
        } catch (Exception e) {
            throw e;
        }
    }

    public ResponseEntity<Object> saveDataEntryTenderShare(TransactionEntryTenderShare request) {
        log.info("proccess saving data entry tender Share");
        try {
            transactionEntryTenderShareRepository.save(request);
            return ResponseUtil.buildResponse(SUCCESS, "DATA SAVED", null, HttpStatus.OK);
        } catch (Exception e) {
            throw e;
        }
    }

    public ResponseEntity<Object> saveDataEntryTradingShare(TransactionEntryTradingShare request) {
        log.info("proccess saving data entry trading Share");
        try {
            TransactionEntryTradingShareRepository.save(request);
            return ResponseUtil.buildResponse(SUCCESS, "DATA SAVED", null, HttpStatus.OK);
        } catch (Exception e) {
            throw e;
        }
    }





}
