package com.rni.rni.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rni.rni.domain.dao.*;
import com.rni.rni.domain.dto.CommonDto;
import com.rni.rni.domain.dto.Metadata;
import com.rni.rni.exception.NotFoundException;
import com.rni.rni.repository.CoaHeaderRepository;
import com.rni.rni.repository.TableMasterSchedulerRepository;
import com.rni.rni.utils.GeneralUtils;
import com.rni.rni.utils.InsertLogMonitoring;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import util.ResponseUtil;

import javax.transaction.Transactional;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import static com.rni.rni.constant.Constant.responseKey.SUCCESS;
import static com.rni.rni.constant.Constant.responseKey.UPLOADING;
import static com.rni.rni.utils.ConvertionUtils.convertStringtoDate;

@Slf4j
@Service
public class FinanceService {

    @Autowired
    private InsertLogMonitoring insertLogMonitoring;

    @Autowired
    private TableMasterSchedulerRepository tableMasterSchedulerRepository;

    @Autowired
    private CoaHeaderRepository coaHeaderRepository;




    @Transactional
    public ResponseEntity<Object> faCategory(Workbook workbook, Sheet sheet, Long size, Metadata metadata) throws IOException {
        log.info("Processing Update fa_category");
        List<FaCategory> faCategory = new ArrayList<>();
        TableMasterScheduler tableMasterScheduler = new TableMasterScheduler();
        CommonDto commonDto = new CommonDto();
        try {
            Iterator<Row> rows = sheet.iterator();
            int rowNumber = 0;
            while (rows.hasNext()) {
                FaCategory category = new FaCategory();
                Row currentRow = rows.next();
                if (rowNumber == 0) {
                    rowNumber++;
                    continue;
                }
                Iterator<Cell> cellInRow = currentRow.iterator();
                int cellIdx = 0;
                while (cellInRow.hasNext()) {
                    Cell currentCell = cellInRow.next();
                    switch (cellIdx) {
                        case 0:
                            category.setKeterangan(currentCell.getStringCellValue());
                            break;
                        default:
                            break;
                    }
                    cellIdx++;
                }
                faCategory.add(category);
            }
            workbook.close();
            faCategory.stream().forEach(x -> {
                x.setCreatedBy(metadata.getUsername());
                x.setCreatedAt(new Date());
                x.setCode(GeneralUtils.code("FAC"));
            });
            var logId = insertLogMonitoring.logMonitoring(null, sheet.getSheetName(), "Uploading", size, faCategory.stream().count());
            commonDto.setFaCategories(faCategory);
            tableMasterScheduler.setTemplateName(sheet.getSheetName());
            tableMasterScheduler.setClassName(FaCategory.class.getCanonicalName());
            tableMasterScheduler.setData(new ObjectMapper().writeValueAsString(commonDto));
            tableMasterScheduler.setIdLog(logId);
            tableMasterSchedulerRepository.save(tableMasterScheduler);
            return ResponseUtil.buildResponse(
                    SUCCESS,
                    UPLOADING,
                    null,
                    HttpStatus.OK);
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional
    public ResponseEntity<Object> faClass(Workbook workbook, Sheet sheet, Long size, Metadata metadata) throws IOException {
        log.info("Processing Update fa_class");
        List<FaClass> faClasses = new ArrayList<>();
        TableMasterScheduler tableMasterScheduler = new TableMasterScheduler();
        CommonDto commonDto = new CommonDto();
        try {
            Iterator<Row> rows = sheet.iterator();
            int rowNumber = 0;
            while (rows.hasNext()) {
                FaClass faClass = new FaClass();
                Row currentRow = rows.next();
                if (rowNumber == 0) {
                    rowNumber++;
                    continue;
                }
                Iterator<Cell> cellInRow = currentRow.iterator();
                int cellIdx = 0;
                while (cellInRow.hasNext()) {
                    Cell currentCell = cellInRow.next();
                    switch (cellIdx) {
                        case 0:
                            faClass.setKeterangan(currentCell.getStringCellValue());
                            break;
                        default:
                            break;
                    }
                    cellIdx++;
                }
                faClasses.add(faClass);
            }
            workbook.close();
            faClasses.stream().forEach(x -> {
                x.setCreatedBy(metadata.getUsername());
                x.setCreatedAt(new Date());
                x.setCode(GeneralUtils.code("FACL"));
            });
            var logId = insertLogMonitoring.logMonitoring(null, sheet.getSheetName(), "Uploading", size, faClasses.stream().count());
            commonDto.setFaClasses(faClasses);
            tableMasterScheduler.setTemplateName(sheet.getSheetName());
            tableMasterScheduler.setClassName(FaClass.class.getCanonicalName());
            tableMasterScheduler.setData(new ObjectMapper().writeValueAsString(commonDto));
            tableMasterScheduler.setIdLog(logId);
            tableMasterSchedulerRepository.save(tableMasterScheduler);
            return ResponseUtil.buildResponse(
                    SUCCESS,
                    UPLOADING,
                    null,
                    HttpStatus.OK);
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional
    public ResponseEntity<Object> faSubClass(Workbook workbook, Sheet sheet, Long size, Metadata metadata) throws IOException {
        log.info("Processing Update fa_subClass");
        List<FaSubClass> faSubClasses = new ArrayList<>();
        TableMasterScheduler tableMasterScheduler = new TableMasterScheduler();
        CommonDto commonDto = new CommonDto();
        try {
            Iterator<Row> rows = sheet.iterator();
            int rowNumber = 0;
            while (rows.hasNext()) {
                FaSubClass subClass = new FaSubClass();
                Row currentRow = rows.next();
                if (rowNumber == 0) {
                    rowNumber++;
                    continue;
                }
                Iterator<Cell> cellInRow = currentRow.iterator();
                int cellIdx = 0;
                while (cellInRow.hasNext()) {
                    Cell currentCell = cellInRow.next();
                    switch (cellIdx) {
                        case 0:
                            subClass.setKeterangan(currentCell.getStringCellValue());
                            break;
                        default:
                            break;
                    }
                    cellIdx++;
                }
                faSubClasses.add(subClass);
            }
            workbook.close();
            faSubClasses.stream().forEach(x -> {
                x.setCreatedBy(metadata.getUsername());
                x.setCreatedAt(new Date());
                x.setCode(GeneralUtils.code("FASC"));
            });
            var logId = insertLogMonitoring.logMonitoring(null, sheet.getSheetName(), "Uploading", size, faSubClasses.stream().count());
            commonDto.setFaSubClasses(faSubClasses);
            tableMasterScheduler.setTemplateName(sheet.getSheetName());
            tableMasterScheduler.setClassName(FaSubClass.class.getCanonicalName());
            tableMasterScheduler.setData(new ObjectMapper().writeValueAsString(commonDto));
            tableMasterScheduler.setIdLog(logId);
            tableMasterSchedulerRepository.save(tableMasterScheduler);
            return ResponseUtil.buildResponse(
                    SUCCESS,
                    UPLOADING,
                    null,
                    HttpStatus.OK);
        } catch (Exception e) {
            throw e;
        }
    }
    @Transactional
    public ResponseEntity<Object> fixedAsset(Workbook workbook, Sheet sheet, Long size, Metadata metadata) throws IOException, ParseException {
        log.info("Processing Update Fixed Asset");
        List<FixedAsset> fixedAssets = new ArrayList<>();
        TableMasterScheduler tableMasterScheduler = new TableMasterScheduler();
        CommonDto commonDto = new CommonDto();

        try {
            Iterator<Row> rows = sheet.iterator();
            DataFormatter formatter = new DataFormatter();
            int rowNumber = 0;
            while (rows.hasNext()) {
                FixedAsset fixedAsset = new FixedAsset();
                Row currentRow = rows.next();
                if (rowNumber == 0) {
                    rowNumber++;
                    continue;
                }
                Iterator<Cell> cellInRow = currentRow.iterator();
                int cellIdx = 0;
                while (cellInRow.hasNext()) {
                    Cell currentCell = cellInRow.next();
                    String dataCol = formatter.formatCellValue(sheet.getRow(currentRow.getRowNum()).getCell(currentCell.getColumnIndex()));

                    switch (cellIdx) {
                        case 0:
                            fixedAsset.setDimension_1(Dimension.builder()
                                            .code(dataCol)
                                    .build());
                            break;
                        case 1:
                            fixedAsset.setDimension_2(DimensionValue.builder().code(dataCol).build());
                            break;
                        case 2:
                            fixedAsset.setFaCategory(dataCol);
                            break;
                        case 3:
                            fixedAsset.setFaClass(dataCol);
                            break;
                        case 4:
                            fixedAsset.setFaSubclass(dataCol);
                            break;
                        case 5:
                            fixedAsset.setMerk(dataCol);
                            break;
                        case 6:
                            if (dataCol.equals("")){
                                fixedAsset.setTanggalPerolehan(null);
                            }else{
                                fixedAsset.setTanggalPerolehan(convertStringtoDate(dataCol));
                            }
                            break;
                        case 7:
                            fixedAsset.setHargaPerolehan(BigDecimal.valueOf(Double.valueOf(dataCol.equals("") ? "0" : dataCol)));
                            break;
                        case 8:
                            if (dataCol.equals("")){
                                fixedAsset.setTanggalPenyusutan(null);
                            }else {
                                fixedAsset.setTanggalPenyusutan(convertStringtoDate(dataCol));
                            }
                            break;
                        case 9:
                            fixedAsset.setNilaiPenyusutan(BigDecimal.valueOf(Double.valueOf(dataCol.equals("") ? "0" : dataCol)));
                            break;
                        case 10:
                            if (dataCol.equals("")){
                                fixedAsset.setTanggalNilaiBuku(null);
                            }else {
                                fixedAsset.setTanggalNilaiBuku(convertStringtoDate(dataCol));
                            }
                            break;
                        case 11:
                            fixedAsset.setNilaiBuku(BigDecimal.valueOf(Double.valueOf(dataCol.equals("") ? "0" : dataCol)));
                            break;
                        case 12:
                            fixedAsset.setUnit(BigDecimal.valueOf(Double.valueOf(dataCol.equals("") ? "0" : dataCol)));
                            break;
                        case 13:
                            fixedAsset.setPengguna(dataCol);
                            break;
                        case 14:
                            fixedAsset.setNoKendaraan(dataCol);
                            break;
                        case 15:
                            fixedAsset.setKeterangan(dataCol);
                            break;
                        default:
                            break;
                    }
                    cellIdx++;
                }
                fixedAssets.add(fixedAsset);
            }
            workbook.close();
            fixedAssets.stream().forEach(x -> {
                x.setCreatedBy(metadata.getUsername());
                x.setCreatedAt(new Date());
                x.setCode(GeneralUtils.code("FIX"));
            });
            var logId = insertLogMonitoring.logMonitoring(null, sheet.getSheetName(), "Uploading", size, fixedAssets.stream().count());
            commonDto.setFixedAssets(fixedAssets);
            tableMasterScheduler.setTemplateName(sheet.getSheetName());
            tableMasterScheduler.setClassName(FixedAsset.class.getCanonicalName());
            tableMasterScheduler.setData(new ObjectMapper().writeValueAsString(commonDto));
            tableMasterScheduler.setIdLog(logId);
            tableMasterSchedulerRepository.save(tableMasterScheduler);
            return ResponseUtil.buildResponse(
                    SUCCESS,
                    UPLOADING,
                    null,
                    HttpStatus.OK);
        } catch (Exception e) {
            throw e;
        }
    }

    public ResponseEntity<Object> glAccount(Workbook workbook, Sheet sheet, long size, Metadata metadata) throws IOException {
        log.info("Processing update data gl account");
        List<TbGlAccount> tbGlAccounts = new ArrayList<>();
        TableMasterScheduler tableMasterScheduler = new TableMasterScheduler();
        CommonDto commonDto = new CommonDto();
        try {
            Iterator<Row> rows = sheet.iterator();
            int rowNumber = 0;
            while (rows.hasNext()) {
                TbGlAccount account = new TbGlAccount();
                Row currentRow = rows.next();
                if (rowNumber == 0) {
                    rowNumber++;
                    continue;
                }
                Iterator<Cell> cellInRow = currentRow.iterator();
                int cellIdx = 0;
                while (cellInRow.hasNext()) {
                    Cell currentCell = cellInRow.next();
                    switch (cellIdx) {
                        case 0:
                            CoaHeader coaHeader = coaHeaderRepository.findByKodeRek(currentCell.getStringCellValue());
                            if (ObjectUtils.isEmpty(coaHeader)){
                                throw new NotFoundException("COA NOT FOUND");
                            }
                           account.setAccountCode(currentCell.getStringCellValue());
                            break;
                        case 1:
                            account.setDateTransaction(currentCell.getStringCellValue());
                            break;
                        case 2:
                            account.setDescription(currentCell.getStringCellValue());
                            break;
                        case 3:
                            account.setAmount(Long.valueOf(currentCell.getStringCellValue()));
                            break;
                        default:
                            break;
                    }
                    cellIdx++;
                }
                tbGlAccounts.add(account);
            }
            workbook.close();
            tbGlAccounts.stream().forEach(x -> {
                x.setCreatedAt(new Date());
            });
            var logId = insertLogMonitoring.logMonitoring(null, sheet.getSheetName(), "Uploading", size, tbGlAccounts.stream().count());
            commonDto.setTbGlAccounts(tbGlAccounts);
            tableMasterScheduler.setTemplateName(sheet.getSheetName());
            tableMasterScheduler.setClassName(TbGlAccount.class.getCanonicalName());
            tableMasterScheduler.setData(new ObjectMapper().writeValueAsString(commonDto));
            tableMasterScheduler.setIdLog(logId);
            tableMasterSchedulerRepository.save(tableMasterScheduler);
            return ResponseUtil.buildResponse(
                    SUCCESS,
                    UPLOADING,
                    null,
                    HttpStatus.OK);
        } catch (Exception e) {
            throw e;
        }
    }
}