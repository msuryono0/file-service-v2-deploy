package com.rni.rni.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rni.rni.domain.dao.*;
import com.rni.rni.domain.dto.*;
import com.rni.rni.exception.NotFoundException;
import com.rni.rni.repository.*;
import com.rni.rni.utils.GeneralUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import util.ResponseUtil;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static com.rni.rni.constant.Constant.redisKey.*;
import static com.rni.rni.constant.Constant.responseKey.*;


@Service
@Slf4j
public class DataMasterService {

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private CoaHeaderRepository coaHeaderRepository;

    @Autowired
    private CommonService commonService;

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    private TbGlAccountRepository tbGlAccountRepository;

    @Autowired
    private FixedAssetRepository fixedAssetRepository;

    @Autowired
    private DimensionValueRepository dimensionValueRepository;

    @Autowired
    private DimensionRepository dimensionRepository;

    @Autowired
    private FaClassRepository faClassRepository;

    @Autowired
    private FaCategoryRepository faCategoryRepository;

    @Autowired
    private FaSubClassRepository faSubClassRepository;
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private GroupProdRepository groupProdRepository;
    @Autowired
    private TypeProdRepository typeProdRepository;
    @Autowired
    private UomRepository uomRepository;
    @Autowired
    private SupplierRepository supplierRepository;
    @Autowired
    private KaryawanRepository karyawanRepository;

    @Autowired
    private DivisiRepository divisiRepository;
    @Autowired
    private JabatanRepository jabatanRepository;

    @Autowired
    private GolonganRepository golonganRepository;

    @Autowired
    private StatusKaryawanRepository statusKaryawanRepository;

    @Autowired
    private GajiKaryawanRepository gajiKaryawanRepository;
    @Autowired
    private PerjalananDinasRepository perjalananDinasRepository;
    @Autowired
    private PerjalananDinasLocalRepository perjalananDinasLocalRepository;

    public ResponseEntity<Object> getAllCoa(CoaRequestDto requestDto, Metadata metadata) throws JsonProcessingException {
        List<CoaHeader> coaHeaders = new ArrayList<>();
        CoaResponseDto dtoCoa = new CoaResponseDto();
        try {
            if (!ObjectUtils.isEmpty(requestDto.getSearch()))
                coaHeaders = List.of(coaHeaderRepository.findByKodeRek(TbGlAccount.builder().accountCode(requestDto.getSearch()).build()));
            else {
                String data = (String) redisTemplate.boundValueOps(KEY_COA).get();
                if (ObjectUtils.isEmpty(data)) {
                    coaHeaders = commonService.findAllCoa();
                } else {
                    coaHeaders = mapper.readValue(data, new TypeReference<List<CoaHeader>>() {
                    });
                }
            }
            PagedListHolder paging = new PagedListHolder(coaHeaders);
            paging = commonService.setData(paging, requestDto.getLimit(), requestDto.getPage());
            dtoCoa.setTotalPage(paging.getPageCount());
            dtoCoa.setNumberOfElement(paging.getPageList().size());
            dtoCoa.setPageNumber(coaHeaders.size());
            dtoCoa.setTotalElement(paging.getNrOfElements());
            dtoCoa.setCoaHeaders(paging.getPageList());
            return ResponseUtil.buildResponse(
                    SUCCESS,
                    "SUCCESS GET DATA",
                    dtoCoa,
                    HttpStatus.OK);
        } catch (Exception e) {
            throw e;
        }
    }

    public ResponseEntity<Object> saveDataCoa(CommonSaveDataDto commonSaveDataDto, Metadata metadata) {
        CoaHeader coaHeader = new CoaHeader();
        ResponseEntity<Object> response = ResponseUtil.buildResponse(
                FAILED,
                "INTERNAL_SERVER_ERROR",
                null,
                HttpStatus.INTERNAL_SERVER_ERROR);
        try {
            if (Objects.isNull(commonSaveDataDto.getId())) {
                log.info("Insert Data");
                coaHeader.setCreatedAt(new Date());
                coaHeader.setCreatedBy(metadata.getUsername());
                coaHeader.setKodeRek(tbGlAccountRepository.findByAccountCode(commonSaveDataDto.getKodeRek()));
                coaHeader.setDeleted(false);
            } else {
                log.info("Update Data");
                coaHeader = coaHeaderRepository.findById(commonSaveDataDto.getId()).get();
                coaHeader.setUpdatedAt(new Date());
                coaHeader.setUpdatedBy(metadata.getUsername());
            }
            coaHeader.setKeterangan(commonSaveDataDto.getKeterangan());
            coaHeaderRepository.save(coaHeader);
            redisTemplate.delete(KEY_COA);
            response = ResponseUtil.buildResponse(
                    SUCCESS,
                    "DATA SAVED",
                    null,
                    HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    public ResponseEntity<Object> deleteDataCod(CommonDeleteData dataCoaDto, Metadata metadata) {
        CoaHeader coaHeader;
        try {
            coaHeader = coaHeaderRepository.findById(dataCoaDto.getId()).get();
            if (Objects.nonNull(coaHeader)) {
                log.info("deleting data");
                coaHeader.setDeleted(true);
            }
            coaHeaderRepository.save(coaHeader);
            redisTemplate.delete(KEY_COA);
            return ResponseUtil.buildResponse(
                    SUCCESS,
                    "DATA DELETED",
                    null,
                    HttpStatus.OK);
        } catch (Exception e) {
            throw e;
        }
    }

    public ResponseEntity<Object> getDataFixedAsset(CoaRequestDto req, Metadata metadata) throws JsonProcessingException {
        List<FixedAsset> fixedAssets = new ArrayList<>();
        FixedAssetResponseDto fixedAssetResponseDto = new FixedAssetResponseDto();
        ResponseEntity<Object> response = ResponseUtil.buildResponse(
                FAILED,
                "INTERNAL_SERVER_ERROR",
                null,
                HttpStatus.INTERNAL_SERVER_ERROR);
        try {

            if (!req.getSearch().isEmpty()) {
                fixedAssets = List.of(fixedAssetRepository.findByCode(req.getSearch()));
            } else {
                String data = (String) redisTemplate.boundValueOps(KEY_ASSET).get();
                if (Objects.isNull(data)) {
                    fixedAssets = commonService.findAsset();
                } else {
                    fixedAssets = mapper.readValue(data, new TypeReference<>() {
                    });
                }
            }
            PagedListHolder paging = new PagedListHolder(fixedAssets);
            paging = commonService.setData(paging, req.getLimit(), req.getPage());
            fixedAssetResponseDto.setTotalPage(paging.getPageCount());
            fixedAssetResponseDto.setNumberOfElement(paging.getPageList().size());
            fixedAssetResponseDto.setPageNumber(fixedAssets.size());
            fixedAssetResponseDto.setTotalElement(paging.getNrOfElements());
            fixedAssetResponseDto.setFixedAssets(paging.getPageList());
            response = ResponseUtil.buildResponse(
                    SUCCESS,
                    "SUCCESS GET DATA",
                    fixedAssetResponseDto,
                    HttpStatus.OK);
        } catch (Exception e) {
            log.error("error while getting data fixed asset", e);
        }
        return response;
    }

    public ResponseEntity<Object> saveDataFixAsset(FixAssetSaveDataDto req, Metadata metadata) {
        FixedAsset fixedAsset = new FixedAsset();
        try {
            if (Objects.isNull(req.getId())) {
                log.info("insert data aset");
                fixedAsset.setCode(GeneralUtils.code("FIX"));
                fixedAsset.setDimension_1(dimensionRepository.findByCode(req.getDimension_1()));
                fixedAsset.setDimension_2(dimensionValueRepository.findByCode(req.getDimension_2()));
                fixedAsset.setFaCategory(req.getFaCategory());
                fixedAsset.setFaClass(req.getFaClass());
                fixedAsset.setFaSubclass(req.getFaSubclass());
                fixedAsset.setMerk(req.getMerk());
                fixedAsset.setTanggalPerolehan(req.getTanggalPerolehan());
                fixedAsset.setHargaPerolehan(new BigDecimal(req.getHargaPerolehan()));
                fixedAsset.setTanggalPenyusutan(req.getTanggalPenyusutan());
                fixedAsset.setNilaiPenyusutan(new BigDecimal(req.getNilaiPenyusutan()));
                fixedAsset.setTanggalNilaiBuku(req.getTanggalNilaiBuku());
                fixedAsset.setNilaiBuku(new BigDecimal(req.getNilaiBuku()));
                fixedAsset.setUnit(new BigDecimal(req.getUnit()));
                fixedAsset.setPengguna(req.getPengguna());
                fixedAsset.setNoKendaraan(req.getNoKendaraan());
                fixedAsset.setKeterangan(req.getKeterangan());
                fixedAsset.setCreatedBy(metadata.getUsername());
                fixedAsset.setCreatedAt(new Date());
                fixedAsset.setDeleted(false);
            } else {
                log.info("Update Data fixed asset");
                fixedAsset = fixedAssetRepository.findById(req.getId()).get();
                fixedAsset.setDimension_1(dimensionRepository.findByCode(req.getDimension_1()));
                fixedAsset.setDimension_2(dimensionValueRepository.findByCode(req.getDimension_2()));
                fixedAsset.setFaCategory(req.getFaCategory());
                fixedAsset.setFaClass(req.getFaClass());
                fixedAsset.setFaSubclass(req.getFaSubclass());
                fixedAsset.setMerk(req.getMerk());
                fixedAsset.setTanggalPerolehan(req.getTanggalPerolehan());
                fixedAsset.setHargaPerolehan(new BigDecimal(req.getHargaPerolehan()));
                fixedAsset.setTanggalPenyusutan(req.getTanggalPenyusutan());
                fixedAsset.setNilaiPenyusutan(new BigDecimal(req.getNilaiPenyusutan()));
                fixedAsset.setTanggalNilaiBuku(req.getTanggalNilaiBuku());
                fixedAsset.setNilaiBuku(new BigDecimal(req.getNilaiBuku()));
                fixedAsset.setUnit(new BigDecimal(req.getUnit()));
                fixedAsset.setPengguna(req.getPengguna());
                fixedAsset.setNoKendaraan(req.getNoKendaraan());
                fixedAsset.setKeterangan(req.getKeterangan());
                fixedAsset.setUpdatedBy(metadata.getUsername());
                fixedAsset.setUpdatedAt(new Date());
            }
            fixedAssetRepository.saveAndFlush(fixedAsset);
            redisTemplate.delete(KEY_ASSET);
            return ResponseUtil.buildResponse(
                    SUCCESS,
                    "DATA SAVED",
                    null,
                    HttpStatus.OK);
        }catch (Exception e){
            throw e;
        }
    }

    public ResponseEntity<Object> deleteFixedAsset(CommonDeleteData req, Metadata metadata) {
        try {
            FixedAsset fixedAsset = fixedAssetRepository.findById(req.getId()).get();
            if (Objects.nonNull(fixedAsset)) {
                log.info("deleting data");
                fixedAsset.setUpdatedAt(new Date());
                fixedAsset.setUpdatedBy(metadata.getUsername());
                fixedAsset.setDeleted(true);
            }
            fixedAssetRepository.save(fixedAsset);
            redisTemplate.delete(KEY_ASSET);
            return ResponseUtil.buildResponse(
                    SUCCESS,
                    "DATA DELETED",
                    null,
                    HttpStatus.OK);
        } catch (Exception e) {
            throw e;
        }
    }

    public ResponseEntity<Object> getdataFaCategory(CoaRequestDto req, Metadata metadata) throws JsonProcessingException {
        List<FaCategory> faCategories;
        FaCategoryFindResponseDto response = new FaCategoryFindResponseDto();
        try {
            faCategories = faCategoryRepository.findAll();
            if (!req.getSearch().isEmpty()){
                faCategories = faCategories.stream().filter(x -> x.getCode().equalsIgnoreCase(req.getSearch() )
                        || x.getKeterangan().contains(req.getSearch().toUpperCase())).collect(Collectors.toList());
            }else {
                String redisData = (String) redisTemplate.boundValueOps(KEY_FA_CATEGORY).get();
                if (Objects.isNull(redisData)){
                    faCategories = commonService.findFaCategory(faCategories);
                }else {
                    faCategories = mapper.readValue(redisData, new TypeReference<>() {});
                }
            }
            if (ObjectUtils.isEmpty(faCategories)){
                throw new NotFoundException("Data tidaK di temukan");
            }
            PagedListHolder paging = new PagedListHolder(faCategories);
            paging = commonService.setData(paging, req.getLimit(), req.getPage());
            response.setTotalPage(paging.getPageCount());
            response.setNumberOfElement(paging.getPageList().size());
            response.setPageNumber(faCategories.size());
            response.setTotalElement(paging.getNrOfElements());
            response.setFaCategories(paging.getPageList());
            return ResponseUtil.buildResponse(
                    SUCCESS,
                    "SUCCESS GET DATA",
                    response,
                    HttpStatus.OK);
        }catch (Exception e){
            throw e;
        }
    }

    public ResponseEntity<Object> saveDataFaCategory(CommonSaveDataDto req, Metadata metadata) {
        FaCategory faCategory = new FaCategory();
        try {
            if (ObjectUtils.isEmpty(req.getId())){
                log.info("SAVE DATA FA CATEGORY");
                faCategory.setCode(req.getKodeRek());
                faCategory.setKeterangan(req.getKeterangan());
                faCategory.setCreatedBy(metadata.getUsername());
                faCategory.setCreatedAt(new Date());
                faCategory.setDeleted(false);
            }else {
                log.info("UPDATE DATA FA CATEGORY");
                faCategory = faCategoryRepository.findById(req.getId()).get();
                faCategory.setKeterangan(req.getKeterangan());
                faCategory.setUpdatedBy(metadata.getUsername());
                faCategory.setUpdatedAt(new Date());
            }
            redisTemplate.delete(KEY_FA_CATEGORY);
            faCategoryRepository.save(faCategory);
            return ResponseUtil.buildResponse(
                    SUCCESS,
                    "DATA SAVED",
                    null,
                    HttpStatus.OK);
        }catch (Exception e){
            throw e;
        }
    }

    public ResponseEntity<Object> deleteDataFaCategory(CommonDeleteData req, Metadata metadata) {
        try {
            FaCategory faCategory = faCategoryRepository.findById(req.getId()).get();
            if (Objects.nonNull(faCategory)) {
                log.info("deleting data");
                faCategory.setUpdatedAt(new Date());
                faCategory.setUpdatedBy(metadata.getUsername());
                faCategory.setDeleted(true);
            }
            faCategoryRepository.save(faCategory);
            redisTemplate.delete(KEY_FA_CATEGORY);
            return ResponseUtil.buildResponse(
                    SUCCESS,
                    "DATA DELETED",
                    null,
                    HttpStatus.OK);
        } catch (Exception e) {
            throw e;
        }
    }

    public ResponseEntity<Object> getDataFaClass(CoaRequestDto req, Metadata metadata) throws JsonProcessingException {
        List<FaClass> faClass;
        FaClassFindResponseDto response = new FaClassFindResponseDto();
        try {
            faClass = faClassRepository.findAll();
            if (!req.getSearch().isEmpty()){
                faClass = faClass.stream().filter(x -> x.getCode().equalsIgnoreCase(req.getSearch() )
                        || x.getKeterangan().contains(req.getSearch().toUpperCase())).collect(Collectors.toList());
            }else {
                String redisData = (String) redisTemplate.boundValueOps(KEY_FA_CLASS).get();
                if (Objects.isNull(redisData)){
                    faClass = commonService.findFaClass(faClass);
                }else {
                    faClass = mapper.readValue(redisData, new TypeReference<>() {});
                }
            }
            if (ObjectUtils.isEmpty(faClass)){
                throw new NotFoundException("Data tidaK di temukan");
            }
            PagedListHolder paging = new PagedListHolder(faClass);
            paging = commonService.setData(paging, req.getLimit(), req.getPage());
            response.setTotalPage(paging.getPageCount());
            response.setNumberOfElement(paging.getPageList().size());
            response.setPageNumber(faClass.size());
            response.setTotalElement(paging.getNrOfElements());
            response.setFaClasses(paging.getPageList());
            return ResponseUtil.buildResponse(
                    SUCCESS,
                    "SUCCESS GET DATA",
                    response,
                    HttpStatus.OK);
        }catch (Exception e){
            throw e;
        }
    }

    public ResponseEntity<Object> saveDataFaClass(CommonSaveDataDto req, Metadata metadata) {
        FaClass faClass = new FaClass();
        try {
            if (ObjectUtils.isEmpty(req.getId())){
                log.info("SAVE DATA FA CATEGORY");
                faClass.setCode(req.getKodeRek());
                faClass.setKeterangan(req.getKeterangan());
                faClass.setCreatedBy(metadata.getUsername());
                faClass.setCreatedAt(new Date());
                faClass.setDeleted(false);
            }else {
                log.info("UPDATE DATA FA CATEGORY");
                faClass = faClassRepository.findById(Integer.parseInt(String.valueOf(req.getId()))).get();
                faClass.setKeterangan(req.getKeterangan());
                faClass.setUpdatedBy(metadata.getUsername());
                faClass.setUpdatedAt(new Date());
            }
            redisTemplate.delete(KEY_FA_CLASS);
            faClassRepository.save(faClass);
            return ResponseUtil.buildResponse(
                    SUCCESS,
                    "DATA SAVED",
                    null,
                    HttpStatus.OK);
        }catch (Exception e){
            throw e;
        }
    }

    public ResponseEntity<Object> deleteFaClass(CommonDeleteData req, Metadata metadata) {
        try {
            FaClass faClass = faClassRepository.findById(Integer.parseInt(String.valueOf(req.getId()))).get();
            if (Objects.nonNull(faClass)) {
                log.info("deleting data");
                faClass.setUpdatedAt(new Date());
                faClass.setUpdatedBy(metadata.getUsername());
                faClass.setDeleted(true);
            }
            faClassRepository.save(faClass);
            redisTemplate.delete(KEY_FA_CLASS);
            return ResponseUtil.buildResponse(
                    SUCCESS,
                    "DATA DELETED",
                    null,
                    HttpStatus.OK);
        } catch (Exception e) {
            throw e;
        }
    }

    public ResponseEntity<Object> getDataFaSubClass(CoaRequestDto req, Metadata metadata) throws JsonProcessingException {
        List<FaSubClass> faSubClasses;
        FaSubClassFindResponseDto response = new FaSubClassFindResponseDto();
        try {
            faSubClasses = faSubClassRepository.findAll();
            if (!req.getSearch().isEmpty()){
                faSubClasses = faSubClasses.stream().filter(x -> x.getCode().equalsIgnoreCase(req.getSearch() )
                        || x.getKeterangan().contains(req.getSearch().toUpperCase())).collect(Collectors.toList());
            }else {
                String redisData = (String) redisTemplate.boundValueOps(KEY_FA_SUB_CLASS).get();
                if (Objects.isNull(redisData)){
                    faSubClasses = commonService.findFaSubClass(faSubClasses);
                }else {
                    faSubClasses = mapper.readValue(redisData, new TypeReference<>() {});
                }
            }
            if (ObjectUtils.isEmpty(faSubClasses)){
                throw new NotFoundException("Data tidaK di temukan");
            }
            PagedListHolder paging = new PagedListHolder(faSubClasses);
            paging = commonService.setData(paging, req.getLimit(), req.getPage());
            response.setTotalPage(paging.getPageCount());
            response.setNumberOfElement(paging.getPageList().size());
            response.setPageNumber(faSubClasses.size());
            response.setTotalElement(paging.getNrOfElements());
            response.setFaSubClasses(paging.getPageList());
            return ResponseUtil.buildResponse(
                    SUCCESS,
                    "SUCCESS GET DATA",
                    response,
                    HttpStatus.OK);
        }catch (Exception e){
            throw e;
        }
    }

    public ResponseEntity<Object> saveDataFaSubClass(CommonSaveDataDto req, Metadata metadata) {
        FaSubClass faSubClass = new FaSubClass();
        try {
            if (ObjectUtils.isEmpty(req.getId())){
                log.info("SAVE DATA FA CATEGORY");
                faSubClass.setCode(req.getKodeRek());
                faSubClass.setKeterangan(req.getKeterangan());
                faSubClass.setCreatedBy(metadata.getUsername());
                faSubClass.setCreatedAt(new Date());
                faSubClass.setDeleted(false);
            }else {
                log.info("UPDATE DATA FA CATEGORY");
                faSubClass = faSubClassRepository.findById(Integer.parseInt(String.valueOf(req.getId()))).get();
                faSubClass.setKeterangan(req.getKeterangan());
                faSubClass.setUpdatedBy(metadata.getUsername());
                faSubClass.setUpdatedAt(new Date());
            }
            redisTemplate.delete(KEY_FA_SUB_CLASS);
            faSubClassRepository.save(faSubClass);
            return ResponseUtil.buildResponse(
                    SUCCESS,
                    "DATA SAVED",
                    null,
                    HttpStatus.OK);
        }catch (Exception e){
            throw e;
        }
    }

    public ResponseEntity<Object> deleteFaSubClass(CommonDeleteData req, Metadata metadata) {
        try {
            FaSubClass FaClassFindResponseDto = faSubClassRepository.findById(Integer.parseInt(String.valueOf(req.getId()))).get();
            if (Objects.nonNull(FaClassFindResponseDto)) {
                log.info("deleting data");
                FaClassFindResponseDto.setUpdatedAt(new Date());
                FaClassFindResponseDto.setUpdatedBy(metadata.getUsername());
                FaClassFindResponseDto.setDeleted(true);
            }
            faSubClassRepository.save(FaClassFindResponseDto);
            redisTemplate.delete(KEY_FA_CLASS);
            return ResponseUtil.buildResponse(
                    SUCCESS,
                    "DATA DELETED",
                    null,
                    HttpStatus.OK);
        } catch (Exception e) {
            throw e;
        }
    }

    /**
     * GET PRODUCT
     * @param request
     * @param metadata
     * @return
     * @throws JsonProcessingException
     */
    //#region GET PRODUCT
    public ResponseEntity<Object> getProduct(CommonRequestDto request, Metadata metadata) throws JsonProcessingException {
        List<Product> products = new ArrayList<>();
        ProductResponseDto response = new ProductResponseDto();
        try {
            if (!ObjectUtils.isEmpty(request.getSearch()))
                products = productRepository.findProduct(request.getSearch());
            else {
                String data = (String) redisTemplate.boundValueOps(KEY_PRODUCT).get();
                if (ObjectUtils.isEmpty(data) || data.equalsIgnoreCase("[]")) {
                    products = commonService.findAllProduct();
                } else {
                    products = mapper.readValue(data, new TypeReference<List<Product>>() {
                    });
                }
            }
            PagedListHolder paging = new PagedListHolder(products);
            paging = commonService.setData(paging, request.getSize(), request.getPage());
            response.setTotalPage(paging.getPageCount());
            response.setNumberOfElement(paging.getPageList().size());
            response.setPageNumber(paging.getPage());
            response.setTotalElement(paging.getNrOfElements());
            response.setProducts(paging.getPageList());
            return ResponseUtil.buildResponse(
                    SUCCESS,
                    INQUIRY_SUCCESS,
                    response,
                    HttpStatus.OK);
        } catch (Exception e) {
            return ResponseUtil.buildResponse(
                    FAILED,
                    INQUIRY_FAILED,
                    null,
                    HttpStatus.OK);
        }
    }
    //#endregion
    /**
     * SAVE PRODUCT
     * @param requestDto
     * @param metadata
     * @return
     */
    //#region SAVE PRODUCT
    public ResponseEntity<Object> saveProduct(ProductRequestDto requestDto,Metadata metadata){
        String redisKey = KEY_PRODUCT;
        String respKey = SAVE_SUCCESS;
        try {
            Product product = new Product();
            if (!ObjectUtils.isEmpty(requestDto.getId())){
                product = productRepository.findById(requestDto.getId()).get();
                product.setId(product.getId());
                product.setUpdatedAt(new Date());
                product.setUpdatedBy(metadata.getUsername());
            }else{
                product.setCreatedAt(new Date());
                product.setCreatedBy(metadata.getUsername());
            }
            product.setProductName(requestDto.getProductName());
            product.setCategory(requestDto.getCategory());
            if (ObjectUtils.isEmpty(groupProdRepository.findByGroupProd(requestDto.getGroupProd()))){
                throw new NotFoundException("Data not found in group prod :"+requestDto.getGroupProd());
            }
            groupProdRepository.findByGroupProd(requestDto.getGroupProd());
            product.setGroupProd(groupProdRepository.findByGroupProd(requestDto.getGroupProd()));
            if (ObjectUtils.isEmpty(typeProdRepository.findByTypeId(requestDto.getType()))){
                throw new NotFoundException("Data not found in group prod :"+requestDto.getGroupProd());
            }
            product.setType(typeProdRepository.findByTypeId(requestDto.getType()));
            if (ObjectUtils.isEmpty(uomRepository.findByKeterangan(requestDto.getUom()))){
                throw new NotFoundException("Data not found in group prod :"+requestDto.getGroupProd());
            }
            product.setUom(uomRepository.findByKeterangan(requestDto.getUom()));
            if (ObjectUtils.isEmpty(supplierRepository.findBySupplierId(requestDto.getSupplier()))){
                throw new NotFoundException("Data not found in group prod :"+requestDto.getGroupProd());
            }
            product.setSupplier(supplierRepository.findBySupplierId(requestDto.getSupplier()));
            productRepository.save(product);
            redisTemplate.delete(redisKey);
            return  ResponseUtil.buildResponse(
                    SUCCESS,
                    respKey,
                    null,
                    HttpStatus.OK);
        }catch (Exception e){
            return  ResponseUtil.buildResponse(
                    FAILED,
                    SAVE_FAILED,
                    null,
                    HttpStatus.BAD_GATEWAY);
        }
    }
    //#endregion
    /**
     * DELETE PRODUCT
     * @param req
     * @param metadata
     * @return
     */
    //#region delete product
    public ResponseEntity<Object> deleteProduct(ProductRequestDto req, Metadata metadata) {
        try {
            Product product = productRepository.findById(req.getId()).get();
            if (Objects.nonNull(product)) {
                log.info("deleting data");
                product.setUpdatedAt(new Date());
                product.setUpdatedBy(metadata.getUsername());
                product.setIsDeleted(true);
            }
            productRepository.save(product);
            redisTemplate.delete(KEY_PRODUCT);
            return ResponseUtil.buildResponse(
                    SUCCESS,
                    DELETE_SUCCESS,
                    null,
                    HttpStatus.OK);
        } catch (Exception e) {
            return ResponseUtil.buildResponse(
                    FAILED,
                    DELETE_FAILED,
                    null,
                    HttpStatus.OK);
        }
    }
    //#endregion
    /**
     * GET EMPLOYEE
     * @param request
     * @param metadata
     * @return
     * @throws JsonProcessingException
     */
    //#region GET EMPLOYEE
    public ResponseEntity<Object> getEmployee(CommonRequestDto request, Metadata metadata) throws JsonProcessingException {
        List<Karyawan> karyawans = new ArrayList<>();
        ProductResponseDto response = new ProductResponseDto();
        try {
            if (!ObjectUtils.isEmpty(request.getSearch()))
                karyawans = karyawanRepository.findKaryawan(request.getSearch());
            else {
                String data = (String) redisTemplate.boundValueOps(KEY_EMPLOYEE).get();
                if (ObjectUtils.isEmpty(data) || data.equalsIgnoreCase("[]")) {
                    karyawans = commonService.findAllKaryawan();
                } else {
                    karyawans = mapper.readValue(data, new TypeReference<List<Karyawan>>() {
                    });
                }
            }
            PagedListHolder paging = new PagedListHolder(karyawans);
            paging = commonService.setData(paging, request.getSize(), request.getPage());
            response.setTotalPage(paging.getPageCount());
            response.setNumberOfElement(paging.getPageList().size());
            response.setPageNumber(paging.getPage());
            response.setTotalElement(paging.getNrOfElements());
            response.setProducts(paging.getPageList());
            return ResponseUtil.buildResponse(
                    SUCCESS,
                    INQUIRY_SUCCESS,
                    response,
                    HttpStatus.OK);
        } catch (Exception e) {
            return ResponseUtil.buildResponse(
                    FAILED,
                    INQUIRY_FAILED,
                    null,
                    HttpStatus.OK);
        }
    }
    //#endregion
    /**
     * GET DIVISI
     * @param request
     * @param metadata
     * @return
     * @throws JsonProcessingException
     */
    //#region GET DIVISI
    public ResponseEntity<Object> getDivisi(CommonRequestDto request, Metadata metadata) throws JsonProcessingException {
        List<Divisi> divisis = new ArrayList<>();
        DivisiResponseDto response = new DivisiResponseDto();
        try {
            if (!ObjectUtils.isEmpty(request.getSearch()))
                divisis = divisiRepository.findDivisi(request.getSearch());
            else {
                String data = (String) redisTemplate.boundValueOps(KEY_DIVISI).get();
                if (ObjectUtils.isEmpty(data) || data.equalsIgnoreCase("[]")) {
                    divisis = commonService.findAllDivisi();
                } else {
                    divisis = mapper.readValue(data, new TypeReference<List<Divisi>>() {
                    });
                }
            }
            PagedListHolder paging = new PagedListHolder(divisis);
            paging = commonService.setData(paging, request.getSize(), request.getPage());
            response.setTotalPage(paging.getPageCount());
            response.setNumberOfElement(paging.getPageList().size());
            response.setPageNumber(paging.getPage());
            response.setTotalElement(paging.getNrOfElements());
            response.setDivisis(paging.getPageList());
            return ResponseUtil.buildResponse(
                    SUCCESS,
                    INQUIRY_SUCCESS,
                    response,
                    HttpStatus.OK);
        } catch (Exception e) {
            return ResponseUtil.buildResponse(
                    FAILED,
                    INQUIRY_FAILED,
                    null,
                    HttpStatus.OK);
        }
    }
    //#endregion
    //#endregion
    /**
     * GET DIVISI
     * @param request
     * @param metadata
     * @return
     * @throws JsonProcessingException
     */
    //#region GET DIVISI
    public ResponseEntity<Object> getGolongan(CommonRequestDto request, Metadata metadata) throws JsonProcessingException {
        List<Golongan> golongans = new ArrayList<>();
        GolonganResponseDto response = new GolonganResponseDto();
        try {
            if (!ObjectUtils.isEmpty(request.getSearch()))
                golongans = golonganRepository.findGolongan(request.getSearch());
            else {
                String data = (String) redisTemplate.boundValueOps(KEY_GOLONGAN).get();
                if (ObjectUtils.isEmpty(data) || data.equalsIgnoreCase("[]")) {
                    golongans = commonService.findAllGolongan();
                } else {
                    golongans = mapper.readValue(data, new TypeReference<List<Golongan>>() {
                    });
                }
            }
            PagedListHolder paging = new PagedListHolder(golongans);
            paging = commonService.setData(paging, request.getSize(), request.getPage());
            response.setTotalPage(paging.getPageCount());
            response.setNumberOfElement(paging.getPageList().size());
            response.setPageNumber(paging.getPage());
            response.setTotalElement(paging.getNrOfElements());
            response.setGolongans(paging.getPageList());
            return ResponseUtil.buildResponse(
                    SUCCESS,
                    INQUIRY_SUCCESS,
                    response,
                    HttpStatus.OK);
        } catch (Exception e) {
            return ResponseUtil.buildResponse(
                    FAILED,
                    INQUIRY_FAILED,
                    null,
                    HttpStatus.OK);
        }
    }
    //#endregion
    /**
     * GET JABATAN
     * @param request
     * @param metadata
     * @return
     * @throws JsonProcessingException
     */
    //#region GET JABATAN
    public ResponseEntity<Object> getJabatan(CommonRequestDto request, Metadata metadata) throws JsonProcessingException {
        List<Jabatan> jabatans = new ArrayList<>();
        JabatanResponseDto response = new JabatanResponseDto();
        try {
            if (!ObjectUtils.isEmpty(request.getSearch()))
                jabatans = jabatanRepository.findJabatan(request.getSearch());
            else {
                String data = (String) redisTemplate.boundValueOps(KEY_JABATAN).get();
                if (ObjectUtils.isEmpty(data) || data.equalsIgnoreCase("[]")) {
                    jabatans = commonService.findAllJabatan();
                } else {
                    jabatans = mapper.readValue(data, new TypeReference<List<Jabatan>>() {
                    });
                }
            }
            PagedListHolder paging = new PagedListHolder(jabatans);
            paging = commonService.setData(paging, request.getSize(), request.getPage());
            response.setTotalPage(paging.getPageCount());
            response.setNumberOfElement(paging.getPageList().size());
            response.setPageNumber(paging.getPage());
            response.setTotalElement(paging.getNrOfElements());
            response.setJabatans(paging.getPageList());
            return ResponseUtil.buildResponse(
                    SUCCESS,
                    INQUIRY_SUCCESS,
                    response,
                    HttpStatus.OK);
        } catch (Exception e) {
            return ResponseUtil.buildResponse(
                    FAILED,
                    INQUIRY_FAILED,
                    null,
                    HttpStatus.OK);
        }
    }
    //#endregion
    /**
     * GET STATUS KARYAWAN
     * @param request
     * @param metadata
     * @return
     * @throws JsonProcessingException
     */
    //#region GET STATUS KARYAWAN
    public ResponseEntity<Object> getStatusKaryawan(CommonRequestDto request, Metadata metadata) throws JsonProcessingException {
        List<StatusKaryawan> statusKaryawans = new ArrayList<>();
        StatusKaryawanResponseDto response = new StatusKaryawanResponseDto();
        try {
            if (!ObjectUtils.isEmpty(request.getSearch()))
                statusKaryawans = statusKaryawanRepository.findStatusKaryawan(request.getSearch());
            else {
                String data = (String) redisTemplate.boundValueOps(KEY_STATUS_KARYAWAN).get();
                if (ObjectUtils.isEmpty(data) || data.equalsIgnoreCase("[]")) {
                    statusKaryawans = commonService.findAllStatusKaryawan();
                } else {
                    statusKaryawans = mapper.readValue(data, new TypeReference<List<StatusKaryawan>>() {
                    });
                }
            }
            PagedListHolder paging = new PagedListHolder(statusKaryawans);
            paging = commonService.setData(paging, request.getSize(), request.getPage());
            response.setTotalPage(paging.getPageCount());
            response.setNumberOfElement(paging.getPageList().size());
            response.setPageNumber(paging.getPage());
            response.setTotalElement(paging.getNrOfElements());
            response.setStatusKaryawans(paging.getPageList());
            return ResponseUtil.buildResponse(
                    SUCCESS,
                    INQUIRY_SUCCESS,
                    response,
                    HttpStatus.OK);
        } catch (Exception e) {
            return ResponseUtil.buildResponse(
                    FAILED,
                    INQUIRY_FAILED,
                    null,
                    HttpStatus.OK);
        }
    }
    //#endregion
    /**
     * GET GAJI
     * @param request
     * @param metadata
     * @return
     * @throws JsonProcessingException
     */
    //#region GET GAJI
    public ResponseEntity<Object> getGaji(GajiRequestDto request, Metadata metadata) throws JsonProcessingException {
        List<GajiKaryawan> gajiKaryawans = new ArrayList<>();
        GajiResponseDto response = new GajiResponseDto();
        try {
            if (!ObjectUtils.isEmpty(request.getCabangCode()) || !ObjectUtils.isEmpty(request.getDivisiCode()) || !ObjectUtils.isEmpty(request.getSearch()) )
                gajiKaryawans = gajiKaryawanRepository.findGajiKaryawan(request.getCabangCode(), request.getDivisiCode(),request.getSearch());
            else {
                String data = (String) redisTemplate.boundValueOps(KEY_GAJI).get();
                if (ObjectUtils.isEmpty(data) || data.equalsIgnoreCase("[]")) {
                    gajiKaryawans = commonService.findAllGajiKaryawan();
                } else {
                    gajiKaryawans = mapper.readValue(data, new TypeReference<List<GajiKaryawan>>() {
                    });
                }
            }
            PagedListHolder paging = new PagedListHolder(gajiKaryawans);
            paging = commonService.setData(paging, request.getSize(), request.getPage());
            response.setTotalPage(paging.getPageCount());
            response.setNumberOfElement(paging.getPageList().size());
            response.setPageNumber(paging.getPage());
            response.setTotalElement(paging.getNrOfElements());
            response.setGajiKaryawans(paging.getPageList());
            return ResponseUtil.buildResponse(
                    SUCCESS,
                    INQUIRY_SUCCESS,
                    response,
                    HttpStatus.OK);
        } catch (Exception e) {
            return ResponseUtil.buildResponse(
                    FAILED,
                    INQUIRY_FAILED,
                    null,
                    HttpStatus.OK);
        }
    }
    //#endregion
    /**
     * GET PERJALANAN DINAS
     * @param request
     * @param metadata
     * @return
     * @throws JsonProcessingException
     */
    //#region GET PERJALANAN DINAS
    public ResponseEntity<Object> getPerjalananDinas(CommonRequestDto request, Metadata metadata) throws JsonProcessingException {
        List<PerjalananDinas> perjalananDinas = new ArrayList<>();
        PerjalananDinasResponseDto response = new PerjalananDinasResponseDto();
        try {
            if (!ObjectUtils.isEmpty(request.getCodeSearch()) || !ObjectUtils.isEmpty(request.getSearch()) ) {
                perjalananDinas = perjalananDinasRepository.findPerjalananDinas(request.getCodeSearch());
                perjalananDinas = perjalananDinas.stream().filter(x -> ObjectUtils.isEmpty(request.getSearch()) || x.getJabatan().getKeterangan().contains(request.getSearch())).collect(Collectors.toList());
            }else {
                String data = (String) redisTemplate.boundValueOps(KEY_SPPD).get();
                if (ObjectUtils.isEmpty(data) || data.equalsIgnoreCase("[]")) {
                    perjalananDinas = commonService.findAllPerjalananDinas();
                } else {
                    perjalananDinas = mapper.readValue(data, new TypeReference<List<PerjalananDinas>>() {
                    });
                }
            }
            PagedListHolder paging = new PagedListHolder(perjalananDinas);
            paging = commonService.setData(paging, request.getSize(), request.getPage());
            response.setTotalPage(paging.getPageCount());
            response.setNumberOfElement(paging.getPageList().size());
            response.setPageNumber(paging.getPage());
            response.setTotalElement(paging.getNrOfElements());
            response.setPerjalananDinas(paging.getPageList());
            return ResponseUtil.buildResponse(
                    SUCCESS,
                    INQUIRY_SUCCESS,
                    response,
                    HttpStatus.OK);
        } catch (Exception e) {
            return ResponseUtil.buildResponse(
                    FAILED,
                    INQUIRY_FAILED,
                    null,
                    HttpStatus.OK);
        }
    }
    //#endregion
    /**
     * GET PERJALANAN DINAS
     * @param request
     * @param metadata
     * @return
     * @throws JsonProcessingException
     */
    //#region GET PERJALANAN DINAS
    public ResponseEntity<Object> getPerjalananDinasLocal(CommonRequestDto request, Metadata metadata) throws JsonProcessingException {
        List<PerjalananDinasLocal> perjalananDinasLocals = new ArrayList<>();
        PerjalananDinasLocalResponseDto response = new PerjalananDinasLocalResponseDto();
        try {
            if (!ObjectUtils.isEmpty(request.getCodeSearch()) || !ObjectUtils.isEmpty(request.getSearch()) )
                perjalananDinasLocals = perjalananDinasLocalRepository.findPerjalananDinasLocal(request.getCodeSearch(),request.getSearch());
            else {
                String data = (String) redisTemplate.boundValueOps(KEY_SPPD_LOCAL).get();
                if (ObjectUtils.isEmpty(data) || data.equalsIgnoreCase("[]")) {
                    perjalananDinasLocals = commonService.findAllPerjalananDinasLocal();
                } else {
                    perjalananDinasLocals = mapper.readValue(data, new TypeReference<List<PerjalananDinasLocal>>() {
                    });
                }
            }
            PagedListHolder paging = new PagedListHolder(perjalananDinasLocals);
            paging = commonService.setData(paging, request.getSize(), request.getPage());
            response.setTotalPage(paging.getPageCount());
            response.setNumberOfElement(paging.getPageList().size());
            response.setPageNumber(paging.getPage());
            response.setTotalElement(paging.getNrOfElements());
            response.setPerjalananDinasLocals(paging.getPageList());
            return ResponseUtil.buildResponse(
                    SUCCESS,
                    INQUIRY_SUCCESS,
                    response,
                    HttpStatus.OK);
        } catch (Exception e) {
            return ResponseUtil.buildResponse(
                    FAILED,
                    INQUIRY_FAILED,
                    null,
                    HttpStatus.OK);
        }
    }
    //#endregion
}