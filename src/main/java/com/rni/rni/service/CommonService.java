package com.rni.rni.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rni.rni.domain.dao.*;
import com.rni.rni.repository.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.rni.rni.constant.Constant.chanelKey.CN_TRAN_HC_OMZET;
import static com.rni.rni.constant.Constant.chanelKey.CN_TRAN_HC_OMZET_MASTER;
import static com.rni.rni.constant.Constant.redisKey.*;

@Service
@Slf4j
public class CommonService {
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private ObjectMapper mapper;

    @Autowired
    private CoaHeaderRepository coaHeaderRepository;

    @Autowired
    private UserLoginRepository userLoginRepository;
    @Autowired
    private DimensionRepository dimensionRepository;
    @Autowired
    private DimensionValueRepository dimensionValueRepository;
    @Autowired
    private UomRepository uomRepository;
    @Autowired
    private PermissionRepository permissionRepository;

    @Autowired
    private FixedAssetRepository fixedAssetRepository;
    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private KaryawanRepository karyawanRepository;
    @Autowired
    private DivisiRepository divisiRepository;
    @Autowired
    private JabatanRepository jabatanRepository;
    @Autowired
    private GolonganRepository golonganRepository;

    @Autowired
    private GajiKaryawanRepository gajiKaryawanRepository;

    @Autowired
    private StatusKaryawanRepository statusKaryawanRepository;
    @Autowired
    private FaCategoryRepository faCategoryRepository;
    @Autowired
    private PerjalananDinasRepository perjalananDinasRepository;
    @Autowired
    private PerjalananDinasLocalRepository perjalananDinasLocalRepository;
    @Autowired
    private TbPjBerasRepository pjBerasRepository;
    @Autowired
    private Ap9Repository ap9Repository;
    @Autowired
    private TransactionHcOmzetRepository transactionHcOmzetRepository;

    @Autowired
    private MasterHcOmzetRepository masterHcOmzetRepository;

    public void storeToRedis(String redisKey,String object){
        redisTemplate.boundValueOps(redisKey).set(object);
    }
    public PagedListHolder setData(PagedListHolder paging,Integer limit,Integer page){
        paging.setPageSize(limit);
        paging.setPage(page);
        return paging;
    }
    public List<CoaHeader> findAllCoa() throws JsonProcessingException {
        redisTemplate.delete(KEY_COA);
        List<CoaHeader> res = coaHeaderRepository.findAll();
        String dataToRedis = mapper.writeValueAsString(res);
        this.storeToRedis(KEY_COA,dataToRedis);
        return res;
    }
    public List<UserLogin> findAllUserLogin() throws JsonProcessingException {
        redisTemplate.delete(KEY_USERLOGIN);
        List<UserLogin> res = userLoginRepository.findAll();
        String dataToRedis = mapper.writeValueAsString(res);
        this.storeToRedis(KEY_USERLOGIN,dataToRedis);
        return res;
    }
    public List<Dimension> findAllCabang() throws JsonProcessingException {
        String redisKey = KEY_CABANG;
        redisTemplate.delete(redisKey);
        List<Dimension> res = dimensionRepository.findAll();
        String dataToRedis = mapper.writeValueAsString(res);
        this.storeToRedis(redisKey,dataToRedis);
        return res;
    }
    public List<DimensionValue> findAllUnit() throws JsonProcessingException {
        String redisKey = KEY_UNIT;
        redisTemplate.delete(redisKey);
        List<DimensionValue> res = dimensionValueRepository.findAll();
        String dataToRedis = mapper.writeValueAsString(res);
        this.storeToRedis(redisKey,dataToRedis);
        return res;
    }
    public List<Uom> findAllUom() throws JsonProcessingException {
        String redisKey = KEY_UOM;
        redisTemplate.delete(redisKey);
        List<Uom> res = uomRepository.findAll();
        String dataToRedis = mapper.writeValueAsString(res);
        this.storeToRedis(redisKey,dataToRedis);
        return res;
    }

    public List<Permission> findAllPermission() throws JsonProcessingException {
        String redisKey = KEY_PERMISSION;
        redisTemplate.delete(redisKey);
        List<Permission> res = permissionRepository.findAll();
        String dataToRedis = mapper.writeValueAsString(res);
        this.storeToRedis(redisKey,dataToRedis);
        return res;
    }

    public List<FixedAsset> findAsset() throws JsonProcessingException {
        redisTemplate.delete(KEY_ASSET);
        List<FixedAsset> assets = fixedAssetRepository.findAll();
        String dataToRedis = mapper.writeValueAsString(assets);
        this.storeToRedis(KEY_ASSET, dataToRedis);
        return assets;
    }
    public List<Product> findAllProduct() throws JsonProcessingException {
        redisTemplate.delete(KEY_PRODUCT);
        List<Product> assets = productRepository.findAll();
        String dataToRedis = mapper.writeValueAsString(assets);
        this.storeToRedis(KEY_PRODUCT, dataToRedis);
        return assets;
    }
    public List<Karyawan> findAllKaryawan() throws JsonProcessingException {
        redisTemplate.delete(KEY_EMPLOYEE);
        List<Karyawan> assets = karyawanRepository.findKaryawan();
        String dataToRedis = mapper.writeValueAsString(assets);
        this.storeToRedis(KEY_EMPLOYEE, dataToRedis);
        return assets;
    }
    public List<Divisi> findAllDivisi() throws JsonProcessingException {
        redisTemplate.delete(KEY_DIVISI);
        List<Divisi> assets = divisiRepository.findAll();
        String dataToRedis = mapper.writeValueAsString(assets);
        this.storeToRedis(KEY_DIVISI, dataToRedis);
        return assets;
    }
    public List<Jabatan> findAllJabatan() throws JsonProcessingException {
        redisTemplate.delete(KEY_JABATAN);
        List<Jabatan> assets = jabatanRepository.findAll();
        String dataToRedis = mapper.writeValueAsString(assets);
        this.storeToRedis(KEY_JABATAN, dataToRedis);
        return assets;
    }
    public List<Golongan> findAllGolongan() throws JsonProcessingException {
        redisTemplate.delete(KEY_GOLONGAN);
        List<Golongan> assets = golonganRepository.findAll();
        String dataToRedis = mapper.writeValueAsString(assets);
        this.storeToRedis(KEY_GOLONGAN, dataToRedis);
        return assets;
    }
    public List<GajiKaryawan> findAllGajiKaryawan() throws JsonProcessingException {
        redisTemplate.delete(KEY_GAJI);
        List<GajiKaryawan> assets = gajiKaryawanRepository.findAll();
        String dataToRedis = mapper.writeValueAsString(assets);
        this.storeToRedis(KEY_GAJI, dataToRedis);
        return assets;
    }
    public List<StatusKaryawan> findAllStatusKaryawan() throws JsonProcessingException {
        redisTemplate.delete(KEY_STATUS_KARYAWAN);
        List<StatusKaryawan> assets = statusKaryawanRepository.findAll();
        String dataToRedis = mapper.writeValueAsString(assets);
        this.storeToRedis(KEY_STATUS_KARYAWAN, dataToRedis);
        return assets;
    }
    public List<FaCategory> findFaCategory(List<FaCategory> faCategories) throws JsonProcessingException {
        String dataToRedis = mapper.writeValueAsString(faCategories);
        this.storeToRedis(KEY_FA_CATEGORY, dataToRedis);
        return faCategories;
    }

    public List<FaClass> findFaClass(List<FaClass> faClasses) throws JsonProcessingException {
        String dataToRedis = mapper.writeValueAsString(faClasses);
        this.storeToRedis(KEY_FA_CLASS, dataToRedis);
        return faClasses;
    }

    public List<FaSubClass> findFaSubClass(List<FaSubClass> faSubClasses) throws JsonProcessingException {
        String dataToRedis = mapper.writeValueAsString(faSubClasses);
        this.storeToRedis(KEY_FA_SUB_CLASS, dataToRedis);
        return faSubClasses;
    }
    public List<PerjalananDinas> findAllPerjalananDinas() throws JsonProcessingException {
        redisTemplate.delete(KEY_SPPD);
        List<PerjalananDinas> assets = perjalananDinasRepository.findAll();
        String dataToRedis = mapper.writeValueAsString(assets);
        this.storeToRedis(KEY_SPPD, dataToRedis);
        return assets;
    }
    public List<PerjalananDinasLocal> findAllPerjalananDinasLocal() throws JsonProcessingException {
        redisTemplate.delete(KEY_SPPD_LOCAL);
        List<PerjalananDinasLocal> assets = perjalananDinasLocalRepository.findAll();
        String dataToRedis = mapper.writeValueAsString(assets);
        this.storeToRedis(KEY_SPPD_LOCAL, dataToRedis);
        return assets;
    }
    public List<TbPjBeras> findBeras() throws JsonProcessingException {
        redisTemplate.delete(KEY_BERAS);
        List<TbPjBeras> tbPjBeras = pjBerasRepository.findAll();
        String dataToRedis = mapper.writeValueAsString(tbPjBeras);
        this.storeToRedis(KEY_BERAS, dataToRedis);
        return tbPjBeras;
    }

    public List<AP9> findAp9() throws JsonProcessingException {
        redisTemplate.delete(KEY_AP9);
        List<AP9> tbPjBeras = ap9Repository.findAll();
        String dataToRedis = mapper.writeValueAsString(tbPjBeras);
        this.storeToRedis(KEY_AP9, dataToRedis);
        return tbPjBeras;
    }

    public List<TransactionEntryHcOmzet> findOmzet() throws JsonProcessingException {
        redisTemplate.delete(CN_TRAN_HC_OMZET);
        List<TransactionEntryHcOmzet> tbPjBeras = transactionHcOmzetRepository.findAll();
        String dataToRedis = mapper.writeValueAsString(tbPjBeras);
        this.storeToRedis(CN_TRAN_HC_OMZET, dataToRedis);
        return tbPjBeras;
    }
    public List<MasterEntryHcOmzet> findOmzetHcMaster() throws JsonProcessingException {
        redisTemplate.delete(CN_TRAN_HC_OMZET_MASTER);
        List<MasterEntryHcOmzet> tableList = masterHcOmzetRepository.findAll();
        String dataToRedis = mapper.writeValueAsString(tableList);
        this.storeToRedis(CN_TRAN_HC_OMZET_MASTER, dataToRedis);
        return tableList;
    }
}
