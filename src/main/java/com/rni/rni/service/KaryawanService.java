package com.rni.rni.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rni.rni.config.RedisConfig;
import com.rni.rni.config.RedisPublishConfig;
import com.rni.rni.config.RedisSubscribeConfig;
import com.rni.rni.domain.dao.*;
import com.rni.rni.domain.dto.CommonDto;
import com.rni.rni.domain.dto.Metadata;
import com.rni.rni.repository.*;
import com.rni.rni.utils.ConvertionUtils;
import com.rni.rni.utils.GeneralUtils;
import com.rni.rni.utils.InsertLogMonitoring;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import util.ResponseUtil;

import javax.transaction.Transactional;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import static com.rni.rni.constant.Constant.responseKey.SUCCESS;
import static com.rni.rni.constant.Constant.responseKey.UPLOADING;


@Slf4j
@Service
public class KaryawanService {

    @Autowired
    private DivisiRepository divisiRepository;

    @Autowired
    private JabatanRepository jabatanRepository;

    @Autowired
    private GolonganRepository golonganRepository;

    @Autowired
    private StatusKaryawanRepository statusKaryawanRepository;

    @Autowired
    private GajiKaryawanRepository gajiKaryawanRepository;

    @Autowired
    private DimensionRepository dimensionRepository;

    @Autowired
    private InsertLogMonitoring insertLogMonitoring;

    @Autowired
    private DimensionValueRepository dimensionValueRepository;

    @Autowired
    private TableMasterSchedulerRepository tableMasterSchedulerRepository;

    @Autowired
    private StatusPernikahanRepository statusPernikahanRepository;

    @Autowired
    private KaryawanRepository karyawanRepository;
    
    @Autowired
    private TbParameterBerasRepository tbParameterBerasRepository;

    @Autowired
    private RedisPublishConfig redisPublishConfig;

    @Autowired
    private RedisSubscribeConfig redisSubscribeConfig;

    @Autowired
    private RedisConfig redisConfig;

    @Transactional
    public ResponseEntity<Object> divisiUpload(Workbook workbook, Sheet sheet, Long size, Metadata metadata) throws IOException {
        log.info("processing update divis");
        List<Divisi> divisiSave = new ArrayList<>();
        TableMasterScheduler tableMasterScheduler = new TableMasterScheduler();
        CommonDto commonDto = new CommonDto();
        try {
            Iterator<Row> rows = sheet.iterator();
            int rowNumber = 0;
            while (rows.hasNext()) {
                Divisi divisi = new Divisi();
                Row currentRow = rows.next();
                if (rowNumber == 0) {
                    rowNumber++;
                    continue;
                }
                Iterator<Cell> cellInRow = currentRow.iterator();
                int cellIdx = 0;
                while (cellInRow.hasNext()) {
                    Cell currentCell = cellInRow.next();
                    switch (cellIdx) {
                        case 0:
                            divisi.setCode(currentCell.getStringCellValue());
                            break;
                        case 1:
                            divisi.setKeterangan(currentCell.getStringCellValue());
                            break;
                        default:
                            break;
                    }
                    cellIdx++;
                }
                divisiSave.add(divisi);
            }
            workbook.close();
            divisiSave.stream().forEach(x -> {
                x.setCreatedBy(metadata.getUsername());
                x.setCreatedAt(new Date());
            });
            var logId = insertLogMonitoring.logMonitoring(null, sheet.getSheetName(), "Uploading", size, divisiSave.stream().count());
            commonDto.setDivisis(divisiSave);
            tableMasterScheduler.setTemplateName(sheet.getSheetName());
            tableMasterScheduler.setClassName(Divisi.class.getCanonicalName());
            tableMasterScheduler.setData(new ObjectMapper().writeValueAsString(commonDto));
            tableMasterScheduler.setIdLog(logId);
            tableMasterSchedulerRepository.save(tableMasterScheduler);
            return ResponseUtil.buildResponse(SUCCESS, UPLOADING, null, HttpStatus.OK);
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional
    public ResponseEntity<Object> jabatanUpload(Workbook workbook, Sheet sheet, Long size, Metadata metadata) throws IOException {
        log.info("processing update jabatan");
        List<Jabatan> jabatanSave = new ArrayList<>();
        TableMasterScheduler tableMasterScheduler = new TableMasterScheduler();
        CommonDto commonDto = new CommonDto();
        try {
            Iterator<Row> rows = sheet.iterator();
            int rowNumber = 0;
            while (rows.hasNext()) {
                Jabatan jabatan = new Jabatan();
                Row currentRow = rows.next();
                if (rowNumber == 0) {
                    rowNumber++;
                    continue;
                }
                Iterator<Cell> cellInRow = currentRow.iterator();
                int cellIdx = 0;
                while (cellInRow.hasNext()) {
                    Cell currentCell = cellInRow.next();
                    switch (cellIdx) {
                        case 0:
                            jabatan.setKeterangan(currentCell.getStringCellValue());
                            break;
                        case 1:
                            jabatan.setLevel(Integer.parseInt(currentCell.getStringCellValue()));
                            break;
                        default:
                            break;
                    }
                    cellIdx++;
                }
                jabatanSave.add(jabatan);
            }
            workbook.close();
            jabatanSave.stream().forEach(x -> {
                x.setCode(GeneralUtils.code("JAB").toUpperCase());
                x.setCreatedBy(metadata.getUsername());
                x.setCreatedAt(new Date());
            });
            var logId = insertLogMonitoring.logMonitoring(null, sheet.getSheetName(), "Uploading", size, jabatanSave.stream().count());
            commonDto.setJabatanList(jabatanSave);
            tableMasterScheduler.setTemplateName(sheet.getSheetName());
            tableMasterScheduler.setClassName(Jabatan.class.getCanonicalName());
            tableMasterScheduler.setData(new ObjectMapper().writeValueAsString(commonDto));
            tableMasterScheduler.setIdLog(logId);
            tableMasterSchedulerRepository.save(tableMasterScheduler);
            return ResponseUtil.buildResponse(SUCCESS, UPLOADING, null, HttpStatus.OK);
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional
    public ResponseEntity<Object> golonganUpload(Workbook workbook, Sheet sheet, Long size, Metadata metadata) throws IOException {
        log.info("Processing upload data golongan");
        List<Golongan> golonganSave = new ArrayList<>();
        TableMasterScheduler tableMasterScheduler = new TableMasterScheduler();
        CommonDto commonDto = new CommonDto();
        try {
            Iterator<Row> rows = sheet.iterator();
            int rowNumber = 0;
            while (rows.hasNext()) {
                Golongan golongan = new Golongan();
                Row currentRow = rows.next();
                if (rowNumber == 0) {
                    rowNumber++;
                    continue;
                }
                Iterator<Cell> cellInRow = currentRow.iterator();
                int cellIdx = 0;
                while (cellInRow.hasNext()) {
                    Cell currentCell = cellInRow.next();
                    switch (cellIdx) {
                        case 0:
                            golongan.setKeterangan(currentCell.getStringCellValue());
                            break;
                        default:
                            break;
                    }
                    cellIdx++;
                }
                golonganSave.add(golongan);
            }
            workbook.close();
            golonganSave.stream().forEach(x -> {
                x.setCode(GeneralUtils.code("GOL").toUpperCase());
                x.setCreatedBy(metadata.getUsername());
                x.setCreatedAt(new Date());
            });
            var logId = insertLogMonitoring.logMonitoring(null, sheet.getSheetName(), "Uploading", size, golonganSave.stream().count());
            commonDto.setGolongans(golonganSave);
            tableMasterScheduler.setTemplateName(sheet.getSheetName());
            tableMasterScheduler.setClassName(Golongan.class.getCanonicalName());
            tableMasterScheduler.setData(new ObjectMapper().writeValueAsString(commonDto));
            tableMasterScheduler.setIdLog(logId);
            tableMasterSchedulerRepository.save(tableMasterScheduler);
            return ResponseUtil.buildResponse(SUCCESS, UPLOADING, null, HttpStatus.OK);
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional
    public ResponseEntity<Object> statusKaryawanUpload(Workbook workbook, Sheet sheet, Long size, Metadata metadata) throws IOException {
        log.info("Processing upload data status karyawan");
        List<StatusKaryawan> statusKaryawan = new ArrayList<>();
        TableMasterScheduler tableMasterScheduler = new TableMasterScheduler();
        CommonDto commonDto = new CommonDto();
        try {
            Iterator<Row> rows = sheet.iterator();
            int rowNumber = 0;
            while (rows.hasNext()) {
                StatusKaryawan stsKaryawan = new StatusKaryawan();
                Row currentRow = rows.next();
                if (rowNumber == 0) {
                    rowNumber++;
                    continue;
                }
                Iterator<Cell> cellInRow = currentRow.iterator();
                int cellIdx = 0;
                while (cellInRow.hasNext()) {
                    Cell currentCell = cellInRow.next();
                    switch (cellIdx) {
                        case 0:
                            stsKaryawan.setKeterangan(currentCell.getStringCellValue());
                            break;
                        default:
                            break;
                    }
                    cellIdx++;
                }
                statusKaryawan.add(stsKaryawan);
            }
            workbook.close();
            statusKaryawan.stream().forEach(x -> {
                x.setCode(GeneralUtils.code("STK").toUpperCase());
                x.setCreatedBy(metadata.getUsername());
                x.setCreatedAt(new Date());
            });
            var logId = insertLogMonitoring.logMonitoring(null, sheet.getSheetName(), "Uploading", size, statusKaryawan.stream().count());
            commonDto.setStatusKaryawans(statusKaryawan);
            tableMasterScheduler.setTemplateName(sheet.getSheetName());
            tableMasterScheduler.setClassName(StatusKaryawan.class.getCanonicalName());
            tableMasterScheduler.setData(new ObjectMapper().writeValueAsString(commonDto));
            tableMasterScheduler.setIdLog(logId);
            tableMasterSchedulerRepository.save(tableMasterScheduler);
            return ResponseUtil.buildResponse(SUCCESS, UPLOADING, null, HttpStatus.OK);
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional
    public ResponseEntity<Object> gajiKaryawanUpload(Workbook workbook, Sheet sheet, Long size, Metadata metadata) throws IOException {
        log.info("Processing upload data gaji karyawan");
        List<GajiKaryawan> gajiKaryawanSave = new ArrayList<>();
        TableMasterScheduler tableMasterScheduler = new TableMasterScheduler();
        CommonDto commonDto = new CommonDto();
        try {
            Iterator<Row> rows = sheet.iterator();
            int rowNumber = 0;
            while (rows.hasNext()) {
                GajiKaryawan gajiKaryawan = new GajiKaryawan();
                Row currentRow = rows.next();
                if (rowNumber == 0) {
                    rowNumber++;
                    continue;
                }
                Iterator<Cell> cellInRow = currentRow.iterator();
                int cellIdx = 0;
                while (cellInRow.hasNext()) {
                    Cell currentCell = cellInRow.next();
                    switch (cellIdx) {
                        case 0:
                            gajiKaryawan.setCabangCode(dimensionRepository.findByCode(currentCell.getStringCellValue()));
                            break;
                        case 1:
                            gajiKaryawan.setDivisiCode(Divisi.builder().code(currentCell.getStringCellValue()).build());
                            break;
                        case 2:
                            gajiKaryawan.setGolonganCode(Golongan.builder().code(currentCell.getStringCellValue()).build());
                            break;
                        case 3:
                            gajiKaryawan.setNominal(currentCell.getStringCellValue());
                            break;
                        default:
                            break;
                    }
                    cellIdx++;
                }
                gajiKaryawanSave.add(gajiKaryawan);
            }
            workbook.close();
            gajiKaryawanSave.stream().forEach(x -> {
                x.setCreatedBy(metadata.getUsername());
                x.setCreatedAt(new Date());
            });
            var logId = insertLogMonitoring.logMonitoring(null, sheet.getSheetName(), "Uploading", size, gajiKaryawanSave.stream().count());
            commonDto.setGajiKaryawans(gajiKaryawanSave);
            tableMasterScheduler.setTemplateName(sheet.getSheetName());
            tableMasterScheduler.setClassName(GajiKaryawan.class.getCanonicalName());
            tableMasterScheduler.setData(new ObjectMapper().writeValueAsString(commonDto));
            tableMasterScheduler.setIdLog(logId);
            tableMasterSchedulerRepository.save(tableMasterScheduler);
            return ResponseUtil.buildResponse(SUCCESS, UPLOADING, null, HttpStatus.OK);
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional
    public ResponseEntity<Object> dimensionUpload(Workbook workbook, Sheet sheet, Long size, Metadata metadata) throws IOException {
        log.info("Processing upload data dimension");
        List<Dimension> dimensions = new ArrayList<>();
        TableMasterScheduler tableMasterScheduler = new TableMasterScheduler();
        CommonDto commonDto = new CommonDto();
        try {
            Iterator<Row> rows = sheet.iterator();
            int rowNumber = 0;
            while (rows.hasNext()) {
                Dimension dimension = new Dimension();
                Row currentRow = rows.next();
                if (rowNumber == 0) {
                    rowNumber++;
                    continue;
                }
                Iterator<Cell> cellInRow = currentRow.iterator();
                int cellIdx = 0;
                while (cellInRow.hasNext()) {
                    Cell currentCell = cellInRow.next();
                    switch (cellIdx) {
                        case 0:
                            dimension.setCode(currentCell.getStringCellValue());
                            break;
                        case 1:
                            dimension.setKeterangan(currentCell.getStringCellValue());
                            break;
                        default:
                            break;
                    }
                    cellIdx++;
                }
                dimensions.add(dimension);
            }
            workbook.close();
            dimensions.stream().forEach(x -> {
                x.setCreatedBy(metadata.getUsername());
                x.setCreatedAt(new Date());
                x.setBlocked(0);
            });
            var logId = insertLogMonitoring.logMonitoring(null, sheet.getSheetName(), "Uploading", size, dimensions.stream().count());
            commonDto.setDimensions(dimensions);
            tableMasterScheduler.setTemplateName(sheet.getSheetName());
            tableMasterScheduler.setClassName(Dimension.class.getCanonicalName());
            tableMasterScheduler.setData(new ObjectMapper().writeValueAsString(commonDto));
            tableMasterScheduler.setIdLog(logId);
            tableMasterSchedulerRepository.save(tableMasterScheduler);
            return ResponseUtil.buildResponse(SUCCESS, UPLOADING, null, HttpStatus.OK);
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional
    public ResponseEntity<Object> dimensionValueUpload(Workbook workbook, Sheet sheet, Long size, Metadata metadata) throws IOException {
        log.info("Processing upload data dimension value");
        List<DimensionValue> dimensionValues = new ArrayList<>();
        TableMasterScheduler tableMasterScheduler = new TableMasterScheduler();
        CommonDto commonDto = new CommonDto();
        try {
            Iterator<Row> rows = sheet.iterator();
            int rowNumber = 0;
            while (rows.hasNext()) {
                DimensionValue dimension = new DimensionValue();
                Row currentRow = rows.next();
                if (rowNumber == 0) {
                    rowNumber++;
                    continue;
                }
                Iterator<Cell> cellInRow = currentRow.iterator();
                int cellIdx = 0;
                while (cellInRow.hasNext()) {
                    Cell currentCell = cellInRow.next();
                    switch (cellIdx) {
                        case 0:
                            dimension.setDimensionCode(Dimension.builder()
                                    .code(currentCell.getStringCellValue()).build());
                            break;
                        case 1:
                            dimension.setNoCabang(currentCell.getStringCellValue());
                            break;
                        case 2:
                            dimension.setCode(currentCell.getStringCellValue());
                            break;
                        case 3:
                            dimension.setKeterangan(currentCell.getStringCellValue());
                            break;
                        default:
                            break;
                    }
                    cellIdx++;
                }
                dimensionValues.add(dimension);
            }
            workbook.close();
            dimensionValues.stream().forEach(x -> {
                x.setCreatedBy(metadata.getUsername());
                x.setCreatedAt(new Date());
                x.setBlocked(0);
            });
            var logId = insertLogMonitoring.logMonitoring(null, sheet.getSheetName(), "Uploading", size, dimensionValues.stream().count());
            commonDto.setDimensionValues(dimensionValues);
            tableMasterScheduler.setTemplateName(sheet.getSheetName());
            tableMasterScheduler.setClassName(DimensionValue.class.getCanonicalName());
            tableMasterScheduler.setData(new ObjectMapper().writeValueAsString(commonDto));
            tableMasterScheduler.setIdLog(logId);
            tableMasterSchedulerRepository.save(tableMasterScheduler);
            return ResponseUtil.buildResponse(SUCCESS, UPLOADING, null, HttpStatus.OK);
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional
    public ResponseEntity<Object> employee(Workbook workbook, Sheet sheet, long size, Metadata metadata) throws ParseException, IOException {
        log.info("Processing upload data dimension value");
        List<Karyawan> karyawans = new ArrayList<>();
        TableMasterScheduler tableMasterScheduler = new TableMasterScheduler();
        CommonDto commonDto = new CommonDto();
        try {
            DataFormatter formatter = new DataFormatter();
            Iterator<Row> rows = sheet.iterator();
            int rowNumber = 0;
            while (rows.hasNext()) {
                Karyawan karyawan = new Karyawan();
                Row currentRow = rows.next();
                if (rowNumber == 0) {
                    rowNumber++;
                    continue;
                }
                Iterator<Cell> cellInRow = currentRow.iterator();
                int cellIdx = 0;
                while (cellInRow.hasNext()) {
                    Cell currentCell = cellInRow.next();
                    String dataCol = formatter.formatCellValue(sheet.getRow(currentRow.getRowNum()).getCell(currentCell.getColumnIndex()));
                    switch (cellIdx) {
                        case 0:
                            karyawan.setNik(dataCol);
                            break;
                        case 1:
                            dataCol = dataCol.equals("-") ? null : dataCol;//ObjectUtils.isEmpty(dimensionRepository.findByCode(dataCol)) ? "" : dataCol;
                            karyawan.setCabang(dimensionRepository.findByCode(dataCol));
                            break;
                        case 2:
                            dataCol = dataCol.equals("-") ? null : dataCol;//ObjectUtils.isEmpty(dimensionValueRepository.findByCode(dataCol)) ? "" : dataCol;
                            karyawan.setDivisi(dataCol);
                            break;
                        case 3:
                            karyawan.setNama(dataCol.equals("-") ? null : dataCol);
                            break;
                        case 4:
//                            dataCol = ObjectUtils.isEmpty(jabatanRepository.findByCode(dataCol)) ? "" : dataCol;
                            karyawan.setJabatan(jabatanRepository.findByCode(dataCol));
                            karyawan.setLevel(ObjectUtils.isEmpty(dataCol) ? null : Integer.parseInt(dataCol.substring(dataCol.length() - 2)));
                            break;
                        case 5:
                            dataCol = dataCol.equals("-") ? null : dataCol;//ObjectUtils.isEmpty(statusKaryawanRepository.findByCode(dataCol)) ? "" : dataCol;
                            karyawan.setStatusKaryawan(dataCol);
                            break;
                        case 6:
                            dataCol = dataCol.equals("-") ? null : dataCol;//ObjectUtils.isEmpty(golonganRepository.findByCode(dataCol)) ? "" : dataCol;
                            karyawan.setGolongan(dataCol);
                            break;
                        case 7:
                            karyawan.setPendidikan(dataCol.equals("-") ? null : dataCol);
                            break;
                        case 8:
                            karyawan.setTanggalBekerja(dataCol.equals("-") ? null : dataCol.equals("") ? null : dataCol.equals(" ") ? null : ConvertionUtils.convertStringtoDate(dataCol));
                            break;
                        case 9:
                            karyawan.setTanggalLahir(dataCol.equals("-") ? null : dataCol.equals("") ? null : ConvertionUtils.convertStringtoDate(dataCol));
                            break;
                        case 10:
                            dataCol = dataCol.equals("-") ? null : dataCol;//ObjectUtils.isEmpty(statusPernikahanRepository.findByCode(dataCol)) ? "" : dataCol;
                            karyawan.setStatusKeluarga(dataCol);
                            break;
                        case 11:
                            karyawan.setTypeKaryawan(dataCol.equals("-") ? null : dataCol);
                            break;
                        case 12:
                            karyawan.setJurusan(dataCol.equals("-") ? null : dataCol.equals("") ? null : dataCol);
                            break;
                        case 13:
                            karyawan.setTahunLulus(dataCol.equals("-") ? null : dataCol);
                            break;
                        case 14:
                            karyawan.setTempatLahir(dataCol.equals("-") ? null : dataCol);
                            break;
                        case 15:
                            karyawan.setTanggalTetap(dataCol.equals("-") ? null : ConvertionUtils.convertStringtoDate(dataCol));
                            break;
                        case 16:
                            karyawan.setSekolah(dataCol.equals("-") ? null : dataCol);
                            break;
                        case 17:
                            karyawan.setStruktural(dataCol.equals("-") ? null : dataCol);
                            break;
                        case 18:
                            karyawan.setTempatMasuk(dataCol.equals("-") ? null : dataCol);
                            break;
                        default:
                            break;
                    }
                    cellIdx++;
                }
                karyawans.add(karyawan);
            }
            workbook.close();
            karyawans.stream().forEach(x -> {
                x.setCreatedBy(metadata.getUsername());
                x.setCreatedAt(new Date());
            });
            var logId = insertLogMonitoring.logMonitoring(null, sheet.getSheetName(), "Uploading", size, karyawans.stream().count());
            commonDto.setKaryawans(karyawans);
            tableMasterScheduler.setTemplateName(sheet.getSheetName());
            tableMasterScheduler.setClassName(Karyawan.class.getCanonicalName());
            tableMasterScheduler.setData(new ObjectMapper().writeValueAsString(commonDto));
            tableMasterScheduler.setIdLog(logId);
            tableMasterSchedulerRepository.save(tableMasterScheduler);
            return ResponseUtil.buildResponse(SUCCESS, UPLOADING, null, HttpStatus.OK);
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional
    public ResponseEntity<Object> struktural(Workbook workbook, Sheet sheet, long size, Metadata metadata) throws ParseException, IOException {
        log.info("Processing upload data dimension value");
        List<Struktural> strukturals = new ArrayList<>();
        TableMasterScheduler tableMasterScheduler = new TableMasterScheduler();
        CommonDto commonDto = new CommonDto();
        try {
            DataFormatter formatter = new DataFormatter();
            Iterator<Row> rows = sheet.iterator();
            int rowNumber = 0;
            while (rows.hasNext()) {
                Struktural struktural = new Struktural();
                Row currentRow = rows.next();
                if (rowNumber == 0) {
                    rowNumber++;
                    continue;
                }
                Iterator<Cell> cellInRow = currentRow.iterator();
                int cellIdx = 0;
                while (cellInRow.hasNext()) {
                    Cell currentCell = cellInRow.next();
                    String dataCol = formatter.formatCellValue(sheet.getRow(currentRow.getRowNum()).getCell(currentCell.getColumnIndex()));
                    switch (cellIdx) {
                        case 0:
                            struktural.setStrukturalName(dataCol);
                            break;
                    }
                    cellIdx++;
                }
                strukturals.add(struktural);
            }
            workbook.close();;
            strukturals.stream().forEach(x -> x.setCode(GeneralUtils.code("ST"))
            );
            var logId = insertLogMonitoring.logMonitoring(null, sheet.getSheetName(), "Uploading", size, strukturals.stream().count());
            commonDto.setStrukturals(strukturals);
            tableMasterScheduler.setTemplateName(sheet.getSheetName());
            tableMasterScheduler.setClassName(Karyawan.class.getCanonicalName());
            tableMasterScheduler.setData(new ObjectMapper().writeValueAsString(commonDto));
            tableMasterScheduler.setIdLog(logId);
            tableMasterSchedulerRepository.save(tableMasterScheduler);
            return ResponseUtil.buildResponse(SUCCESS, UPLOADING, null, HttpStatus.OK);
        } catch (Exception e) {
            throw e;
        }
    }
}
