package com.rni.rni.service;

import com.rni.rni.domain.dao.*;
import com.rni.rni.domain.dto.TRansactionOmzetSumDto;
import com.rni.rni.domain.dto.TransactionHCOmzetDto;
import com.rni.rni.domain.dto.TransactionOmzetDto;
import com.rni.rni.repository.TransactionHcOmzetRepository;
import com.rni.rni.utils.ConvertionUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import util.ResponseUtil;

import java.util.Objects;
import java.util.Optional;

import static com.rni.rni.constant.Constant.responseKey.*;
import static com.rni.rni.constant.Constant.responseKey.INQUIRY_FAILED;

@Service
@Slf4j
public class SummaryServiceOmzet {

    @Autowired
    private TransactionHcOmzetRepository transactionHcOmzetRepository;

    public TRansactionOmzetSumDto sumTotalTransactionEntryHc(TRansactionOmzetSumDto tRansactionOmzetSumDto, TransactionEntryHcOmzet x) {
        TRansactionOmzetSumDto res = new TRansactionOmzetSumDto();
        res.setJan(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getJan()) + Long.parseLong(Objects.equals(x.getJan(), "") ? "0" : x.getJan()));
        res.setFeb(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getFeb()) + Long.parseLong(Objects.equals(x.getFeb(), "") ? "0" : x.getFeb()));
        res.setMar(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getMar()) + Long.parseLong(Objects.equals(x.getMar(), "") ? "0" : x.getMar()));
        res.setApr(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getApr()) + Long.parseLong(Objects.equals(x.getApr(), "") ? "0" : x.getApr()));
        res.setMei(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getMei()) + Long.parseLong(Objects.equals(x.getMei(), "") ? "0" : x.getMei()));
        res.setJun(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getJun()) + Long.parseLong(Objects.equals(x.getJun(), "") ? "0" : x.getJun()));
        res.setJul(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getJul()) + Long.parseLong(Objects.equals(x.getJul(), "") ? "0" : x.getJul()));
        res.setAgt(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getAgt()) + Long.parseLong(Objects.equals(x.getAgt(), "") ? "0" : x.getAgt()));
        res.setSep(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getSep()) + Long.parseLong(Objects.equals(x.getSep(), "") ? "0" : x.getSep()));
        res.setOkt(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getOkt()) + Long.parseLong(Objects.equals(x.getOkt(), "") ? "0" : x.getOkt()));
        res.setNov(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getNov()) + Long.parseLong(Objects.equals(x.getNov(), "") ? "0" : x.getNov()));
        res.setDes(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getDes()) + Long.parseLong(Objects.equals(x.getDes(), "") ? "0" : x.getDes()));
        return res;
    }
    public TransactionHCOmzetDto sumTotal(TRansactionOmzetSumDto tRansactionOmzetSumDto, TransactionEntryHcOmzet x) {
        TransactionHCOmzetDto res = new TransactionHCOmzetDto();
        res.setId(x.getId());
        res.setProduct(x.getProduct());
        res.setCabangDivisi(x.getCabangDivisi());
        res.setPeriode(x.getPeriode());
        res.setYear(x.getYear());
        res.setKeterangan(x.getKeterangan());
        res.setGroupProd(x.getGroupProd());
        res.setRowNumber(x.getRowNumber());
        res.setJan(tRansactionOmzetSumDto.getJan());
        res.setFeb(tRansactionOmzetSumDto.getFeb());
        res.setMar(tRansactionOmzetSumDto.getMar());
        res.setTwi(res.getMar()+res.getJan()+res.getFeb());
        res.setApr(tRansactionOmzetSumDto.getApr());
        res.setMei(tRansactionOmzetSumDto.getMei());
        res.setJun(tRansactionOmzetSumDto.getJun());
        res.setTwii(res.getApr()+res.getMei()+res.getJun());
        res.setJul(tRansactionOmzetSumDto.getJul());
        res.setAgt(tRansactionOmzetSumDto.getAgt());
        res.setSep(tRansactionOmzetSumDto.getSep());
        res.setTwiii(res.getJul()+res.getAgt()+res.getSep());
        res.setOkt(tRansactionOmzetSumDto.getOkt());
        res.setNov(tRansactionOmzetSumDto.getNov());
        res.setDes(tRansactionOmzetSumDto.getDes());
        res.setTwiv(res.getOkt()+res.getNov()+res.getDes());
        res.setTtl(res.getTwi()+res.getTwii()+res.getTwiii()+res.getTwiv());
        return res;
    }
    public ResponseEntity<Object> updateRowHcOmzet(TransactionHCOmzetDto transactionHCOmzetDto) {

        Optional<TransactionEntryHcOmzet> transactionEntryHcOmzet = transactionHcOmzetRepository.findById(transactionHCOmzetDto.getId());
        if (transactionEntryHcOmzet.isPresent()) {
            TransactionEntryHcOmzet transactionEntryHcOmzet1 = transactionEntryHcOmzet.get();
            transactionEntryHcOmzet1.setJan(transactionHCOmzetDto.getJan().toString());
            transactionEntryHcOmzet1.setFeb(transactionHCOmzetDto.getFeb().toString());
            transactionEntryHcOmzet1.setMar(transactionHCOmzetDto.getMar().toString());
            transactionEntryHcOmzet1.setApr(transactionHCOmzetDto.getApr().toString());
            transactionEntryHcOmzet1.setMei(transactionHCOmzetDto.getMei().toString());
            transactionEntryHcOmzet1.setJun(transactionHCOmzetDto.getJun().toString());
            transactionEntryHcOmzet1.setJul(transactionHCOmzetDto.getJul().toString());
            transactionEntryHcOmzet1.setAgt(transactionHCOmzetDto.getAgt().toString());
            transactionEntryHcOmzet1.setSep(transactionHCOmzetDto.getSep().toString());
            transactionEntryHcOmzet1.setOkt(transactionHCOmzetDto.getOkt().toString());
            transactionEntryHcOmzet1.setNov(transactionHCOmzetDto.getNov().toString());
            transactionEntryHcOmzet1.setDes(transactionHCOmzetDto.getDes().toString());
            transactionHcOmzetRepository.save(transactionEntryHcOmzet1);
            return ResponseUtil.buildResponse(
                    SUCCESS,
                    INQUIRY_SUCCESS,
                    transactionHCOmzetDto,
                    HttpStatus.OK);
        }


        return ResponseUtil.buildResponse(
                FAILED,
                INQUIRY_FAILED,
                null,
                HttpStatus.BAD_REQUEST);
    }
    public TRansactionOmzetSumDto sumTotalTransactionByRowNumber(TRansactionOmzetSumDto tRansactionOmzetSumDto, TransactionEntryDistOmzet x) {
        TRansactionOmzetSumDto res = new TRansactionOmzetSumDto();
        res.setJan(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getJan()) + Long.parseLong(Objects.equals(x.getJan(), "") ? "0" : x.getJan()));
        res.setFeb(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getFeb()) + Long.parseLong(Objects.equals(x.getFeb(), "") ? "0" : x.getFeb()));
        res.setMar(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getMar()) + Long.parseLong(Objects.equals(x.getMar(), "") ? "0" : x.getMar()));
        res.setApr(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getApr()) + Long.parseLong(Objects.equals(x.getApr(), "") ? "0" : x.getApr()));
        res.setMei(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getMei()) + Long.parseLong(Objects.equals(x.getMei(), "") ? "0" : x.getMei()));
        res.setJun(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getJun()) + Long.parseLong(Objects.equals(x.getJun(), "") ? "0" : x.getJun()));
        res.setJul(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getJul()) + Long.parseLong(Objects.equals(x.getJul(), "") ? "0" : x.getJul()));
        res.setAgt(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getAgt()) + Long.parseLong(Objects.equals(x.getAgt(), "") ? "0" : x.getAgt()));
        res.setSep(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getSep()) + Long.parseLong(Objects.equals(x.getSep(), "") ? "0" : x.getSep()));
        res.setOkt(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getOkt()) + Long.parseLong(Objects.equals(x.getOkt(), "") ? "0" : x.getOkt()));
        res.setNov(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getNov()) + Long.parseLong(Objects.equals(x.getNov(), "") ? "0" : x.getNov()));
        res.setDes(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getDes()) + Long.parseLong(Objects.equals(x.getDes(), "") ? "0" : x.getDes()));
        return res;
    }

    public TransactionHCOmzetDto sumTotal(TRansactionOmzetSumDto tRansactionOmzetSumDto, TransactionEntryDistOmzet x) {
        TransactionHCOmzetDto res = new TransactionHCOmzetDto();
        res.setId(x.getId());
        res.setProduct(x.getProduct());
        res.setCabangDivisi(x.getCabangDivisi());
        res.setPeriode(x.getPeriode());
        res.setYear(x.getTahun());
        res.setKeterangan(x.getKeterangan());
        res.setGroupProd(x.getGroupProd());
        res.setRowNumber(x.getRowNumber());
        res.setJan(tRansactionOmzetSumDto.getJan());
        res.setFeb(tRansactionOmzetSumDto.getFeb());
        res.setMar(tRansactionOmzetSumDto.getMar());
        res.setTwi(res.getMar()+res.getJan()+res.getFeb());
        res.setApr(tRansactionOmzetSumDto.getApr());
        res.setMei(tRansactionOmzetSumDto.getMei());
        res.setJun(tRansactionOmzetSumDto.getJun());
        res.setTwii(res.getApr()+res.getMei()+res.getJun());
        res.setJul(tRansactionOmzetSumDto.getJul());
        res.setAgt(tRansactionOmzetSumDto.getAgt());
        res.setSep(tRansactionOmzetSumDto.getSep());
        res.setTwiii(res.getJul()+res.getAgt()+res.getSep());
        res.setOkt(tRansactionOmzetSumDto.getOkt());
        res.setNov(tRansactionOmzetSumDto.getNov());
        res.setDes(tRansactionOmzetSumDto.getDes());
        res.setTwiv(res.getOkt()+res.getNov()+res.getDes());
        res.setTtl(res.getTwi()+res.getTwii()+res.getTwiii()+res.getTwiv());
        return res;
    }

    public TransactionOmzetDto sumTotal(TRansactionOmzetSumDto tRansactionOmzetSumDto, TransactionEntryLdOmzet x) {
        TransactionOmzetDto res = new TransactionOmzetDto();
        res.setId(x.getId());
        res.setProduct(x.getProduct());
        res.setCabangDivisi(x.getCabangDivisi());
        res.setPeriode(x.getPeriode());
        res.setYear(x.getYear());
        res.setKeterangan(x.getKeterangan());
        res.setGroupProd(x.getGroupProd());
        res.setRowNumber(x.getRowNumber());
        res.setJan(tRansactionOmzetSumDto.getJan());
        res.setFeb(tRansactionOmzetSumDto.getFeb());
        res.setMar(tRansactionOmzetSumDto.getMar());
        res.setTwi(res.getMar()+res.getJan()+res.getFeb());
        res.setApr(tRansactionOmzetSumDto.getApr());
        res.setMei(tRansactionOmzetSumDto.getMei());
        res.setJun(tRansactionOmzetSumDto.getJun());
        res.setTwii(res.getApr()+res.getMei()+res.getJun());
        res.setJul(tRansactionOmzetSumDto.getJul());
        res.setAgt(tRansactionOmzetSumDto.getAgt());
        res.setSep(tRansactionOmzetSumDto.getSep());
        res.setTwiii(res.getJul()+res.getAgt()+res.getSep());
        res.setOkt(tRansactionOmzetSumDto.getOkt());
        res.setNov(tRansactionOmzetSumDto.getNov());
        res.setDes(tRansactionOmzetSumDto.getDes());
        res.setTwiv(res.getOkt()+res.getNov()+res.getDes());
        res.setTtl(res.getTwi()+res.getTwii()+res.getTwiii()+res.getTwiv());
        return res;
    }

    public TRansactionOmzetSumDto sumTotalTransactionByRowNumber(TRansactionOmzetSumDto tRansactionOmzetSumDto, TransactionEntryRegOmzet x) {
        TRansactionOmzetSumDto res = new TRansactionOmzetSumDto();
        res.setJan(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getJan()) + Long.parseLong(Objects.equals(x.getJan(), "") ? "0" : x.getJan()));
        res.setFeb(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getFeb()) + Long.parseLong(Objects.equals(x.getFeb(), "") ? "0" : x.getFeb()));
        res.setMar(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getMar()) + Long.parseLong(Objects.equals(x.getMar(), "") ? "0" : x.getMar()));
        res.setApr(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getApr()) + Long.parseLong(Objects.equals(x.getApr(), "") ? "0" : x.getApr()));
        res.setMei(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getMei()) + Long.parseLong(Objects.equals(x.getMei(), "") ? "0" : x.getMei()));
        res.setJun(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getJun()) + Long.parseLong(Objects.equals(x.getJun(), "") ? "0" : x.getJun()));
        res.setJul(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getJul()) + Long.parseLong(Objects.equals(x.getJul(), "") ? "0" : x.getJul()));
        res.setAgt(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getAgt()) + Long.parseLong(Objects.equals(x.getAgt(), "") ? "0" : x.getAgt()));
        res.setSep(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getSep()) + Long.parseLong(Objects.equals(x.getSep(), "") ? "0" : x.getSep()));
        res.setOkt(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getOkt()) + Long.parseLong(Objects.equals(x.getOkt(), "") ? "0" : x.getOkt()));
        res.setNov(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getNov()) + Long.parseLong(Objects.equals(x.getNov(), "") ? "0" : x.getNov()));
        res.setDes(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getDes()) + Long.parseLong(Objects.equals(x.getDes(), "") ? "0" : x.getDes()));
        return res;
    }
    public TRansactionOmzetSumDto sumTotalTransactionByRowNumber(TRansactionOmzetSumDto tRansactionOmzetSumDto, TransactionEntryLdOmzet x) {
        TRansactionOmzetSumDto res = new TRansactionOmzetSumDto();
        res.setJan(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getJan()) + Long.parseLong(Objects.equals(x.getJan(), "") ? "0" : x.getJan()));
        res.setFeb(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getFeb()) + Long.parseLong(Objects.equals(x.getFeb(), "") ? "0" : x.getFeb()));
        res.setMar(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getMar()) + Long.parseLong(Objects.equals(x.getMar(), "") ? "0" : x.getMar()));
        res.setApr(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getApr()) + Long.parseLong(Objects.equals(x.getApr(), "") ? "0" : x.getApr()));
        res.setMei(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getMei()) + Long.parseLong(Objects.equals(x.getMei(), "") ? "0" : x.getMei()));
        res.setJun(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getJun()) + Long.parseLong(Objects.equals(x.getJun(), "") ? "0" : x.getJun()));
        res.setJul(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getJul()) + Long.parseLong(Objects.equals(x.getJul(), "") ? "0" : x.getJul()));
        res.setAgt(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getAgt()) + Long.parseLong(Objects.equals(x.getAgt(), "") ? "0" : x.getAgt()));
        res.setSep(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getSep()) + Long.parseLong(Objects.equals(x.getSep(), "") ? "0" : x.getSep()));
        res.setOkt(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getOkt()) + Long.parseLong(Objects.equals(x.getOkt(), "") ? "0" : x.getOkt()));
        res.setNov(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getNov()) + Long.parseLong(Objects.equals(x.getNov(), "") ? "0" : x.getNov()));
        res.setDes(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getDes()) + Long.parseLong(Objects.equals(x.getDes(), "") ? "0" : x.getDes()));
        return res;
    }

    public TransactionOmzetDto sumTotal(TRansactionOmzetSumDto tRansactionOmzetSumDto, TransactionEntryRegOmzet x) {
        TransactionOmzetDto res = new TransactionOmzetDto();
        res.setId(x.getId());
        res.setProduct(x.getProduct());
        res.setCabangDivisi(x.getCabangDivisi());
        res.setPeriode(x.getPeriode());
        res.setYear(x.getYear());
        res.setKeterangan(x.getKeterangan());
        res.setGroupProd(x.getGroupProd());
        res.setRowNumber(x.getRowNumber());
        res.setJan(tRansactionOmzetSumDto.getJan());
        res.setFeb(tRansactionOmzetSumDto.getFeb());
        res.setMar(tRansactionOmzetSumDto.getMar());
        res.setTwi(res.getMar()+res.getJan()+res.getFeb());
        res.setApr(tRansactionOmzetSumDto.getApr());
        res.setMei(tRansactionOmzetSumDto.getMei());
        res.setJun(tRansactionOmzetSumDto.getJun());
        res.setTwii(res.getApr()+res.getMei()+res.getJun());
        res.setJul(tRansactionOmzetSumDto.getJul());
        res.setAgt(tRansactionOmzetSumDto.getAgt());
        res.setSep(tRansactionOmzetSumDto.getSep());
        res.setTwiii(res.getJul()+res.getAgt()+res.getSep());
        res.setOkt(tRansactionOmzetSumDto.getOkt());
        res.setNov(tRansactionOmzetSumDto.getNov());
        res.setDes(tRansactionOmzetSumDto.getDes());
        res.setTwiv(res.getOkt()+res.getNov()+res.getDes());
        res.setTtl(res.getTwi()+res.getTwii()+res.getTwiii()+res.getTwiv());
        return res;
    }

    public TransactionOmzetDto sumTotal(TRansactionOmzetSumDto tRansactionOmzetSumDto, TransactionEntryServiceOmzet x) {
        TransactionOmzetDto res = new TransactionOmzetDto();
        res.setId(x.getId());
        res.setProduct(x.getProduct());
        res.setCabangDivisi(x.getCabangDivisi());
        res.setPeriode(x.getPeriode());
        res.setYear(x.getYear());
        res.setKeterangan(x.getKeterangan());
        res.setGroupProd(x.getGroupProd());
        res.setRowNumber(x.getRowNumber());
        res.setJan(tRansactionOmzetSumDto.getJan());
        res.setFeb(tRansactionOmzetSumDto.getFeb());
        res.setMar(tRansactionOmzetSumDto.getMar());
        res.setTwi(res.getMar()+res.getJan()+res.getFeb());
        res.setApr(tRansactionOmzetSumDto.getApr());
        res.setMei(tRansactionOmzetSumDto.getMei());
        res.setJun(tRansactionOmzetSumDto.getJun());
        res.setTwii(res.getApr()+res.getMei()+res.getJun());
        res.setJul(tRansactionOmzetSumDto.getJul());
        res.setAgt(tRansactionOmzetSumDto.getAgt());
        res.setSep(tRansactionOmzetSumDto.getSep());
        res.setTwiii(res.getJul()+res.getAgt()+res.getSep());
        res.setOkt(tRansactionOmzetSumDto.getOkt());
        res.setNov(tRansactionOmzetSumDto.getNov());
        res.setDes(tRansactionOmzetSumDto.getDes());
        res.setTwiv(res.getOkt()+res.getNov()+res.getDes());
        res.setTtl(res.getTwi()+res.getTwii()+res.getTwiii()+res.getTwiv());
        return res;
    }

    public TRansactionOmzetSumDto sumTotalTransactionByRowNumber(TRansactionOmzetSumDto tRansactionOmzetSumDto, TransactionEntryServiceOmzet x) {
        TRansactionOmzetSumDto res = new TRansactionOmzetSumDto();
        res.setJan(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getJan()) + Long.parseLong(Objects.equals(x.getJan(), "") ? "0" : x.getJan()));
        res.setFeb(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getFeb()) + Long.parseLong(Objects.equals(x.getFeb(), "") ? "0" : x.getFeb()));
        res.setMar(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getMar()) + Long.parseLong(Objects.equals(x.getMar(), "") ? "0" : x.getMar()));
        res.setApr(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getApr()) + Long.parseLong(Objects.equals(x.getApr(), "") ? "0" : x.getApr()));
        res.setMei(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getMei()) + Long.parseLong(Objects.equals(x.getMei(), "") ? "0" : x.getMei()));
        res.setJun(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getJun()) + Long.parseLong(Objects.equals(x.getJun(), "") ? "0" : x.getJun()));
        res.setJul(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getJul()) + Long.parseLong(Objects.equals(x.getJul(), "") ? "0" : x.getJul()));
        res.setAgt(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getAgt()) + Long.parseLong(Objects.equals(x.getAgt(), "") ? "0" : x.getAgt()));
        res.setSep(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getSep()) + Long.parseLong(Objects.equals(x.getSep(), "") ? "0" : x.getSep()));
        res.setOkt(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getOkt()) + Long.parseLong(Objects.equals(x.getOkt(), "") ? "0" : x.getOkt()));
        res.setNov(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getNov()) + Long.parseLong(Objects.equals(x.getNov(), "") ? "0" : x.getNov()));
        res.setDes(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getDes()) + Long.parseLong(Objects.equals(x.getDes(), "") ? "0" : x.getDes()));
        return res;
    }

    public TransactionOmzetDto sumTotal(TRansactionOmzetSumDto tRansactionOmzetSumDto, TransactionEntrySurgeryOmzet x) {
        TransactionOmzetDto res = new TransactionOmzetDto();
        res.setId(x.getId());
        res.setProduct(x.getProduct());
        res.setCabangDivisi(x.getCabangDivisi());
        res.setPeriode(x.getPeriode());
        res.setYear(x.getYear());
        res.setKeterangan(x.getKeterangan());
        res.setGroupProd(x.getGroupProd());
        res.setRowNumber(x.getRowNumber());
        res.setJan(tRansactionOmzetSumDto.getJan());
        res.setFeb(tRansactionOmzetSumDto.getFeb());
        res.setMar(tRansactionOmzetSumDto.getMar());
        res.setTwi(res.getMar()+res.getJan()+res.getFeb());
        res.setApr(tRansactionOmzetSumDto.getApr());
        res.setMei(tRansactionOmzetSumDto.getMei());
        res.setJun(tRansactionOmzetSumDto.getJun());
        res.setTwii(res.getApr()+res.getMei()+res.getJun());
        res.setJul(tRansactionOmzetSumDto.getJul());
        res.setAgt(tRansactionOmzetSumDto.getAgt());
        res.setSep(tRansactionOmzetSumDto.getSep());
        res.setTwiii(res.getJul()+res.getAgt()+res.getSep());
        res.setOkt(tRansactionOmzetSumDto.getOkt());
        res.setNov(tRansactionOmzetSumDto.getNov());
        res.setDes(tRansactionOmzetSumDto.getDes());
        res.setTwiv(res.getOkt()+res.getNov()+res.getDes());
        res.setTtl(res.getTwi()+res.getTwii()+res.getTwiii()+res.getTwiv());
        return res;
    }

    public TransactionOmzetDto sumTotal(TRansactionOmzetSumDto tRansactionOmzetSumDto, TransactionEntryTenderOmzet x) {
        TransactionOmzetDto res = new TransactionOmzetDto();
        res.setId(x.getId());
        res.setProduct(x.getProduct());
        res.setCabangDivisi(x.getCabangDivisi());
        res.setPeriode(x.getPeriode());
        res.setYear(x.getYear());
        res.setKeterangan(x.getKeterangan());
        res.setGroupProd(x.getGroupProd());
        res.setRowNumber(x.getRowNumber());
        res.setJan(tRansactionOmzetSumDto.getJan());
        res.setFeb(tRansactionOmzetSumDto.getFeb());
        res.setMar(tRansactionOmzetSumDto.getMar());
        res.setTwi(res.getMar()+res.getJan()+res.getFeb());
        res.setApr(tRansactionOmzetSumDto.getApr());
        res.setMei(tRansactionOmzetSumDto.getMei());
        res.setJun(tRansactionOmzetSumDto.getJun());
        res.setTwii(res.getApr()+res.getMei()+res.getJun());
        res.setJul(tRansactionOmzetSumDto.getJul());
        res.setAgt(tRansactionOmzetSumDto.getAgt());
        res.setSep(tRansactionOmzetSumDto.getSep());
        res.setTwiii(res.getJul()+res.getAgt()+res.getSep());
        res.setOkt(tRansactionOmzetSumDto.getOkt());
        res.setNov(tRansactionOmzetSumDto.getNov());
        res.setDes(tRansactionOmzetSumDto.getDes());
        res.setTwiv(res.getOkt()+res.getNov()+res.getDes());
        res.setTtl(res.getTwi()+res.getTwii()+res.getTwiii()+res.getTwiv());
        return res;
    }

    public TRansactionOmzetSumDto sumTotalTransactionByRowNumber(TRansactionOmzetSumDto tRansactionOmzetSumDto, TransactionEntryTenderOmzet x) {
        TRansactionOmzetSumDto res = new TRansactionOmzetSumDto();
        res.setJan(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getJan()) + Long.parseLong(Objects.equals(x.getJan(), "") ? "0" : x.getJan()));
        res.setFeb(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getFeb()) + Long.parseLong(Objects.equals(x.getFeb(), "") ? "0" : x.getFeb()));
        res.setMar(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getMar()) + Long.parseLong(Objects.equals(x.getMar(), "") ? "0" : x.getMar()));
        res.setApr(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getApr()) + Long.parseLong(Objects.equals(x.getApr(), "") ? "0" : x.getApr()));
        res.setMei(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getMei()) + Long.parseLong(Objects.equals(x.getMei(), "") ? "0" : x.getMei()));
        res.setJun(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getJun()) + Long.parseLong(Objects.equals(x.getJun(), "") ? "0" : x.getJun()));
        res.setJul(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getJul()) + Long.parseLong(Objects.equals(x.getJul(), "") ? "0" : x.getJul()));
        res.setAgt(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getAgt()) + Long.parseLong(Objects.equals(x.getAgt(), "") ? "0" : x.getAgt()));
        res.setSep(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getSep()) + Long.parseLong(Objects.equals(x.getSep(), "") ? "0" : x.getSep()));
        res.setOkt(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getOkt()) + Long.parseLong(Objects.equals(x.getOkt(), "") ? "0" : x.getOkt()));
        res.setNov(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getNov()) + Long.parseLong(Objects.equals(x.getNov(), "") ? "0" : x.getNov()));
        res.setDes(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getDes()) + Long.parseLong(Objects.equals(x.getDes(), "") ? "0" : x.getDes()));
        return res;
    }

    public TransactionOmzetDto sumTotal(TRansactionOmzetSumDto tRansactionOmzetSumDto, TransactionEntryTradingOmzet x) {
        TransactionOmzetDto res = new TransactionOmzetDto();
        res.setId(x.getId());
        res.setProduct(x.getProduct());
        res.setCabangDivisi(x.getCabangDivisi());
        res.setPeriode(x.getPeriode());
        res.setYear(x.getYear());
        res.setKeterangan(x.getKeterangan());
        res.setGroupProd(x.getGroupProd());
        res.setRowNumber(x.getRowNumber());
        res.setJan(tRansactionOmzetSumDto.getJan());
        res.setFeb(tRansactionOmzetSumDto.getFeb());
        res.setMar(tRansactionOmzetSumDto.getMar());
        res.setTwi(res.getMar()+res.getJan()+res.getFeb());
        res.setApr(tRansactionOmzetSumDto.getApr());
        res.setMei(tRansactionOmzetSumDto.getMei());
        res.setJun(tRansactionOmzetSumDto.getJun());
        res.setTwii(res.getApr()+res.getMei()+res.getJun());
        res.setJul(tRansactionOmzetSumDto.getJul());
        res.setAgt(tRansactionOmzetSumDto.getAgt());
        res.setSep(tRansactionOmzetSumDto.getSep());
        res.setTwiii(res.getJul()+res.getAgt()+res.getSep());
        res.setOkt(tRansactionOmzetSumDto.getOkt());
        res.setNov(tRansactionOmzetSumDto.getNov());
        res.setDes(tRansactionOmzetSumDto.getDes());
        res.setTwiv(res.getOkt()+res.getNov()+res.getDes());
        res.setTtl(res.getTwi()+res.getTwii()+res.getTwiii()+res.getTwiv());
        return res;
    }

    public TRansactionOmzetSumDto sumTotalTransactionByRowNumber(TRansactionOmzetSumDto tRansactionOmzetSumDto, TransactionEntryTradingOmzet x) {
        TRansactionOmzetSumDto res = new TRansactionOmzetSumDto();
        res.setJan(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getJan()) + Long.parseLong(Objects.equals(x.getJan(), "") ? "0" : x.getJan()));
        res.setFeb(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getFeb()) + Long.parseLong(Objects.equals(x.getFeb(), "") ? "0" : x.getFeb()));
        res.setMar(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getMar()) + Long.parseLong(Objects.equals(x.getMar(), "") ? "0" : x.getMar()));
        res.setApr(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getApr()) + Long.parseLong(Objects.equals(x.getApr(), "") ? "0" : x.getApr()));
        res.setMei(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getMei()) + Long.parseLong(Objects.equals(x.getMei(), "") ? "0" : x.getMei()));
        res.setJun(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getJun()) + Long.parseLong(Objects.equals(x.getJun(), "") ? "0" : x.getJun()));
        res.setJul(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getJul()) + Long.parseLong(Objects.equals(x.getJul(), "") ? "0" : x.getJul()));
        res.setAgt(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getAgt()) + Long.parseLong(Objects.equals(x.getAgt(), "") ? "0" : x.getAgt()));
        res.setSep(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getSep()) + Long.parseLong(Objects.equals(x.getSep(), "") ? "0" : x.getSep()));
        res.setOkt(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getOkt()) + Long.parseLong(Objects.equals(x.getOkt(), "") ? "0" : x.getOkt()));
        res.setNov(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getNov()) + Long.parseLong(Objects.equals(x.getNov(), "") ? "0" : x.getNov()));
        res.setDes(ConvertionUtils.nullToLong(tRansactionOmzetSumDto.getDes()) + Long.parseLong(Objects.equals(x.getDes(), "") ? "0" : x.getDes()));
        return res;
    }


    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


}
