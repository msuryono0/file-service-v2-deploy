package com.rni.rni.service;

import com.rni.rni.domain.dto.CommonSearchDto;
import com.rni.rni.domain.dto.FilterSearchDto;
import com.rni.rni.domain.dto.FilterSortDto;
import lombok.AllArgsConstructor;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;
@AllArgsConstructor
public class CommonSearchService<T> implements Specification<T> {

    private final transient CommonSearchDto request;

    @Override
    public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
        Predicate predicate = criteriaBuilder.equal(criteriaBuilder.literal(Boolean.TRUE), Boolean.TRUE);

        for (FilterSearchDto filter : this.request.getFilters()) {
            predicate = filter.getOperator().build(root, criteriaBuilder, filter, predicate);
        }

        List<Order> orders = new ArrayList<>();
        for (FilterSortDto sort : this.request.getSorts()) {
            orders.add(sort.getDirection().build(root, criteriaBuilder, sort));
        }

        query.orderBy(orders);
        return predicate;
    }
}
