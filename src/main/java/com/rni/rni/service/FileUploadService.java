package com.rni.rni.service;

import com.rni.rni.constant.FileUploadConstant;
import com.rni.rni.domain.dto.Metadata;
import com.rni.rni.exception.SheetNotMatchException;
import com.rni.rni.repository.FileUploadersRepoistory;
import com.rni.rni.utils.InsertLogMonitoring;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.xml.sax.SAXException;
import util.ApiResponse;
import util.ResponseUtil;

import java.io.IOException;
import java.util.Objects;

@Service
@Slf4j
public class FileUploadService {

    @Autowired
    private FileUploadersRepoistory fileUploadersRepoistory;

    @Autowired
    private FinanceService financeService;

    @Autowired
    private ProductService productService;

    @Autowired
    private KaryawanService karyawanService;

    @Autowired
    private InsertLogMonitoring insertLogMonitoring;

    @Autowired
    private RestTemplate restTemplate;
    
    @Autowired
    private TransaksiPusatService transaksiPusatService;

    @Value("${url.trigger}")
    private String url;


    public ResponseEntity<Object> handleUpload(MultipartFile multipartFile, String request, Metadata metadata) throws IOException, OpenXML4JException, SAXException {
        ResponseEntity<Object> response = ResponseUtil.buildResponse("FAILED",
                "INTERNAL SERVER ERORR",
                null,
                HttpStatus.INTERNAL_SERVER_ERROR);
        Workbook workbook = new XSSFWorkbook(multipartFile.getInputStream());
        Sheet sheet = workbook.getSheetAt(0);
        String sheetName = sheet.getSheetName();
        //validate sheet name not match with table name
        if (!sheetName.equals(request))
            throw new SheetNotMatchException("Sheet yang di upload tidak sesuai");
        log.info("start upload");
        try {
            switch (request) {
                case FileUploadConstant.FA_CATEGORY:
                    response = financeService.faCategory(workbook, sheet, multipartFile.getSize(), metadata);
                    break;
                case FileUploadConstant.PROD_GROUP:
                    response = productService.prodGroup(workbook, sheet, multipartFile.getSize(), metadata);
                    break;
                case FileUploadConstant.PROD_CLASS:
                    response = productService.prodClass(workbook, sheet, multipartFile.getSize(), metadata);
                    break;
                case FileUploadConstant.PROD_TYPE:
                    response = productService.prodType(workbook, sheet, multipartFile.getSize(), metadata);
                    break;
                case FileUploadConstant.DIVISI:
                    response = karyawanService.divisiUpload(workbook, sheet, multipartFile.getSize(), metadata);
                    break;
                case FileUploadConstant.JABATAN:
                    response = karyawanService.jabatanUpload(workbook, sheet, multipartFile.getSize(), metadata);
                    break;
                case FileUploadConstant.GOLONGAN:
                    response = karyawanService.golonganUpload(workbook, sheet, multipartFile.getSize(), metadata);
                    break;
                case FileUploadConstant.STATUS_KARYAWAN:
                    response = karyawanService.statusKaryawanUpload(workbook, sheet, multipartFile.getSize(), metadata);
                    break;
                case FileUploadConstant.FA_CLASS:
                    response = financeService.faClass(workbook, sheet, multipartFile.getSize(), metadata);
                    break;
                case FileUploadConstant.FA_SUB_CLASS:
                    response = financeService.faSubClass(workbook, sheet, multipartFile.getSize(), metadata);
                    break;
                case FileUploadConstant.PROD_CATEGORY:
                    response = productService.prodCategory(workbook, sheet, multipartFile.getSize(), metadata);
                    break;
                case FileUploadConstant.DIMENSION:
                    response = karyawanService.dimensionUpload(workbook, sheet, multipartFile.getSize(), metadata);
                    break;
                case FileUploadConstant.DIMENSION_VALUE:
                    response = karyawanService.dimensionValueUpload(workbook, sheet, multipartFile.getSize(), metadata);
                    break;
                case FileUploadConstant.GAJI_KARYAWAN:
                    response = karyawanService.gajiKaryawanUpload(workbook, sheet, multipartFile.getSize(), metadata);
                    break;
                case FileUploadConstant.GL_ACCOUNT:
                    response = financeService.glAccount(workbook, sheet, multipartFile.getSize(), metadata);
                    break;
                case FileUploadConstant.FIXED_ASSET:
                    response = financeService.fixedAsset(workbook, sheet, multipartFile.getSize(), metadata);
                    break;
                case FileUploadConstant.PRODUCT:
                    response = productService.productUpload(workbook, sheet, multipartFile.getSize(), metadata);
                    break;
                case FileUploadConstant.EMPLOYEE:
                    response = karyawanService.employee(workbook, sheet, multipartFile.getSize(), metadata);
                    break;
                case FileUploadConstant.STRUKTURAL:
                    response = karyawanService.struktural(workbook, sheet, multipartFile.getSize(), metadata);
                    break;
                default:
                    break;
            }
            restTemplate.postForEntity(url, true, void.class);
            log.info("data being upload");
            var apiResponse = (ApiResponse) response.getBody();
            if (Objects.nonNull(apiResponse)) {
                response = ResponseUtil.buildResponse(
                        apiResponse.getResponseKey(),
                        apiResponse.getMessage(),
                        apiResponse.getData(),
                        HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("Error while saving data {}", e);
            response = ResponseUtil.buildResponse(
                    "FAILED",
                    e.getMessage(),
                    null,
                    HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return response;
    }


    public ResponseEntity<Object> handleUploadTransaksi(MultipartFile multipartFile, String year, String periode, Metadata metadata, String request) throws IOException, OpenXML4JException, SAXException {
        ResponseEntity<Object> response = ResponseUtil.buildResponse("FAILED",
                "INTERNAL SERVER ERORR",
                null,
                HttpStatus.INTERNAL_SERVER_ERROR);
        Workbook workbook = new XSSFWorkbook(multipartFile.getInputStream());
        Sheet sheet = workbook.getSheetAt(0);
        String sheetName = sheet.getSheetName();
        //validate sheet name not match with table name
        if (!sheetName.equals(request))
            throw new SheetNotMatchException("Sheet yang di upload tidak sesuai");
        log.info("start upload");
        try {
            switch (request) {
                case FileUploadConstant.TRANSACTION_ENTRY_HC_SHARE:
                    response = transaksiPusatService.transactionHcShare(multipartFile,year,periode,metadata);
                    break;
                case FileUploadConstant.PJ_BERAS:
                    response = transaksiPusatService.beras(multipartFile,year,metadata);
                    break;
                case FileUploadConstant.AP9:
                    response = transaksiPusatService.uploadAp9(multipartFile,year,metadata);
                    break;
                case FileUploadConstant.TRANSACTION_ENTRY_HC_OMZET:
                    response = transaksiPusatService.transactionOmzet(multipartFile,year,periode,metadata);
                    break;

                default:
                    break;
            }
            restTemplate.postForEntity(url, true, void.class);
            log.info("data being upload");
            var apiResponse = (ApiResponse) response.getBody();
            if (Objects.nonNull(apiResponse)) {
                response = ResponseUtil.buildResponse(
                        apiResponse.getResponseKey(),
                        apiResponse.getMessage(),
                        apiResponse.getData(),
                        HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("Error while saving data {}", e);
            response = ResponseUtil.buildResponse(
                    "FAILED",
                    e.getMessage(),
                    null,
                    HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return response;
    }
}
