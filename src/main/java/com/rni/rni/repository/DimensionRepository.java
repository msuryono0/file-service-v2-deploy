package com.rni.rni.repository;

import com.rni.rni.domain.dao.Dimension;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DimensionRepository extends JpaRepository<Dimension, Long> {
    Dimension findByCode(String stringCellValue);
    List<Dimension> findByKeteranganContainingIgnoreCase(String keterangan);
}
