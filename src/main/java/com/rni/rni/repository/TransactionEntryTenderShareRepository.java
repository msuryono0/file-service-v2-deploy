package com.rni.rni.repository;

import com.rni.rni.domain.dao.TransactionEntryTenderShare;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TransactionEntryTenderShareRepository extends JpaRepository<TransactionEntryTenderShare,Long> {

}
