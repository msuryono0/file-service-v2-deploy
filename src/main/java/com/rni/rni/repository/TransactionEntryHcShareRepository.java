package com.rni.rni.repository;

import com.rni.rni.domain.dao.TransactionEntryHcShare;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TransactionEntryHcShareRepository extends JpaRepository<TransactionEntryHcShare,Long> {

}
