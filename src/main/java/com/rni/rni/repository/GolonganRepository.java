package com.rni.rni.repository;

import com.rni.rni.domain.dao.Golongan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GolonganRepository extends JpaRepository<Golongan,Long> {

    @Query(value = "select g from Golongan g where g.code like %:search% or g.keterangan like %:search%",nativeQuery = true)
    List<Golongan> findGolongan(@Param("search") String search);
    Golongan findByCode(String stringCellValue);
}
