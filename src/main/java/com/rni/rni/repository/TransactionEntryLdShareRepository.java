package com.rni.rni.repository;

import com.rni.rni.domain.dao.TransactionEntryLdShare;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TransactionEntryLdShareRepository extends JpaRepository<TransactionEntryLdShare,Long> {

}
