package com.rni.rni.repository;

import com.rni.rni.domain.dao.AP9;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface Ap9Repository extends JpaRepository<AP9,Long> {

    @Query(value = "select a from AP9 a where lower(a.cabangDivisi.keterangan) like lower(concat('%', :codeSearch,'%')) or :codeSearch is null and " +
            "lower(a.struktural.strukturalName) like lower(concat('%', :search,'%')) or :search is null ")
    List<AP9> findAllAp9(@Param("codeSearch") String codeSearch,@Param("search") String search);
}
