package com.rni.rni.repository;

import com.rni.rni.domain.dao.Dimension;
import com.rni.rni.domain.dao.GajiKaryawan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface GajiKaryawanRepository extends JpaRepository<GajiKaryawan,Long> {

    @Query(value = "select gk from GajiKaryawan gk where (gk.cabangCode.code = :cabangCode or :cabangCode is null) AND " +
            "(gk.divisiCode.code =:divisiCode or :divisiCode is null ) AND (gk.golonganCode.keterangan like %:search% or :search is null)")
    List<GajiKaryawan> findGajiKaryawan(@Param("cabangCode") String cabangCode,
                            @Param("divisiCode") String divisiCode,
                            @Param("search") String search);

    GajiKaryawan findByCabangCode(Dimension dimension);
}
