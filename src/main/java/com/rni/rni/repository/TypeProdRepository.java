package com.rni.rni.repository;

import com.rni.rni.domain.dao.TypeProd;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TypeProdRepository extends JpaRepository<TypeProd,Long> {
    TypeProd findByTypeId (String typeId);
}
