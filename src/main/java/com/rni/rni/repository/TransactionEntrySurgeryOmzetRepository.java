package com.rni.rni.repository;

import com.rni.rni.domain.dao.TransactionEntrySurgeryOmzet;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TransactionEntrySurgeryOmzetRepository extends JpaRepository<TransactionEntrySurgeryOmzet,Long> {

    List<TransactionEntrySurgeryOmzet> findByYearAndPeriodeAndCabangDivisi(String tahun, String periode, String unit);
}
