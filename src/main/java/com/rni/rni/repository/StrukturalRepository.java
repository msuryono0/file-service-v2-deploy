package com.rni.rni.repository;

import com.rni.rni.domain.dao.Struktural;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StrukturalRepository extends JpaRepository<Struktural,Long> {
    List<Struktural> findByStrukturalName(String name);
}
