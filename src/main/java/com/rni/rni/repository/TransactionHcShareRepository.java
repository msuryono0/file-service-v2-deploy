package com.rni.rni.repository;

import com.rni.rni.domain.dao.TransactionEntryHcShare;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TransactionHcShareRepository extends JpaRepository<TransactionEntryHcShare,Long> {

}
