package com.rni.rni.repository;

import com.rni.rni.domain.dao.TransactionEntryMedicalOmzet;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TransactionEntryMedicalOmzetRepository extends JpaRepository<TransactionEntryMedicalOmzet,Long> {

}
