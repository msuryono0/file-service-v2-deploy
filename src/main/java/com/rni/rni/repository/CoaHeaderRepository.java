package com.rni.rni.repository;

import com.rni.rni.domain.dao.CoaHeader;
import com.rni.rni.domain.dao.TbGlAccount;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CoaHeaderRepository extends JpaRepository<CoaHeader, Long> {
    CoaHeader findByKodeRek(String kodeRek);
    CoaHeader findByKodeRek(TbGlAccount kodeRek);
}
