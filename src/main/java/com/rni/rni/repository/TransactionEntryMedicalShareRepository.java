package com.rni.rni.repository;

import com.rni.rni.domain.dao.TransactionEntryMedicalShare;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TransactionEntryMedicalShareRepository extends JpaRepository<TransactionEntryMedicalShare,Long> {

}
