package com.rni.rni.repository;

import com.rni.rni.domain.dao.TransactionEntryRegOmzet;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TransactionEntryRegOmzetRepository extends JpaRepository<TransactionEntryRegOmzet,Long> {

    List<TransactionEntryRegOmzet> findByYearAndPeriodeAndCabangDivisi(String tahun, String periode, String unit);
}
