package com.rni.rni.repository;

import com.rni.rni.domain.dao.TransactionEntryEquipmentShare;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TransactionEntryEquipmentShareRepository extends JpaRepository<TransactionEntryEquipmentShare,Long> {

}
