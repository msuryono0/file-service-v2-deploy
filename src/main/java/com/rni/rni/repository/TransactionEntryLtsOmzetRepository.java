package com.rni.rni.repository;

import com.rni.rni.domain.dao.TransactionEntryLdOmzet;
import com.rni.rni.domain.dao.TransactionEntryLtsOmzet;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TransactionEntryLtsOmzetRepository extends JpaRepository<TransactionEntryLtsOmzet,Long> {

    List<TransactionEntryLdOmzet> findByYearAndPeriodeAndCabangDivisi(String tahun, String periode, String unit);
}
