package com.rni.rni.repository;

import com.rni.rni.domain.dao.Uom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UomRepository extends JpaRepository<Uom,Long> {
    Uom findByKeterangan (String keterangan);
    List<Uom> findByKeteranganContainingIgnoreCase(String keterangan);
}
