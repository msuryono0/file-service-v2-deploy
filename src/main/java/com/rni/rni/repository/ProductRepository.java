package com.rni.rni.repository;

import com.rni.rni.domain.dao.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<Product,Long> {
    @Query(value = "select p from Product p where " +
            "p.groupProd.groupItem LIKE %:search% OR " +
            "p.type.typeName LIKE %:search% OR " +
            "p.uom.keterangan LIKE %:search% OR " +
            "p.productName LIKE %:search% OR " +
            "p.productId LIKE %:search% OR " +
            "p.supplier.supplierName LIKE %:search%",nativeQuery = false)
    List<Product> findProduct(@Param("search") String search);

    Product findByProductNameContainingIgnoreCase(String dataCol);
}
