package com.rni.rni.repository;

import com.rni.rni.domain.dao.StatusKaryawan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StatusKaryawanRepository extends JpaRepository<StatusKaryawan,Long> {

    @Query(value = "select sk from StatusKaryawan sk where sk.code like %:search% or sk.keterangan like %:search%")
    List<StatusKaryawan> findStatusKaryawan(@Param("search") String search);
    StatusKaryawan findByCode(String stringCellValue);
}
