package com.rni.rni.repository;

import com.rni.rni.domain.dao.Permission;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PermissionRepository extends JpaRepository<Permission, Long> {

    @Query(value = "select p from Permission p where " +
            "p.menuCode.menuName LIKE %:search% OR " +
            "p.subMenuCode.subMenuName LIKE %:search% OR " +
            "p.levelMenuCode.levelMenuName LIKE %:search%",nativeQuery = false)
    List<Permission> findPermission(@Param("search") String search);
}
