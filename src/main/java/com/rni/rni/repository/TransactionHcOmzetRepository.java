package com.rni.rni.repository;

import com.rni.rni.domain.dao.TransactionEntryHcOmzet;
import com.rni.rni.domain.dto.TRansactionOmzetSumDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TransactionHcOmzetRepository extends JpaRepository<TransactionEntryHcOmzet, Long> {

    @Query(value = "select t from TransactionEntryHcOmzet t " +
            "where t.year = :tahun and t.cabangDivisi = :unit and t.periode = :periode ")
    List<TransactionEntryHcOmzet> findByYearAndCabangDivisi_CodeAndPeriode(String tahun, String unit, String periode);

    @Query(value = "select sum('jan') from transaction_entry_hc_omzet t where t.row_number IN (:whereIn)", nativeQuery = true)
    TRansactionOmzetSumDto sumOmzetRow(String[] whereIn);
}
