
package com.rni.rni.repository;

import com.rni.rni.domain.dao.PerjalananDinas;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PerjalananDinasRepository extends JpaRepository<PerjalananDinas, Integer> {
     @Query("select pj from PerjalananDinas pj where (pj.kode = :kode or :kode is null)")
     List<PerjalananDinas> findPerjalananDinas(@Param("kode") String kode);
}
