package com.rni.rni.repository;

import com.rni.rni.domain.dao.Pendidikan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PindidikanRepository extends JpaRepository<Pendidikan, Integer> {
    Pendidikan findByCode(String stringCellValue);
}
