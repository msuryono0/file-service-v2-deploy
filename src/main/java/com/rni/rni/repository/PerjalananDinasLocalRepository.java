
package com.rni.rni.repository;

import com.rni.rni.domain.dao.PerjalananDinasLocal;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PerjalananDinasLocalRepository extends JpaRepository<PerjalananDinasLocal, Integer> {
     @Query("select pj from PerjalananDinasLocal pj where pj.kode = :kode And (pj.lokasi like %:search% or :search is null)")
     List<PerjalananDinasLocal> findPerjalananDinasLocal(@Param("kode") String kode,
                                               @Param("search") String search);
}
