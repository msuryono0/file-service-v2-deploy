package com.rni.rni.repository;

import com.rni.rni.domain.dao.TransactionEntryTenderOmzet;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TransactionEntryTenderOmzetRepository extends JpaRepository<TransactionEntryTenderOmzet,Long> {

    List<TransactionEntryTenderOmzet> findByYearAndPeriodeAndCabangDivisi(String tahun, String periode, String unit);
}
