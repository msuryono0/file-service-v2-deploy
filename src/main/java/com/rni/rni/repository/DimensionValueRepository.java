package com.rni.rni.repository;

import com.rni.rni.domain.dao.Dimension;
import com.rni.rni.domain.dao.DimensionValue;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DimensionValueRepository extends JpaRepository<DimensionValue, Long> {
    DimensionValue findByCode(String code);
    List<DimensionValue> findByDimensionCode(Dimension dimension);
    List<DimensionValue> findByKeteranganContainingIgnoreCaseOrCodeAndDimensionCode(String keterangan,
                                                                                    String Code,
                                                                                    Dimension dimensionValue);

    List<DimensionValue> findByKeteranganContainingIgnoreCaseOrCode(String keterangan,
                                                                    String Code);
}
