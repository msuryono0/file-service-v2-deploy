package com.rni.rni.repository;

import com.rni.rni.domain.dao.Jabatan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface JabatanRepository extends JpaRepository<Jabatan,Long> {

    @Query(value = "select j from Jabatan j where j.code like %:search% or j.keterangan like %:search%")
    List<Jabatan> findJabatan(@Param("search") String search);
    Jabatan findByCode(String stringCellValue);
}
