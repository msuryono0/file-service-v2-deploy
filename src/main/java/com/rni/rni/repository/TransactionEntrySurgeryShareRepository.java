package com.rni.rni.repository;

import com.rni.rni.domain.dao.TransactionEntrySurgeryShare;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TransactionEntrySurgeryShareRepository extends JpaRepository<TransactionEntrySurgeryShare,Long> {

}
