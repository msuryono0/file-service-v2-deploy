package com.rni.rni.repository;

import com.rni.rni.domain.dao.TransactionEntryRegShare;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TransactionEntryRegShareRepository extends JpaRepository<TransactionEntryRegShare,Long> {

}
