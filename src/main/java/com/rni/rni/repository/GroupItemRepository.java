package com.rni.rni.repository;

import com.rni.rni.domain.dao.GroupItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GroupItemRepository extends JpaRepository<GroupItem,Long> {

    GroupItem findByGroupId(String groupId);
}
