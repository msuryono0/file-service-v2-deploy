package com.rni.rni.repository;

import com.rni.rni.domain.dao.TbMParameterSeragam;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TbMParameterSeragamRepository extends JpaRepository<TbMParameterSeragam,Long> {
}
