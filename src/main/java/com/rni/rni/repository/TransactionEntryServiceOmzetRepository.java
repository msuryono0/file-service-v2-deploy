package com.rni.rni.repository;

import com.rni.rni.domain.dao.TransactionEntryServiceOmzet;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TransactionEntryServiceOmzetRepository extends JpaRepository<TransactionEntryServiceOmzet,Long> {

    List<TransactionEntryServiceOmzet> findByYearAndPeriodeAndCabangDivisi(String tahun, String periode, String unit);
}
