package com.rni.rni.repository;

import com.rni.rni.domain.dao.ProdGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GroupProdRepository extends JpaRepository<ProdGroup,Long> {

    ProdGroup findByGroupProd(String groupId);
}
