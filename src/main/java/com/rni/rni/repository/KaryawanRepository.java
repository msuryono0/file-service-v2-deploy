package com.rni.rni.repository;

import com.rni.rni.domain.dao.Karyawan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface KaryawanRepository extends JpaRepository<Karyawan,Long> {
    @Query(value = "select k.id,k.nik, d.keterangan as cabang, dv.keterangan as divisi, k.nama,\n" +
            "j.keterangan as jabatan, k.\"level\" ,sk.keterangan as status_karyawan,g.keterangan as golongan,\n" +
            "p.keterangan as pendidikan,k.tanggal_bekerja ,k.tanggal_lahir ,sp.keterangan as status_keluarga ,\n" +
            "tms.keterangan as type_karyawan ,j.keterangan as jurusan ,k.tahun_lulus ,k.tempat_lahir ,k.tanggal_tetap ,\n" +
            "k.tempat_masuk ,k.struktural ,k.sekolah,k.created_at,k.created_by,k.updated_by,k.updated_at\n" +
            "from karyawan k \n" +
            "join dimension d on k.cabang = d.code \n" +
            "join dimension_value dv on k.divisi = dv.code \n" +
            "left join jabatan j on j.code = k.jabatan \n" +
            "left join status_karyawan sk on sk.code  = k.status_karyawan \n" +
            "left join golongan g on g.code  = k.golongan \n" +
            "left join status_pernikahan sp on sp.code  = k.status_keluarga \n" +
            "left join pendidikan p on p.code  = k.pendidikan \n" +
            "left join tb_m_system tms on tms.sys_code = 'karyawan' and tms.sys_desc ='karyawan_type' and tms.value = cast(k.type_karyawan as varchar)\n" +
            "where k.nama like %:search% or k.nik like %:search% or d.keterangan like %:search%  or dv.keterangan like %:search%",nativeQuery = true)
    List<Karyawan> findKaryawan(@Param("search") String search);
    @Query(value = "select k.id,k.nik, d.keterangan as cabang, dv.keterangan as divisi, k.nama,\n" +
            "j.keterangan as jabatan, k.\"level\" ,sk.keterangan as status_karyawan,g.keterangan as golongan,\n" +
            "p.keterangan as pendidikan,k.tanggal_bekerja ,k.tanggal_lahir ,sp.keterangan as status_keluarga ,\n" +
            "tms.keterangan as type_karyawan ,j.keterangan as jurusan ,k.tahun_lulus ,k.tempat_lahir ,k.tanggal_tetap ,\n" +
            "k.tempat_masuk ,k.struktural ,k.sekolah,k.created_at,k.created_by,k.updated_by,k.updated_at\n" +
            "from karyawan k \n" +
            "join dimension d on k.cabang = d.code \n" +
            "join dimension_value dv on k.divisi = dv.code \n" +
            "left join jabatan j on j.code = k.jabatan \n" +
            "left join status_karyawan sk on sk.code  = k.status_karyawan \n" +
            "left join golongan g on g.code  = k.golongan \n" +
            "left join status_pernikahan sp on sp.code  = k.status_keluarga \n" +
            "left join pendidikan p on p.code  = k.pendidikan \n" +
            "left join tb_m_system tms on tms.sys_code = 'karyawan' and tms.sys_desc ='karyawan_type' and tms.value = cast(k.type_karyawan as varchar)",nativeQuery = true)
    List<Karyawan> findKaryawan();

    Karyawan findByNamaContainingIgnoreCase(String dataCol);
}
