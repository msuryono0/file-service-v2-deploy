package com.rni.rni.repository;

import com.rni.rni.domain.dao.TransactionEntryDistOmzet;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TransactionEntryDistOmzetRepository extends JpaRepository<TransactionEntryDistOmzet,Long> {

    List<TransactionEntryDistOmzet> findByTahunAndPeriodeAndCabangDivisi(String tahun, String periode, String unit);
}
