package com.rni.rni.repository;

import com.rni.rni.domain.dao.Jabatan;
import com.rni.rni.domain.dao.TbParameterBeras;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TbParameterBerasRepository extends JpaRepository<TbParameterBeras,Long> {
    List<TbParameterBeras> findByJabatan(Jabatan jabatan);
}
