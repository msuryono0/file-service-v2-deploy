package com.rni.rni.repository;

import com.rni.rni.domain.dao.UserLevel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserLevelRepository extends JpaRepository<UserLevel, Long> {
}
