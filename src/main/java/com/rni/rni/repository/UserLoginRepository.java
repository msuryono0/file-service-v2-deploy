package com.rni.rni.repository;

import com.rni.rni.domain.dao.Dimension;
import com.rni.rni.domain.dao.DimensionValue;
import com.rni.rni.domain.dao.UserLogin;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface UserLoginRepository extends JpaRepository<UserLogin, Long>, JpaSpecificationExecutor<UserLogin> {
    List<UserLogin> findByUsernameContainingIgnoreCaseOrNameContainingIgnoreCase(String username,String name);
    List<UserLogin> findByCabang(Dimension dimension);
    List<UserLogin> findByDivisi(DimensionValue dimensionValue);
}
