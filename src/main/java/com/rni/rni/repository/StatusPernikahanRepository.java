package com.rni.rni.repository;

import com.rni.rni.domain.dao.StatusPernikahan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StatusPernikahanRepository extends JpaRepository<StatusPernikahan,Integer> {
    StatusPernikahan findByCode(String dataCol);
}
