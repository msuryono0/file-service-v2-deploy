package com.rni.rni.repository;

import com.rni.rni.domain.dao.MasterEntryHcOmzet;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MasterHcOmzetRepository extends JpaRepository<MasterEntryHcOmzet,Long> {

}
