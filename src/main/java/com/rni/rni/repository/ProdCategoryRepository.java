package com.rni.rni.repository;

import com.rni.rni.domain.dao.ProdCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProdCategoryRepository extends JpaRepository<ProdCategory, Integer> {
    ProdCategory findByCode (String code);
}
