package com.rni.rni.repository;

import com.rni.rni.domain.dao.TransactionEntryLdOmzet;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TransactionEntryLdOmzetRepository extends JpaRepository<TransactionEntryLdOmzet,Long> {

    List<TransactionEntryLdOmzet> findByYearAndPeriodeAndCabangDivisi(String tahun, String periode, String unit);
}
