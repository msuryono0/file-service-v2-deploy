package com.rni.rni.repository;

import com.rni.rni.domain.dao.TransactionEntryDistShare;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TransactionEntryDistSahreRepository extends JpaRepository<TransactionEntryDistShare,Long> {
}
