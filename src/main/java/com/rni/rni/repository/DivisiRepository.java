package com.rni.rni.repository;

import com.rni.rni.domain.dao.Divisi;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DivisiRepository extends JpaRepository<Divisi,Long> {
    @Query(value = "select d from Divisi d where d.code like %:search% or d.keterangan like %:search%")
    List<Divisi> findDivisi(@Param("search") String search);
}
