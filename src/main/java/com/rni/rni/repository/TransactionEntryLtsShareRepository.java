package com.rni.rni.repository;

import com.rni.rni.domain.dao.TransactionEntryLtsShare;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TransactionEntryLtsShareRepository extends JpaRepository<TransactionEntryLtsShare,Long> {

}
