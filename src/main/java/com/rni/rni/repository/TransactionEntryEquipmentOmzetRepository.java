package com.rni.rni.repository;

import com.rni.rni.domain.dao.TransactionEntryEquipmentOmzet;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TransactionEntryEquipmentOmzetRepository extends JpaRepository<TransactionEntryEquipmentOmzet,Long> {

}
