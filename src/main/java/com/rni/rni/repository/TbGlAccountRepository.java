package com.rni.rni.repository;

import com.rni.rni.domain.dao.TbGlAccount;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TbGlAccountRepository extends JpaRepository<TbGlAccount, Integer> {
    TbGlAccount findByAccountCode(String kodeRek);
}
