package com.rni.rni.repository;

import com.rni.rni.domain.dao.TransactionEntryServiceShare;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TransactionEntryServiceShareRepository extends JpaRepository<TransactionEntryServiceShare,Long> {

}
