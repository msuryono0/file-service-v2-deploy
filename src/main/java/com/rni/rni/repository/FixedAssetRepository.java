package com.rni.rni.repository;

import com.rni.rni.domain.dao.FixedAsset;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FixedAssetRepository extends JpaRepository<FixedAsset,Long> {
    FixedAsset findByCode(String kode);
}
