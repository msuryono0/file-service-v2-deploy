package com.rni.rni.repository;

import com.rni.rni.domain.dao.TransactionEntryHcOmzet;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TransactionEntryHcOmzetRepository extends JpaRepository<TransactionEntryHcOmzet,Long> {

}
