package com.rni.rni.repository;

import com.rni.rni.domain.dao.FileUploaders;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FileUploadersRepoistory extends JpaRepository<FileUploaders, Long> {
}
