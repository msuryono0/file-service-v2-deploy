package com.rni.rni.repository;

import com.rni.rni.domain.dao.ClassProd;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClassProdRepository extends JpaRepository<ClassProd,Long> {
}
