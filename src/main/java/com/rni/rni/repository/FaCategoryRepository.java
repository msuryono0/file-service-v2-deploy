package com.rni.rni.repository;

import com.rni.rni.domain.dao.FaCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FaCategoryRepository extends JpaRepository<FaCategory, Long> {
    FaCategory findByKeterangan(String keterangan);

    FaCategory findByCode(String search);
}
