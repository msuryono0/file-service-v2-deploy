package com.rni.rni.repository;

import com.rni.rni.domain.dao.FaSubClass;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FaSubClassRepository extends JpaRepository<FaSubClass, Integer> {
     FaSubClass findByKeterangan(String faSubclass);
}
