package com.rni.rni.repository;

import com.rni.rni.domain.dao.TransactionEntryTradingShare;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TransactionEntryTradingShareRepository extends JpaRepository<TransactionEntryTradingShare,Long> {

}
