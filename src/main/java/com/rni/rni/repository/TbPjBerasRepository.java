package com.rni.rni.repository;

import com.rni.rni.domain.dao.TbPjBeras;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TbPjBerasRepository extends JpaRepository<TbPjBeras,Long> {

    List<TbPjBeras> findByIdKaryawan_NamaContainingIgnoreCase(String search);
}
