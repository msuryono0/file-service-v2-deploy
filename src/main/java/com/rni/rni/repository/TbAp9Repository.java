package com.rni.rni.repository;

import com.rni.rni.domain.dao.AP9;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TbAp9Repository extends JpaRepository<AP9,Long> {
}
