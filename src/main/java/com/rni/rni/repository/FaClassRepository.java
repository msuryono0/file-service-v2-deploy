package com.rni.rni.repository;

import com.rni.rni.domain.dao.FaClass;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FaClassRepository extends JpaRepository<FaClass, Integer> {
    FaClass findByKeterangan(String faClass);
}
