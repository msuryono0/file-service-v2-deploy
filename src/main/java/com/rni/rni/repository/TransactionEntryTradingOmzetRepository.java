package com.rni.rni.repository;

import com.rni.rni.domain.dao.TransactionEntryTradingOmzet;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TransactionEntryTradingOmzetRepository extends JpaRepository<TransactionEntryTradingOmzet,Long> {

    List<TransactionEntryTradingOmzet> findByYearAndPeriodeAndCabangDivisi(String tahun, String periode, String unit);
}
