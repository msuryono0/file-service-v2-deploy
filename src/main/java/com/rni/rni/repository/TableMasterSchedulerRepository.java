package com.rni.rni.repository;

import com.rni.rni.domain.dao.TableMasterScheduler;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TableMasterSchedulerRepository extends JpaRepository<TableMasterScheduler,Long> {
}
