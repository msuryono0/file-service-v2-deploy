package com.rni.rni.utils;

import com.rni.rni.domain.dao.FileUploaders;
import com.rni.rni.repository.FileUploadersRepoistory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.Date;

@Slf4j
@Service
public class InsertLogMonitoring {
    @Autowired
    private FileUploadersRepoistory fileUploadersRepoistory;
    public Long logMonitoring(Long id,String sheetName,String status,Long size,Long totalRow){
        FileUploaders fileUploaders = new FileUploaders();

        if (ObjectUtils.isEmpty(id)){
            fileUploaders.setFileName(sheetName);
            fileUploaders.setStatusUpload(status);
            fileUploaders.setCreatedAt(new Date());
            fileUploaders.setFeature(sheetName);
            fileUploaders.setSize(size);
            fileUploaders.setTotalRows(totalRow);
        }else{
            fileUploaders =fileUploadersRepoistory.findById(id).get();
            fileUploaders.setUpdatedAt(new Date());
            fileUploaders.setStatusUpload(status);

        }
        var data = fileUploadersRepoistory.save(fileUploaders);
        return data.getId();
    }
}
