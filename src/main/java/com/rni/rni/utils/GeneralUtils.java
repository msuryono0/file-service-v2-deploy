package com.rni.rni.utils;

import java.util.Random;

public class GeneralUtils {


    public static String code(String code) {
        Random random = new Random();
        int length = 5;
        char[] digits = new char[length];
        digits[0] = (char) (random.nextInt(9) + '1');
        for (int i = 1; i < length; i++) {
            digits[i] = (char) (random.nextInt(10) + '0');
        }
        return code.concat(String.valueOf(Long.parseLong(new String(digits))));
    }

}
