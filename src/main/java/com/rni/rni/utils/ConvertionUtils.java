package com.rni.rni.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class ConvertionUtils {

    public static  String convertToDate(Date date){
        String pattern = "dd/MM/yyyy, HH : mm : ss";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        return simpleDateFormat.format(date);
    }

    public static Boolean integerToBoolean(Integer integer) {
        return (integer != null) ? (integer.intValue() == 1) ? Boolean.TRUE : Boolean.FALSE : Boolean.FALSE;
    }
    public static Date convertStringtoDate(String val) throws ParseException {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd");
        Date date = dateFormat.parse(val);
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, -1);
        return cal.getTime();
    }

    public static List<String> convertStringToArray(String str) {
        String[] arrays = str.split("\\;");
        System.out.println
                (java.util.Arrays.toString(arrays));
        String lastCode = arrays[arrays.length - 1];
        String subsTring = lastCode.substring(1,lastCode.length());
        Integer number = Integer.parseInt(subsTring);

        List<String> a = new ArrayList<>();
        for (int i = 0; i < number; i++) {
            a.add(String.format("A%03d", i + 1));
        }
        return a;
    }

    public static Long nullToLong(Object dataNull) {
        if (dataNull == null) return  new Long(0);
        return (Long) dataNull;
    }
}
