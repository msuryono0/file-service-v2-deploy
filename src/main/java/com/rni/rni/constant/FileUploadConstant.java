package com.rni.rni.constant;

public class FileUploadConstant {

    public FileUploadConstant() {
    }

    public static final String FA_CATEGORY = "01";
    public static final String PRODUCT = "02";
    public static final String EMPLOYEE = "03";
    public static final String SUPPLIER = "04";
    public static final String PROD_GROUP = "05";
    public static final String PROD_CLASS = "06";
    public static final String PROD_TYPE = "07";
    public static final String DIVISI = "08";
    public static final String JABATAN = "09";
    public static final String GOLONGAN = "10";
    public static final String STATUS_KARYAWAN = "11";
    public static final String GAJI_KARYAWAN = "12";
    public static final String FA_CLASS = "13";
    public static final String FA_SUB_CLASS = "14";
    public static final String PROD_CATEGORY = "15";
    public static final String DIMENSION = "16";
    public static final String DIMENSION_VALUE = "17";
    public static final String GL_ACCOUNT = "18";
    public static final String FIXED_ASSET = "19";
    public static final String KK9 = "20";
    public static final String STRUKTURAL = "21";
    public static final String PJ_BERAS = "22";
    public static final String AP9 = "23";
    public static final String TRANSACTION_ENTRY_HC_OMZET = "24";
    public static final String TRANSACTION_ENTRY_HC_SHARE = "25";
    public static final String TRANSACTION_ENTRY_SURGERY_SHARE = "26";

}
