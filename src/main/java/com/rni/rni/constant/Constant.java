package com.rni.rni.constant;

public class Constant {

    Constant(){

    }
    public static class responseKey{
        responseKey(){}
        public static final String SUCCESS = "SUCCESS";
        public static final String INQUIRY_SUCCESS = "INQUIRY SUCCESS";
        public static final String SAVE_SUCCESS = "SAVE SUCCESS";
        public static final String DELETE_SUCCESS = "DELETE SUCCESS";
        public static final String DELETE_FAILED = "DELETE FAILED";
        public static final String SAVE_FAILED = "SAVE FAILED";
        public static final String INQUIRY_FAILED = "INQUIRY FAILED";
        public static final String FAILED = "FAILED";
        public static final String UPLOADING = "UPLOADING DATA";

    }

    public static class redisKey{
        redisKey(){}
        public static final String KEY_COA ="COAHEADER";
        public static final String KEY_ASSET = "FIXEDASSET";
        public static final String KEY_USERLOGIN ="USERLOGIN";
        public static final String KEY_CABANG ="CABANG";
        public static final String KEY_UNIT ="UNIT";
        public static final String KEY_UOM ="UOM";
        public static final String KEY_PERMISSION ="PERMISSION";
        public static final String KEY_FA_CATEGORY = "FA_CATEGORY";
        public static final String KEY_FA_CLASS = "FA_CLASS";
        public static final String KEY_FA_SUB_CLASS = "FA_SUB_CLASS";
        public static final String KEY_PRODUCT ="PRODUCT";
        public static final String KEY_EMPLOYEE ="EMPLOYEE";
        public static final String KEY_DIVISI ="DIVISI";
        public static final String KEY_JABATAN ="JABATAN";
        public static final String KEY_GOLONGAN ="GOLONGAN";
        public static final String KEY_GAJI ="GAJI_KARYAWAN";
        public static final String KEY_STATUS_KARYAWAN ="STATUS_KARYAWAN";
        public static final String KEY_SPPD ="SPPD";
        public static final String KEY_SPPD_LOCAL ="SPPD_LOCAL";
        public static final String KEY_KK9 = "KK_9";
        public static final String KEY_BERAS ="BERAS";
        public static final String KEY_AP9 ="AP_9";

    }
    public static class chanelKey{
        chanelKey(){}
        public static final String CN_KK9 = "KK9";
        public static final String CN_AP9 = "AP9";
        public static final String CN_TRAN_HC_OMZET_MASTER = "TRANSACTION_HC_OMZET_MASTER";
        public static final String CN_TRAN_HC_OMZET = "TRANSACTION_HC_OMZET";
    }
}
