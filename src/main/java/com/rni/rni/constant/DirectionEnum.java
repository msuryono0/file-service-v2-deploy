package com.rni.rni.constant;

import com.rni.rni.domain.dto.FilterSortDto;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Root;

public enum DirectionEnum {
    ASC {
        public <T> Order build(Root<T> root, CriteriaBuilder cb, FilterSortDto request) {
            return cb.asc(root.get(request.getKey()));
        }
    },
    DESC {
        public <T> Order build(Root<T> root, CriteriaBuilder cb, FilterSortDto request) {
            return cb.desc(root.get(request.getKey()));
        }
    };

    public abstract <T> Order build(Root<T> root, CriteriaBuilder cb, FilterSortDto request);

}
