package com.rni.rni.controler;

import com.rni.rni.constant.FileUploadConstant;
import com.rni.rni.domain.dao.*;
import com.rni.rni.domain.dto.CommonRequestDto;
import com.rni.rni.domain.dto.Metadata;
import com.rni.rni.service.FileUploadService;
import com.rni.rni.service.TransaksiPusatService;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.xml.sax.SAXException;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.text.ParseException;

@Api
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@Slf4j
public class TransactionBranchController extends AbstractBaseController {

    @Autowired
    private TransaksiPusatService transaksiPusatService;

    @Autowired
    FileUploadService fileUploadService;

    @ResponseBody
    @PostMapping(value = "/upload/transaksi-kantor-pusat/kk")
    public ResponseEntity<Object> responseKk9(@RequestParam("file") MultipartFile file,
                                           @RequestParam() String cabang, String divisi, String periode, String year,
                                           HttpServletRequest servletRequest) throws IOException {
        Metadata metadata = constructMetadata(servletRequest);
         return transaksiPusatService.kk9(file, cabang, divisi, periode, year, metadata);
    }

    @ResponseBody
    @PostMapping(value = "/upload/pj-beras")
    public ResponseEntity<Object> responseBeras(@RequestParam("file") MultipartFile file,
                                           @RequestParam() String year, String periode,
                                           HttpServletRequest servletRequest) throws IOException, ParseException, OpenXML4JException, SAXException {
        Metadata metadata = constructMetadata(servletRequest);
        return fileUploadService.handleUploadTransaksi(file, year, periode, metadata, FileUploadConstant.PJ_BERAS);
    }
    @ResponseBody
    @PostMapping(value = "/upload/ap9")
    public ResponseEntity<Object> responseAP9(@RequestParam("file") MultipartFile file,
                                                @RequestParam() String year, String periode,
                                                HttpServletRequest servletRequest) throws IOException, ParseException, OpenXML4JException, SAXException {
        Metadata metadata = constructMetadata(servletRequest);
        return fileUploadService.handleUploadTransaksi(file,periode, year, metadata, FileUploadConstant.AP9);
    }

    @ResponseBody
    @PostMapping(value = "/upload/pj-seragam")
    public ResponseEntity<Object> responseSeragam(@RequestParam("file") MultipartFile file,
                                                @RequestParam() String year,
                                                HttpServletRequest servletRequest) throws IOException, ParseException {
        Metadata metadata = constructMetadata(servletRequest);
        return transaksiPusatService.seragam(file, year, metadata);
    }

    @ResponseBody
    @PostMapping(value = "/upload/transaction-hc-share")
    public ResponseEntity<Object> responseHc(@RequestParam("file") MultipartFile file,
                                                  @RequestParam() String year, String periode,
                                                  HttpServletRequest servletRequest) throws IOException, ParseException, OpenXML4JException, SAXException {
        Metadata metadata = constructMetadata(servletRequest);
        return fileUploadService.handleUploadTransaksi(file, year, periode,metadata, FileUploadConstant.TRANSACTION_ENTRY_HC_SHARE);
    }

    @ResponseBody
    @PostMapping(value = "/upload/transaction-hc-omzet")
    public ResponseEntity<Object> responseTransactionOmzet(@RequestParam("file") MultipartFile file,
                                             @RequestParam() String year, String periode,
                                             HttpServletRequest servletRequest) throws IOException, ParseException, OpenXML4JException, SAXException {
        Metadata metadata = constructMetadata(servletRequest);
        return fileUploadService.handleUploadTransaksi(file, year, periode,metadata, FileUploadConstant.TRANSACTION_ENTRY_HC_OMZET);
    }

    // TODO: 8/29/2022  rendika get transaction_entry_dist_omzet


    @ResponseBody
    @PostMapping(value = "/transaction/entry-dist-omzet/save")
    public ResponseEntity<Object> saveDataEntryDistOmzet(@RequestBody TransactionEntryDistOmzet request){
        return transaksiPusatService.saveDataEntryDistOmzet(request);
    }

    @ResponseBody
    @PostMapping(value = "/transaction/entry-dist-omzet/get")
    public ResponseEntity<Object> getDataEntryDistOmzet(@RequestBody CommonRequestDto request,HttpServletRequest httpServletRequest){
        Metadata metadata = constructMetadata(httpServletRequest);
        return transaksiPusatService.getDataEntryDistOmzet(request,metadata);
    }

    @ResponseBody
    @PostMapping(value = "/transaction/entry-equipment-omzet/save")
    public ResponseEntity<Object> saveDataEntryEquipmentOmzet(@RequestBody TransactionEntryEquipmentOmzet request){
        return transaksiPusatService.saveDataEntryEquipmentOmzet(request);
    }

    @ResponseBody
    @PostMapping(value = "/transaction/entry-hc-omzet/save")
    public ResponseEntity<Object> saveDataEntryHcOmzet(@RequestBody TransactionEntryHcOmzet request){
        return transaksiPusatService.saveDataEntryHcOmzet(request);
    }

    @ResponseBody
    @PostMapping(value = "/transaction/entry-ld-omzet/save")
    public ResponseEntity<Object> saveDataEntryLdOmzet(@RequestBody TransactionEntryLdOmzet request){
        return transaksiPusatService.saveDataEntryLdOmzet(request);
    }

    @ResponseBody
    @PostMapping(value = "/transaction/entry-lts-omzet/save")
    public ResponseEntity<Object> saveDataEntryLtsOmzet(@RequestBody TransactionEntryLtsOmzet request){
        return transaksiPusatService.saveDataEntryLtsOmzet(request);
    }

    @ResponseBody
    @PostMapping(value = "/transaction/entry-medical-omzet/save")
    public ResponseEntity<Object> saveDataEntryMedicalOmzet(@RequestBody TransactionEntryMedicalOmzet request){
        return transaksiPusatService.saveDataEntryMedicalOmzet(request);
    }

    @ResponseBody
    @PostMapping(value = "/transaction/entry-reg-omzet/save")
    public ResponseEntity<Object> saveDataEntryRegOmzet(@RequestBody TransactionEntryRegOmzet request){
        return transaksiPusatService.saveDataEntryRegOmzet(request);
    }

    @ResponseBody
    @PostMapping(value = "/transaction/entry-service-omzet/save")
    public ResponseEntity<Object> saveDataEntryServiceOmzet(@RequestBody TransactionEntryServiceOmzet request){
        return transaksiPusatService.saveDataEntryServiceOmzet(request);
    }

    @ResponseBody
    @PostMapping(value = "/transaction/entry-surgery-omzet/save")
    public ResponseEntity<Object> saveDataEntrySurgeryOmzet(@RequestBody TransactionEntrySurgeryOmzet request){
        return transaksiPusatService.saveDataEntrySurgeryOmzet(request);
    }

    @ResponseBody
    @PostMapping(value = "/transaction/entry-tender-omzet/save")
    public ResponseEntity<Object> saveDataEntryTenderOmzet(@RequestBody TransactionEntryTenderOmzet request){
        return transaksiPusatService.saveDataEntryTenderOmzet(request);
    }

    @ResponseBody
    @PostMapping(value = "/transaction/entry-trading-omzet/save")
    public ResponseEntity<Object> saveDataEntryTradingOmzet(@RequestBody TransactionEntryTradingOmzet request){
        return transaksiPusatService.saveDataEntryTradingOmzet(request);
    }

    @ResponseBody
    @PostMapping(value = "/transaction/dist-share/save")
    public ResponseEntity<Object> saveDataEntryDistShare(@RequestBody TransactionEntryDistShare request){
        return transaksiPusatService.saveDataEntryDistShare(request);
    }

    @ResponseBody
    @PostMapping(value = "/transaction/equipment-share/save")
    public ResponseEntity<Object> saveDataEntryDistShare(@RequestBody TransactionEntryEquipmentShare request){
        return transaksiPusatService.saveDataEntryEquipmentShare(request);
    }

    @ResponseBody
    @PostMapping(value = "/transaction/hc-share/save")
    public ResponseEntity<Object> saveDataEntryHcShare(@RequestBody TransactionEntryHcShare request){
        return transaksiPusatService.saveDataEntryHcShare(request);
    }

    @ResponseBody
    @PostMapping(value = "/transaction/ld-share/save")
    public ResponseEntity<Object> saveDataEntryLdShare(@RequestBody TransactionEntryLdShare request){
        return transaksiPusatService.saveDataEntryLdShare(request);
    }

    @ResponseBody
    @PostMapping(value = "/transaction/lts-share/save")
    public ResponseEntity<Object> saveDataEntryLtsShare(@RequestBody TransactionEntryLtsShare request){
        return transaksiPusatService.saveDataEntryLtsShare(request);
    }

    @ResponseBody
    @PostMapping(value = "/transaction/medical-share/save")
    public ResponseEntity<Object> saveDataEntryLtsShare(@RequestBody TransactionEntryMedicalShare request){
        return transaksiPusatService.saveDataEntryMedicalShare(request);
    }

    @ResponseBody
    @PostMapping(value = "/transaction/reg-share/save")
    public ResponseEntity<Object> saveDataEntryRegShare(@RequestBody TransactionEntryRegShare request){
        return transaksiPusatService.saveDataEntryRegShare(request);
    }

    @ResponseBody
    @PostMapping(value = "/transaction/service-share/save")
    public ResponseEntity<Object> saveDataEntryServiceShare(@RequestBody TransactionEntryServiceShare request){
        return transaksiPusatService.saveDataEntryServiceShare(request);
    }

    @ResponseBody
    @PostMapping(value = "/transaction/surgery-share/save")
    public ResponseEntity<Object> saveDataEntrySurgeryShare(@RequestBody TransactionEntrySurgeryShare request){
        return transaksiPusatService.saveDataEntrySurgeryShare(request);
    }

    @ResponseBody
    @PostMapping(value = "/transaction/tender-share/save")
    public ResponseEntity<Object> saveDataEntryTenderShare(@RequestBody TransactionEntryTenderShare request){
        return transaksiPusatService.saveDataEntryTenderShare(request);
    }

    @ResponseBody
    @PostMapping(value = "/transaction/trading-share/save")
    public ResponseEntity<Object> saveDataEntryTradingShare(@RequestBody TransactionEntryTradingShare request){
        return transaksiPusatService.saveDataEntryTradingShare(request);
    }
    @ResponseBody
    @PostMapping(value = "/transaction/trading-omzet/get")
    public ResponseEntity<Object> getDataEntryTradingOmzet(@RequestBody CommonRequestDto request,HttpServletRequest meta){
        Metadata metadata = constructMetadata(meta);
        return transaksiPusatService.getDatanEntryTradingOmzet(request,metadata);
    }
    @ResponseBody
    @PostMapping(value = "/transaction/tender-omzet/get")
    public ResponseEntity<Object> getDataEntryTenderOmzet(@RequestBody CommonRequestDto request,HttpServletRequest meta){
        Metadata metadata = constructMetadata(meta);
        return transaksiPusatService.getDatanEntryTenderOmzet(request,metadata);
    }
    @ResponseBody
    @PostMapping(value = "/transaction/service-omzet/get")
    public ResponseEntity<Object> getDataEntryServiceOmzet(@RequestBody CommonRequestDto request,HttpServletRequest meta){
        Metadata metadata = constructMetadata(meta);
        return transaksiPusatService.getDataEntryServiceOmzet(request,metadata);
    }
    @ResponseBody
    @PostMapping(value = "/transaction/reg-omzet/get")
    public ResponseEntity<Object> getDataEntryRegOmzet(@RequestBody CommonRequestDto request,HttpServletRequest meta){
        Metadata metadata = constructMetadata(meta);
        return transaksiPusatService.getDataEntryRegOmzet(request,metadata);
    }
    @ResponseBody
    @PostMapping(value = "/transaction/Lts-omzet/get")
    public ResponseEntity<Object> getDataEntryLtsOmzet(@RequestBody CommonRequestDto request,HttpServletRequest meta){
        Metadata metadata = constructMetadata(meta);
        return transaksiPusatService.getDataEntryLtsOmzet(request,metadata);
    }

}
