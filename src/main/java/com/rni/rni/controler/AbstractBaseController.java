package com.rni.rni.controler;

import com.rni.rni.domain.dto.Metadata;

import javax.servlet.http.HttpServletRequest;

public class AbstractBaseController {

    private static final String CABANG_CATEGORY = "cabang";
    private static final String UNIT = "unit";
    private static final String BRANCH = "branch";
    private static final String USERNAME = "username";

    protected Metadata constructMetadata(HttpServletRequest request) {
        return Metadata.builder()
                .cabangCategory(request.getHeader(CABANG_CATEGORY))
                .unit(request.getHeader(UNIT))
                .branch(request.getHeader(BRANCH))
                .username(request.getHeader(USERNAME))
                .build();
    }

}