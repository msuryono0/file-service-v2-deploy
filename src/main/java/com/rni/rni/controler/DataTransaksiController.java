package com.rni.rni.controler;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.rni.rni.domain.dto.CoaRequestDto;
import com.rni.rni.domain.dto.CommonRequestDto;
import com.rni.rni.domain.dto.Metadata;
import com.rni.rni.domain.dto.TransactionHCOmzetDto;
import com.rni.rni.service.DataTransaksiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
public class DataTransaksiController extends AbstractBaseController{

    @Autowired
    private DataTransaksiService dataTransaksiService;

    @ResponseBody
    @PostMapping(value = "/transaksipusat/pjberas/get")
    public ResponseEntity<Object> getBeras(@RequestBody CoaRequestDto requestDto,
                                           HttpServletRequest servletRequest) throws JsonProcessingException {
        Metadata metadata = constructMetadata(servletRequest);
        return dataTransaksiService.getBeras(requestDto, metadata);
    }
    @ResponseBody
    @PostMapping(value = "/transaksipusat/ap9/get")
    public ResponseEntity<Object> getAp9(@RequestBody CommonRequestDto requestDto,
                                           HttpServletRequest servletRequest) throws JsonProcessingException {
        Metadata metadata = constructMetadata(servletRequest);
        return dataTransaksiService.getAp9(requestDto, metadata);
    }

    @ResponseBody
    @PostMapping(value = "/omzet-hc/get")
    public ResponseEntity<Object> getOmzet(@RequestBody CommonRequestDto requestDto,
                                         HttpServletRequest servletRequest) throws JsonProcessingException {
        Metadata metadata = constructMetadata(servletRequest);
        return dataTransaksiService.getOmzetHc(requestDto, metadata);
    }

    @ResponseBody
    @PostMapping(value = "/omzet-hc/master")
    public ResponseEntity<Object> getOmzetMasterHc(@RequestBody CommonRequestDto requestDto,
                                           HttpServletRequest servletRequest) throws JsonProcessingException {
        Metadata metadata = constructMetadata(servletRequest);
        return dataTransaksiService.getOmzetMasterHc();
    }
    @ResponseBody
    @PostMapping(value = "/omzet-hc/save")
    public ResponseEntity<Object> saveOmzet(@RequestBody CommonRequestDto requestDto,
                                                 HttpServletRequest servletRequest) throws JsonProcessingException {
        Metadata metadata = constructMetadata(servletRequest);
        return dataTransaksiService.getOmzetHc(requestDto, metadata);
    }

    @ResponseBody
    @PostMapping(value = "/omzet-hc/change/row")
    public ResponseEntity<Object> updateHcOmzet(@RequestBody TransactionHCOmzetDto requestDto) throws JsonProcessingException {
        return dataTransaksiService.updateRowHcOmzet(requestDto);
    }
}
