package com.rni.rni.controler;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.rni.rni.domain.dto.*;
import com.rni.rni.service.DataMasterService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
@Api
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
public class DataMasterController extends AbstractBaseController {

    @Autowired
    private DataMasterService dataMasterService;

    @ApiOperation("get data coa")
    @PostMapping(value = "/coa/get")
    public ResponseEntity<Object> response(@RequestBody CoaRequestDto requestDto, HttpServletRequest servletRequest) throws JsonProcessingException {
        Metadata metadata = constructMetadata(servletRequest);
        return dataMasterService.getAllCoa(requestDto, metadata);
    }

    @ApiOperation("save/edit coa")
    @PostMapping(value = "/coa/save")
    public ResponseEntity<Object> response (@RequestBody CommonSaveDataDto commonSaveDataDto, HttpServletRequest servletRequest){
        Metadata metadata = constructMetadata(servletRequest);
        return dataMasterService.saveDataCoa(commonSaveDataDto, metadata);
    }

    @ApiOperation("delete coa")
    @PostMapping(value = "/coa/delete")
    public ResponseEntity<Object> response (@RequestBody CommonDeleteData dataCoaDto, HttpServletRequest servletRequest){
        Metadata metadata = constructMetadata(servletRequest);
        return dataMasterService.deleteDataCod(dataCoaDto, metadata);
    }

    @ApiOperation("get all fixed asset")
    @PostMapping(value = "/asset/get")
    public ResponseEntity<Object> getFixAsset (@RequestBody CoaRequestDto req, HttpServletRequest servletRequest) throws JsonProcessingException {
        Metadata metadata = constructMetadata(servletRequest);
        return dataMasterService.getDataFixedAsset(req, metadata);
    }

    @ApiOperation("save-edit data fixed asset")
    @PostMapping(value = "/asset/save")
    public ResponseEntity<Object> saveFixAsset (@RequestBody FixAssetSaveDataDto fixAssetSaveDataDto, HttpServletRequest servletRequest) throws JsonProcessingException {
        Metadata metadata = constructMetadata(servletRequest);
        return dataMasterService.saveDataFixAsset(fixAssetSaveDataDto, metadata);
    }

    @ApiOperation("delete asset")
    @PostMapping(value = "/asset/delete")
    public ResponseEntity<Object> deleteAsset (@RequestBody CommonDeleteData req, HttpServletRequest servletRequest) throws JsonProcessingException {
        Metadata metadata = constructMetadata(servletRequest);
        return dataMasterService.deleteFixedAsset(req, metadata);
    }

    @ApiOperation("get data fa_category")
    @PostMapping(value = "/fa-category/get")
    public ResponseEntity<Object> getFaCategory (@RequestBody CoaRequestDto req, HttpServletRequest servletRequest) throws JsonProcessingException {
        Metadata metadata = constructMetadata(servletRequest);
        return dataMasterService.getdataFaCategory(req, metadata);
    }

    @ApiOperation("save data fa_category")
    @PostMapping(value = "/fa-category/save")
    public ResponseEntity<Object> saveFaCategory (@RequestBody CommonSaveDataDto req, HttpServletRequest servletRequest) throws JsonProcessingException {
        Metadata metadata = constructMetadata(servletRequest);
        return dataMasterService.saveDataFaCategory(req, metadata);
    }

    @ApiOperation("delete-data fa category")
    @PostMapping(value = "/fa-category/delete")
    public ResponseEntity<Object> deleteFaCategory (@RequestBody CommonDeleteData req, HttpServletRequest servletRequest) throws JsonProcessingException {
        Metadata metadata = constructMetadata(servletRequest);
        return dataMasterService.deleteDataFaCategory(req, metadata);
    }

    @ApiOperation("get data fa_class")
    @PostMapping(value = "/fa-class/get")
    public ResponseEntity<Object> getFaClass (@RequestBody CoaRequestDto req, HttpServletRequest servletRequest) throws JsonProcessingException {
        Metadata metadata = constructMetadata(servletRequest);
        return dataMasterService.getDataFaClass(req, metadata);
    }

    @ApiOperation("save data fa_class")
    @PostMapping(value = "/fa-class/save")
    public ResponseEntity<Object> saveFaClass (@RequestBody CommonSaveDataDto req, HttpServletRequest servletRequest) throws JsonProcessingException {
        Metadata metadata = constructMetadata(servletRequest);
        return dataMasterService.saveDataFaClass(req, metadata);
    }

    @ApiOperation("delete-data fa_class")
    @PostMapping(value = "/fa-class/delete")
    public ResponseEntity<Object> deleteFaClass (@RequestBody CommonDeleteData req, HttpServletRequest servletRequest) throws JsonProcessingException {
        Metadata metadata = constructMetadata(servletRequest);
        return dataMasterService.deleteFaClass(req, metadata);
    }

    @ApiOperation("get data fa_subclass")
    @PostMapping(value = "/fa-subclass/get")
    public ResponseEntity<Object> getFaSubClass (@RequestBody CoaRequestDto req, HttpServletRequest servletRequest) throws JsonProcessingException {
        Metadata metadata = constructMetadata(servletRequest);
        return dataMasterService.getDataFaSubClass(req, metadata);
    }

    @ApiOperation("save data fa_subclass")
    @PostMapping(value = "/fa-subclass/save")
    public ResponseEntity<Object> saveFaSubClass (@RequestBody CommonSaveDataDto req, HttpServletRequest servletRequest) throws JsonProcessingException {
        Metadata metadata = constructMetadata(servletRequest);
        return dataMasterService.saveDataFaSubClass(req, metadata);
    }

    @ApiOperation("delete-data fa_subclasss")
    @PostMapping(value = "/fa-subclass/delete")
    public ResponseEntity<Object> deleteFaSubClass (@RequestBody CommonDeleteData req, HttpServletRequest servletRequest) throws JsonProcessingException {
        Metadata metadata = constructMetadata(servletRequest);
        return dataMasterService.deleteFaSubClass(req, metadata);
    }

    @ApiOperation("Get Product")
    @PostMapping(value = "/product/get")
    public ResponseEntity<Object> getProduct (@RequestBody CommonRequestDto request, HttpServletRequest servletRequest) throws JsonProcessingException {
        Metadata metadata = constructMetadata(servletRequest);
        return dataMasterService.getProduct(request, metadata);
    }

    @ApiOperation("Save Product")
    @PostMapping(value = "/product/save")
    public ResponseEntity<Object> saveProduct (@RequestBody ProductRequestDto request, HttpServletRequest servletRequest) throws JsonProcessingException {
        Metadata metadata = constructMetadata(servletRequest);
        return dataMasterService.saveProduct(request, metadata);
    }
    @ApiOperation("Delete Product")
    @PostMapping(value = "/product/delete")
    public ResponseEntity<Object> deleteProduct (@RequestBody ProductRequestDto request, HttpServletRequest servletRequest) throws JsonProcessingException {
        Metadata metadata = constructMetadata(servletRequest);
        return dataMasterService.deleteProduct(request, metadata);
    }

    @ApiOperation("Get Employee")
    @PostMapping(value = "/employee/get")
    public ResponseEntity<Object> getEmployee (@RequestBody CommonRequestDto request, HttpServletRequest servletRequest) throws JsonProcessingException {
        Metadata metadata = constructMetadata(servletRequest);
        return dataMasterService.getEmployee(request, metadata);
    }
    @ApiOperation("Get Divisi")
    @PostMapping(value = "/divisi/get")
    public ResponseEntity<Object> getDivisi (@RequestBody CommonRequestDto request, HttpServletRequest servletRequest) throws JsonProcessingException {
        Metadata metadata = constructMetadata(servletRequest);
        return dataMasterService.getDivisi(request, metadata);
    }
    @ApiOperation("Get Golongan")
    @PostMapping(value = "/golongan/get")
    public ResponseEntity<Object> getGolongan (@RequestBody CommonRequestDto request, HttpServletRequest servletRequest) throws JsonProcessingException {
        Metadata metadata = constructMetadata(servletRequest);
        return dataMasterService.getGolongan(request, metadata);
    }
    @ApiOperation("Get Jabatan")
    @PostMapping(value = "/jabatan/get")
    public ResponseEntity<Object> getjabatan (@RequestBody CommonRequestDto request, HttpServletRequest servletRequest) throws JsonProcessingException {
        Metadata metadata = constructMetadata(servletRequest);
        return dataMasterService.getJabatan(request, metadata);
    }
    @ApiOperation("Get Status Karyawan")
    @PostMapping(value = "/statuskaryawan/get")
    public ResponseEntity<Object> getStatusKaryawan (@RequestBody CommonRequestDto request, HttpServletRequest servletRequest) throws JsonProcessingException {
        Metadata metadata = constructMetadata(servletRequest);
        return dataMasterService.getStatusKaryawan(request, metadata);
    }
    @ApiOperation("Get Gaji Karyawan")
    @PostMapping(value = "/gajikaryawan/get")
    public ResponseEntity<Object> getGajiKaryawan (@RequestBody GajiRequestDto request, HttpServletRequest servletRequest) throws JsonProcessingException {
        Metadata metadata = constructMetadata(servletRequest);
        return dataMasterService.getGaji(request, metadata);
    }
    @ApiOperation("Get Perjalanan Dinas")
    @PostMapping(value = "/sppd/get")
    public ResponseEntity<Object> getPerjalananDinas (@RequestBody CommonRequestDto request, HttpServletRequest servletRequest) throws JsonProcessingException {
        Metadata metadata = constructMetadata(servletRequest);
        return dataMasterService.getPerjalananDinas(request, metadata);
    }
    @ApiOperation("Get Perjalanan Dinas Local")
    @PostMapping(value = "/sppdlocal/get")
    public ResponseEntity<Object> getPerjalananDinasLocal (@RequestBody CommonRequestDto request, HttpServletRequest servletRequest) throws JsonProcessingException {
        Metadata metadata = constructMetadata(servletRequest);
        return dataMasterService.getPerjalananDinasLocal(request, metadata);
    }
}
