package com.rni.rni.controler;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.rni.rni.domain.dto.*;
import com.rni.rni.service.DataSetupService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@Api
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
public class DataSetupController extends AbstractBaseController {

    @Autowired
    private DataSetupService dataSetupService;

    @ApiOperation("Get User Login")
    @PostMapping(value = "/userlogin/get")
    public ResponseEntity<Object> getUserLogin(@RequestBody CommonRequestDto request, HttpServletRequest servletRequest) throws JsonProcessingException {
        Metadata metadata = constructMetadata(servletRequest);
        return dataSetupService.getUserLogin(request,metadata);
    }
    @ApiOperation("Save User Login")
    @PostMapping(value = "/userlogin/save")
    public ResponseEntity<Object> saveUserLogin(@RequestBody UserLoginRequestDto request, HttpServletRequest servletRequest) {
        Metadata metadata = constructMetadata(servletRequest);
        return dataSetupService.saveUserLogin(request,metadata);
    }
    @ApiOperation("Delete User Login")
    @PostMapping(value = "/userlogin/delete")
    public ResponseEntity<Object> deleteUserLogin(@RequestBody UserLoginRequestDto request, HttpServletRequest servletRequest) {
        Metadata metadata = constructMetadata(servletRequest);
        return dataSetupService.deleteUserLogin(request,metadata);
    }
    @ApiOperation("Get Cabang Kategori")
    @PostMapping(value = "/cabang/get")
    public ResponseEntity<Object> getCabang(@RequestBody CommonRequestDto request, HttpServletRequest servletRequest) throws JsonProcessingException {
        Metadata metadata = constructMetadata(servletRequest);
        return dataSetupService.getCabang(request,metadata);
    }
    @ApiOperation("Save Cabang Kategori")
    @PostMapping(value = "/cabang/save")
    public ResponseEntity<Object> saveCabang(@RequestBody CabangRequestDto request, HttpServletRequest servletRequest) {
        Metadata metadata = constructMetadata(servletRequest);
        return dataSetupService.saveCabang(request,metadata);
    }
    @ApiOperation("Delete Cabang Kategori")
    @PostMapping(value = "/cabang/delete")
    public ResponseEntity<Object> deleteCabang(@RequestBody CabangRequestDto request, HttpServletRequest servletRequest) {
        Metadata metadata = constructMetadata(servletRequest);
        return dataSetupService.deleteCabang(request,metadata);
    }
    @ApiOperation("Get Unit Kategori")
    @PostMapping(value = "/unit/get")
    public ResponseEntity<Object> getUnit(@RequestBody CommonRequestDto request, HttpServletRequest servletRequest) throws JsonProcessingException {
        Metadata metadata = constructMetadata(servletRequest);
        return dataSetupService.getUnit(request,metadata);
    }
    @ApiOperation("Save Unit Kategori")
    @PostMapping(value = "/unit/save")
    public ResponseEntity<Object> saveUnit(@RequestBody UnitRequestDto request, HttpServletRequest servletRequest) {
        Metadata metadata = constructMetadata(servletRequest);
        return dataSetupService.saveUnit(request,metadata);
    }
    @ApiOperation("Delete Unit Kategori")
    @PostMapping(value = "/unit/delete")
    public ResponseEntity<Object> deleteUnit(@RequestBody UnitRequestDto request, HttpServletRequest servletRequest) {
        Metadata metadata = constructMetadata(servletRequest);
        return dataSetupService.deleteUnit(request,metadata);
    }
    @ApiOperation("Get Unit Kategori")
    @PostMapping(value = "/uom/get")
    public ResponseEntity<Object> getUom(@RequestBody CommonRequestDto request, HttpServletRequest servletRequest) throws JsonProcessingException {
        Metadata metadata = constructMetadata(servletRequest);
        return dataSetupService.getUom(request,metadata);
    }
    @ApiOperation("Save Unit Kategori")
    @PostMapping(value = "/uom/save")
    public ResponseEntity<Object> saveUom(@RequestBody UomRequestDto request, HttpServletRequest servletRequest) {
        Metadata metadata = constructMetadata(servletRequest);
        return dataSetupService.saveUOM(request,metadata);
    }
    @ApiOperation("Delete Unit Kategori")
    @PostMapping(value = "/uom/delete")
    public ResponseEntity<Object> deleteUom(@RequestBody UomRequestDto request, HttpServletRequest servletRequest) {
        Metadata metadata = constructMetadata(servletRequest);
        return dataSetupService.deleteUOM(request,metadata);
    }
    @ApiOperation("Get permission")
    @PostMapping(value = "/permission/get")
    public ResponseEntity<Object> getPermission(@RequestBody CommonRequestDto request, HttpServletRequest servletRequest) throws JsonProcessingException {
        Metadata metadata = constructMetadata(servletRequest);
        return dataSetupService.getPermission(request,metadata);
    }
    @ApiOperation("Save permission")
    @PostMapping(value = "/permission/save")
    public ResponseEntity<Object> savePermission(@RequestBody PermissionRequestDto request, HttpServletRequest servletRequest) {
        Metadata metadata = constructMetadata(servletRequest);
        return dataSetupService.savePermission(request,metadata);
    }
    @ApiOperation("Delete permission")
    @PostMapping(value = "/permission/delete")
    public ResponseEntity<Object> deletePermission(@RequestBody PermissionRequestDto request, HttpServletRequest servletRequest) {
        Metadata metadata = constructMetadata(servletRequest);
        return dataSetupService.deletePermission(request,metadata);
    }
}
