package com.rni.rni.controler;

import com.rni.rni.domain.dto.Metadata;
import com.rni.rni.service.FileUploadService;
import io.swagger.annotations.Api;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.xml.sax.SAXException;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@Api
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
public class FileUploadController extends AbstractBaseController {

    @Autowired
    FileUploadService fileUploadService;

    @PostMapping(value = "/upload")
    public ResponseEntity<Object> response(@RequestParam("file") MultipartFile file,
                                           @RequestParam("table_name") String request, HttpServletRequest servletRequest) throws IOException, OpenXML4JException, SAXException {
        Metadata metadata = constructMetadata(servletRequest);
        return fileUploadService.handleUpload(file,request,metadata);
    }
}
