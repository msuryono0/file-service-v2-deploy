package com.rni.rni.controler;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rni.rni.config.RedisConfig;
import com.rni.rni.config.RedisPublishConfig;
import com.rni.rni.config.RedisSubscribeConfig;
import com.rni.rni.domain.dao.Golongan;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Api
@RestController
public class RedisMasterController extends AbstractBaseController{

    @Autowired
    private RedisPublishConfig redisPublishConfig;
    @Autowired
    private RedisSubscribeConfig redisSubscribeConfig;

    @Autowired
    private RedisConfig redisConfig;

    @Autowired
    private ObjectMapper mapper;

    /**
     * CONTOH REDIS CHANEL PUB/SUB
     * @param golongan
     * @throws JsonProcessingException
     */
    @PostMapping("/publish/golongan")
    public void publish(@RequestBody List<Golongan> golongan) throws JsonProcessingException {
        redisConfig.setMessageListener("golongan");
        redisPublishConfig.publish(mapper.writeValueAsString(golongan),"golongan");
    }
    @PostMapping("/publish/get")
    public List<Golongan> get() throws JsonProcessingException {
        redisConfig.setChanel("golongan");
        var data = RedisSubscribeConfig.messageList.substring(7);
        List<Golongan>  golongan = mapper.readValue(data, new TypeReference<List<Golongan>>(){});
        return golongan;
    }

}
